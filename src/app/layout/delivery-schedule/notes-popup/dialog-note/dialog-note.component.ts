import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ComponentsService } from 'src/app/shared/services';

@Component({
  selector: 'app-dialog-note',
  templateUrl: './dialog-note.component.html',
  styleUrls: ['./dialog-note.component.scss']
})
export class DialogNoteComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogNoteComponent>,
    public componentsService: ComponentsService,
    @Inject(MAT_DIALOG_DATA) public data) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  saveComponentNote(key, comments){
    const updateObj = {"comments": comments};
    return this.componentsService.updateComponent(key, updateObj)
            .then(()=>{
              this.dialogRef.close();
            });
  }
}
