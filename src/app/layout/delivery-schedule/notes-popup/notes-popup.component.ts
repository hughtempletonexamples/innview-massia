import { Component, OnInit, Input } from '@angular/core';
import { DialogNoteComponent } from './dialog-note/dialog-note.component';
import { MatDialog } from '@angular/material';
import { Components } from 'src/app/shared/models/models';

@Component({
  selector: 'app-notes-popup',
  templateUrl: './notes-popup.component.html',
  styleUrls: ['./notes-popup.component.scss']
})
export class NotesPopupComponent {


  @Input() component: Components;

  constructor(public dialog: MatDialog) {}

  openDialog(): void {

    const dialogRef = this.dialog.open(DialogNoteComponent, {
      width: '300px',
      data: {component: this.component}
    });

    dialogRef.componentInstance.data = {component: this.component};

  }
}
