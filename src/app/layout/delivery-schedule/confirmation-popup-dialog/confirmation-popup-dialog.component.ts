import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DeliveryScheduleComponent } from '../delivery-schedule.component';
import { LoadPanelsService } from 'src/app/shared/services/load-panels.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-confirmation-popup-dialog',
  templateUrl: './confirmation-popup-dialog.component.html',
  styleUrls: ['./confirmation-popup-dialog.component.scss']
})
export class ConfirmationPopupDialogComponent{

  text: string;
  titles: Array<any>;
  errorTitles: Array<any>;
  statusData: object;
  statusSubscription: Subscription;

  constructor(
    public dialogRef: MatDialogRef<DeliveryScheduleComponent>,
    private loadpanelsService: LoadPanelsService,
    @Inject(MAT_DIALOG_DATA) public data) {
      if(this.data.result.errors.length){
        this.text = "The following errors have been identified: ";
      }else{
        this.text = "The following components and areas have been identified: ";
      }
      this.titles = [
        {name: "Component", id: "component"},
        {name: "Phase", id: "phase"},
        {name: "Floor", id: "floor"},
        {name: "Type", id: "type"},
        {name: "Estimated Panels", id: "estimatedPanels"},
        {name: "Estimated Area", id: "estimatedArea"}
      ];
      this.errorTitles = [
        {name: "Row"},
        {name: "EST_Phase"},
        {name: "Base Constraint"},
        {name: "Function"},
        {name: "Area_Net"},
        {name: "Type"}
      ];

      this.statusData = {
        loading: false,
        status:"Processing....",
        type: "Delete",
        counter: 0
      }

      this.statusSubscription = this.loadpanelsService.getProgress().subscribe((data)=>{
        this.statusData = data;
      });
    }

  onNoClick(): void {
    this.dialogRef.close();
  }

  addAreas(){
    return this.loadpanelsService.addAreas(this.data);
  }

  ngOnDestroy(){
    this.statusSubscription.unsubscribe();
  }

  getAreaId(area, project) {
    return [project.$key, area.phase, area.floor, area.type].join("-");
  }
}
