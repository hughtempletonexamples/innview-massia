import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectComponent } from './project.component';

const routes: Routes = [
  {
      path: '',
      component: ProjectComponent,
      children: [
        { 
          path: '', 
          redirectTo: 'project-details', 
          pathMatch: 'full' },
        {
          path: 'project-details',
          loadChildren: '../project-details/project-details.module#ProjectDetailsModule'
        },
        {
          path: 'delivery-schedule',
          loadChildren: '../delivery-schedule/delivery-schedule.module#DeliveryScheduleModule'
        },
        {
          path: 'project-scope',
          loadChildren: '../project-scope/project-scope.module#ProjectScopeModule'
        },
        {
          path: 'project-revisions',
          loadChildren: '../project-revisions/project-revisions.module#ProjectRevisionsModule'
        }
    ],
  },{
    path: '**',
    redirectTo: 'project-details',
    pathMatch: 'full'
  }];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectRoutingModule { }
