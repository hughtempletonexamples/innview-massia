import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatTableModule, MatSortModule, MatToolbarModule, MatIconModule } from '@angular/material';
import { MatFormFieldModule, MatPaginatorModule } from '@angular/material';
import { MatInputModule } from '@angular/material';

import { ProjectsRoutingModule } from './projects-routing.module';
import { ProjectsComponent } from './projects.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { UserFromRefModule } from '../../shared/modules/user-from-ref/user-from-ref.module';

import { MatCarouselModule } from '@ngmodule/material-carousel';

@NgModule({
    imports: [
        CommonModule,
        ProjectsRoutingModule,
        MatToolbarModule,
        MatSortModule,
        MatTableModule,
        MatFormFieldModule,
        MatPaginatorModule,
        MatInputModule,
        MatIconModule,
        UserFromRefModule,
        MatCarouselModule.forRoot(),
        FlexLayoutModule.withConfig({addFlexToParent: false})
    ],
    declarations: [ProjectsComponent]
})
export class ProjectsModule {}
