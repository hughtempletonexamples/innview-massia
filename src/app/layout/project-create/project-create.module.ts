import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectCreateComponent } from './project-create.component';
import { MatCardModule, MatFormFieldModule, MatInputModule, MatNativeDateModule, MatDatepickerModule, MatCheckboxModule, MatButtonModule, MatToolbarModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SrdSelectModule } from 'src/app/shared/modules/srd-select/srd-select.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AssociatedUsersSelectModule } from 'src/app/shared/modules/associated-users-select/associated-users-select.module';
import { NotificationSelectModule } from 'src/app/shared/modules/notification-select/notification-select.module';
import { GeoRouteModule } from 'src/app/shared/modules/geo-route/geo-route.module';
import { ProjectCreateRoutingModule } from './project-create-routing.module';

@NgModule({
  declarations: [ProjectCreateComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatFormFieldModule,
    MatToolbarModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    SrdSelectModule,
    NotificationSelectModule,
    GeoRouteModule,
    AssociatedUsersSelectModule,
    MatNativeDateModule, 
    MatDatepickerModule,
    ProjectCreateRoutingModule,
    FlexLayoutModule.withConfig({addFlexToParent: false})
  ],
  exports: [ProjectCreateComponent]
})

export class ProjectCreateModule { }
