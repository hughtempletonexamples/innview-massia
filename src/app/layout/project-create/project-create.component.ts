import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ProjectsService, AreasService } from 'src/app/shared/services';
import { MatDialog, MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { RevisionsService } from 'src/app/shared/services/revisions.service';
import { Router } from '@angular/router';
import { MomentDateAdapter } from '@angular/material-moment-adapter';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/M/YYYY',
  },
  display: {
    dateInput: 'DD/M/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};


@Component({
  selector: 'app-project-create',
  templateUrl: './project-create.component.html',
  styleUrls: ['./project-create.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class ProjectCreateComponent implements OnInit {

  minDate = new Date();
  projectControl: FormGroup;
  additionalControl: FormGroup;
  siteStarted: boolean;
  siteStartDateIsValid: boolean;
  nextDeliveryDateAfterToday: boolean;
  userKeys: Array<any>;
  submitted: boolean;

  constructor(public dialog: MatDialog,  private router: Router, private revisionsService: RevisionsService, private fb: FormBuilder, private projectsService: ProjectsService, private areasService: AreasService) { 
    this.userKeys = [];
    this.submitted = false;
    this.nextDeliveryDateAfterToday = false;
    this.siteStarted = false;
  }

  ngOnInit(){
    this.setUpForm();
  }

  isLoginToken(): boolean {
    const field = this.projectControl.get('id');
    return field.hasError('idAvailable');
  }

  setUpForm(){

    this.projectControl = this.fb.group({
      'name': new FormControl(
        // initial value
        null,
        // sync built-in validators
        Validators.compose(
          [ Validators.required ],
        ),
        // custom async validator
        this.areasService.projNameValidator()), 
      'id': new FormControl(
        // initial value
        null,
        // sync built-in validators
        Validators.compose(
          [ Validators.required, Validators.pattern('\\d\\d\\d$') ],
        ),
        // custom async validator
        this.areasService.projIdValidator()), 
      'client': new FormControl(null, [Validators.required]),
      'datumType': new FormControl(null, [Validators.required]),
      'chainOfCustody': new FormControl(),
      'siteAccess': new FormControl(),
      'siteAddress': new FormControl(),
      'postCode': new FormControl(),
      'weekDuration': new FormControl(),
      'siteStart': new FormControl(null, [Validators.required]),
      'includeSaturday': new FormControl(false),
      'specialRequirements': new FormControl(),
      'estimatedAreas': this.fb.group({
        'Int': new FormControl(null, [Validators.required]),
        'Ext': new FormControl(null, [Validators.required]),
        'Floor': new FormControl(null, [Validators.required]),
        'Roof': new FormControl(null, [Validators.required]),
        'ExtF': new FormControl(null, [Validators.required])
      }),
      'delivery-manager': new FormControl(),
      'notifyUsers': new FormControl(),
      'gateway': new FormControl()
    });

    this.additionalControl = this.fb.group({
      'site-ops': new FormControl(),
      'designer': new FormControl(),
      "quantity-surveyor":new FormControl(), 
      "engineer": new FormControl(),
      "team": new FormControl(),  
      "installer": new FormControl(), 
    });

  }
  

  onSubmit(): void{
    this.submitted = true;
    let updateObj = this.projectControl.value;
    updateObj = this.projectsService.convertFormToDatabaseStruct(updateObj);

    let updateObjAdditional = this.additionalControl.value;
    updateObjAdditional = this.projectsService.convertToAdditional(updateObjAdditional);

    let progKey = "";
    this.projectsService.createProject(updateObj)
      .then((createdProject)=>{
        progKey = createdProject.key;
        return this.projectsService.updateProjectAdditional(progKey, updateObjAdditional);
      })
      .then(()=>{
        return this.revisionsService.createRevisionForProject(progKey);
      })
      .then((revision)=>{
        return this.revisionsService.setUnpublishedRevisionOnProject(revision);
      })
      .then(()=>{
        this.router.navigate(['/project/' + progKey]);
      });
  }
  
}
