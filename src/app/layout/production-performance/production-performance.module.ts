import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatGridListModule, MatCardModule,MatFormFieldModule, MatDatepickerModule, MatIconModule, MatInputModule } from '@angular/material';
import { ProductionPerformanceRoutingModule } from './production-performance-routing.module';
import { ProductionPerformanceComponent } from './production-performance.component';
import { byNowPipe } from './by-now.pipe';
import { CurrentDateComponent } from '../production-performance/current-date/current-date.component';
import { MatMomentDateModule } from '@angular/material-moment-adapter';


@NgModule({
    imports: [
        CommonModule, 
        ProductionPerformanceRoutingModule, 
        MatGridListModule,
        MatCardModule,
        MatDatepickerModule,
        MatMomentDateModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule
    ],
    declarations: [ProductionPerformanceComponent, byNowPipe, CurrentDateComponent]
})
export class ProductionPerformanceModule {}
