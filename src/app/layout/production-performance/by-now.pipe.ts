import {Pipe,PipeTransform,Input} from "@angular/core";
import { formatDate } from "@angular/common"

@Pipe({name:"byNow"})

export class byNowPipe implements PipeTransform {
      
      transform(target, number):number {
        if(number != "~"){
          var byNow = 0;
          var date = new Date();
          var now = date;
          var dayHours = now.getHours();
          var dayMinutes = now.getMinutes();
          var timeAsDecimal = dayHours + (dayMinutes/60);

          var start = new Date (date.setHours(8,0,0,0)).getHours();

        if(timeAsDecimal >= start && timeAsDecimal <= 15.5){
            var portionOfDay = (timeAsDecimal - start) +1;
            var portionOfDayMinutes:number = (portionOfDay * 60);
            var cal = (portionOfDayMinutes) / 510;
            byNow = number * cal;
          }

          if(timeAsDecimal >= 15.5){
              byNow = number;
          }


          return Math.round(byNow);          
        }

      }

        
      }