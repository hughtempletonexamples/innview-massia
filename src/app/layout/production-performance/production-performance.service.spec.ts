import { TestBed, getTestBed, inject } from "@angular/core/testing";
import { AngularFireDatabaseModule, AngularFireDatabase, AngularFireList } from "@angular/fire/database";
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { of, Observable } from "rxjs";
import { MockFirebase } from "firebase-mock";
import { DateUtilService } from '../../shared/services/date-util.service';
import { ProductionPerformanceService } from './production-performance.service';
import { ForwardPlannerService } from "src/app/shared/services";


xdescribe('Production Performance Service', () => {
  let angularFireDatabaseMock: any;
  let fbListMock: any;
  let fbObjectMock: any;
  let data:any;
  let dateUtilsServiceMock: any;
  let forwardPlannerServiceMock: any;
  let service: ProductionPerformanceService;
  let firebaseRoot;
  let capacitiesRef;
  let statsRef;
  let areasRef;
  let helper: Helper;
  let httpMock;
  
  beforeEach(() => {
    helper = new Helper();

    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    capacitiesRef = firebaseRoot.child("productionCapacities");
    statsRef = firebaseRoot.child("stats");
    areasRef = firebaseRoot.child("areas");

    helper.addQuerySupport(capacitiesRef);
    helper.addQuerySupport(statsRef);
    helper.addQuerySupport(areasRef);

    //mocking for this service
  
    angularFireDatabaseMock = jasmine.createSpyObj('AngularFireDatabase', ['list', 'object']);

    fbListMock = jasmine.createSpyObj('list', ['snapshotChanges', 'valueChanges']);
    fbObjectMock = jasmine.createSpyObj('object', ['snapshotChanges', 'valueChanges']);

    dateUtilsServiceMock = jasmine.createSpyObj('dateUtilsServiceMock',['daysBetweenWeekDaysOnly','buildMap','todayAsIso'])
    //mocking for dependancy services
    dateUtilsServiceMock.daysBetweenWeekDaysOnly.and.callFake(value =>{
      return value;
    });

    forwardPlannerServiceMock = jasmine.createSpyObj('forwardPlannerServiceMock',['getStats','getCapacities'])


    forwardPlannerServiceMock.getCapacities.and.callFake(function(){
      const data = capacitiesRef.getData();
      return of(helper.getSnapShotChanges(data,false))
    });
    forwardPlannerServiceMock.getStats.and.callFake(function(){
      const data = statsRef.getData();
      return of(helper.getSnapShotChanges(data,false))
    });
    angularFireDatabaseMock.list.and.returnValue(fbListMock);
    angularFireDatabaseMock.object.and.returnValue(fbObjectMock);

    fbListMock.snapshotChanges.and.returnValue(of([]));
    fbObjectMock.snapshotChanges.and.returnValue(of([]));

    //mocking for dependancy services
    TestBed.configureTestingModule({
      imports: [
        AngularFireDatabaseModule,
        HttpClientTestingModule
      ],
      providers: [
        {provide: AngularFireDatabase, useValue: angularFireDatabaseMock},
        {provide: DateUtilService, useValue: dateUtilsServiceMock},
        {provide: ForwardPlannerService, useValue: forwardPlannerServiceMock}
      ]
    });
    httpMock = getTestBed().get(HttpTestingController);
    service = TestBed.get(ProductionPerformanceService);




  });

    beforeEach(() =>{
      statsRef.set({
        "SIP":{
          "2019-06-04":{
            "startedCount":23,
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-05":{
            "startedCount":21,
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-06":{
            "startedCount":19,
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-07":{
            "startedCount":31,
            "completedCount":21,
            "completedM2":88.9
          }
        },
        "HSIP":{
          "2019-06-04":{
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-05":{
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-06":{
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-07":{
            "completedCount":21,
            "completedM2":88.9
          }
        },
        "TF":{
          "2019-06-04":{
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-05":{
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-06":{
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-07":{
            "completedCount":21,
            "completedM2":88.9
          }
        },
        "CASS":{
          "2019-06-04":{
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-05":{
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-06":{
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-07":{
            "completedCount":26,
            "completedM2":98.9
          }
        }
      })
      capacitiesRef.set({
        "SIP":{
          "2019-06-04":22,
          "2019-06-05":30,
          "2019-06-06":8,
          "2019-06-07":10
        },
        "HSIP":{
          "2019-06-04":33,
          "2019-06-05":40,
          "2019-06-06":12,
          "2019-06-07":15
        },
        "TF":{
          "2019-06-04":11,
          "2019-06-05":20,
          "2019-06-06":5,
          "2019-06-07":6
        },
        "CASS":{
          "2019-06-04":12,
          "2019-06-05":10,
          "2019-06-06":16,
          "2019-06-07":11
        }
      })
    })  

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe("should subscribe ", ()=>{

    it('and combines latest', () => {
      service.getProductionPerformanceData().subscribe(([capacity,stats]) =>{
        //test capacity

        //CASS
        expect(capacity[0].payload.key).toEqual("CASS"); 
        expect(capacity[0].payload.val()).toEqual({ "2019-06-04": 12, "2019-06-05": 10, "2019-06-06": 16, "2019-06-07": 11 });
        //TF
        expect(capacity[2].payload.key).toEqual("SIP"); 
        expect(capacity[2].payload.val()).toEqual({ "2019-06-04": 22, "2019-06-05": 30, "2019-06-06": 8, "2019-06-07": 10 });
        //HSIP
        expect(capacity[1].payload.key).toEqual("HSIP"); 
        expect(capacity[1].payload.val()).toEqual({ "2019-06-04": 33, "2019-06-05": 40, "2019-06-06": 12, "2019-06-07": 15 });
        //SIP
        expect(capacity[3].payload.key).toEqual("TF"); 
        expect(capacity[3].payload.val()).toEqual({ "2019-06-04": 11, "2019-06-05": 20, "2019-06-06": 5, "2019-06-07": 6 });

        //test stats
        //Cass
        expect(stats[0].payload.key).toEqual("CASS");
        expect(stats[0].payload.val()).toEqual(        
        {
          "2019-06-04":{
          "completedCount":21,
          "completedM2":88.9
        },
        "2019-06-05":{
          "completedCount":21,
          "completedM2":88.9
        },
        "2019-06-06":{
          "completedCount":21,
          "completedM2":88.9
        },
        "2019-06-07":{
          "completedCount":26,
          "completedM2":98.9
        }
        });
        //HSIP
        expect(stats[1].payload.key).toEqual("HSIP");
        expect(stats[1].payload.val()).toEqual(        
        {
          "2019-06-04":{
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-05":{
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-06":{
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-07":{
            "completedCount":21,
            "completedM2":88.9
          }
        });
        //SIP
        expect(stats[2].payload.key).toEqual("SIP");
        expect(stats[2].payload.val()).toEqual(        
        {
          "2019-06-04":{
            "startedCount":23,
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-05":{
            "startedCount":21,
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-06":{
            "startedCount":19,
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-07":{
            "startedCount":31,
            "completedCount":21,
            "completedM2":88.9
          }
        });
        //TF
        expect(stats[3].payload.key).toEqual("TF");
        expect(stats[3].payload.val()).toEqual(        
        {
          "2019-06-04":{
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-05":{
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-06":{
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-07":{
            "completedCount":21,
            "completedM2":88.9
          }
        });

      })
    });
  })

  describe("should get Mapped data and", () =>{
      var capacitiesMapped = {
        "SIP":{
          "2019-06-04":22,
          "2019-06-05":30,
          "2019-06-06":8,
          "2019-06-07":10
        },
        "HSIP":{
          "2019-06-04":33,
          "2019-06-05":40,
          "2019-06-06":12,
          "2019-06-07":15
        },
        "TF":{
          "2019-06-04":11,
          "2019-06-05":20,
          "2019-06-06":5,
          "2019-06-07":6
        },
        "CASS":{
          "2019-06-04":12,
          "2019-06-05":10,
          "2019-06-06":16,
          "2019-06-07":11
        }
      }
      var statsMapped = {
        "SIP":{
          "2019-06-04":{
            "startedCount":23,
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-05":{
            "startedCount":21,
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-06":{
            "startedCount":19,
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-07":{
            "startedCount":31,
            "completedCount":21,
            "completedM2":88.9
          }
        },
        "HSIP":{
          "2019-06-04":{
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-05":{
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-06":{
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-07":{
            "completedCount":21,
            "completedM2":88.9
          }
        },
        "TF":{
          "2019-06-04":{
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-05":{
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-06":{
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-07":{
            "completedCount":21,
            "completedM2":88.9
          }
        },
        "CASS":{
          "2019-06-04":{
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-05":{
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-06":{
            "completedCount":21,
            "completedM2":88.9
          },
          "2019-06-07":{
            "completedCount":26,
            "completedM2":98.9
          }
        }
      }
      var areasMapped = {
        "area1":{
          "$key":"area1",
          "active":true,
          "actualArea":20.9,
          "actualPanels":5,
          "floor":"3F",
          "project":"project1",
          "_productionLine":"SIP"
        },
        "area4":{
          "$key":"area1",
          "active":true,
          "actualArea":20.9,
          "actualPanels":5,
          "floor":"3F",
          "project":"project1",
          "_productionLine":"SIP"
        }
      }
      var plansMapped = {

          "area1":{
            "plannedFinish":"2019-07-25",
            "plannedStart":"2019-06-30",
            "riskDate":"2019-07-11",
            "project":"project1"
          },
          "area2":{
            "plannedFinish":"2019-07-25",
            "plannedStart":"2019-07-04",
            "riskDate":"2019-07-11",
            "project":"project1"
          },
          "area3":{
            "plannedFinish":"2019-07-25",
            "plannedStart":"2019-06-30",
            "riskDate":"2019-07-28",
            "project":"project1"
          },
          "area4":{
            "plannedFinish":"2019-07-15",
            "plannedStart":"2019-07-08",
            "riskDate":"2019-07-11",
            "project":"project1"
          }
      }
    it("should get target for production Line",() =>{
      data = service.target(capacitiesMapped,"2019-06-04","SIP");
      expect(data).toEqual(22);
    });
    it("should get Panel quantity production Line",() =>{
      data = service.getPanelQty(statsMapped,"2019-06-07","CASS","completedCount");
      expect(data).toEqual(26);
    });
    it("should get Day target for production Line",() =>{
      data = service.dayTarget(statsMapped,capacitiesMapped,"2019-06-07","CASS");
      expect(data).toEqual(15);
    });    
    it("should get In progress for line",() =>{
      data = service.getInProgressCount(statsMapped,"SIP");
      expect(data).toEqual(10);
    });
    it("should get greatest risks",() =>{
      var data = service.getGreatestRisks(plansMapped,areasMapped);
      expect(data['SIP']).toContain('2019-07-25');
    });
    it("should sort lowest risks",() =>{
      var arr = ["32","12","09","78","-5",-"18","14"]
      var data = service.getLowestRisk(arr);
      expect(data).toEqual(-18);
    });
    it("should get areas for production today",() =>{
      var data = service.getAreasForProductionToday(areasMapped,plansMapped,"2019-07-08");
      expect(data).toEqual(21);
    });  
    it("should get areas for production today",() =>{
      var data = service.getPlannedProductionTotal(areasMapped,plansMapped,"SIP","2019-07-08");
      expect(data).toEqual(21);
    });   
  })
  describe("should usecolour change code", ()=>{
    beforeEach(()=>{

    })
    it("and return colour indicator",()=>{
      var scenario1 = service.colorIndicator(12,15)
      expect(scenario1).toEqual("red-indicator")
      var scenario2 = service.colorIndicator(14,15)
      expect(scenario2).toEqual("amber-indicator")
      var scenario3 = service.colorIndicator(16,15)
      expect(scenario3).toEqual("purple-indicator")
      var scenario4 = service.colorIndicator(15,15)
      expect(scenario4).toEqual("green-indicator")
    })

    it("and return day Target Color Indicator",()=>{
    var now = new Date();
    var before3 = now.setHours(13,0,0,0)
    var after3 = now.setHours(15,30,0,0)

      var scenario1 = service.dayTargetColorIndicator(after3,-15,-6,-5,0)
      expect(scenario1).toEqual("red-indicator")
      var scenario2 = service.dayTargetColorIndicator(after3,2,-6,-5,0)
      expect(scenario2).toEqual("green-indicator")
      var scenario3 = service.dayTargetColorIndicator(after3,-2,-6,-5,0)
      expect(scenario3).toEqual("amber-indicator")
      var scenario4 = service.dayTargetColorIndicator(before3,-5,-6,-5,0)
      expect(scenario4).toEqual("white-indicator")
    })
  })


});

class Helper {
  actions: any[] = [];
  actions2: any[] = [];
  actions3: any[] = [];
  getSnapShotChanges(data: object, asObserv: boolean){
      this.actions = [];
      const dataKeys = Object.keys(data);
      for(let i = 0; i < dataKeys.length; i++){
          this.actions.push({
            payload: {
              val:  function() { return data[dataKeys[i]] },
              key: dataKeys[i]
            },
            prevKey: null,
            type: "value"
          });
      }
      if(asObserv){
        return of(this.actions);
      }else{
        return this.actions;
      }
  };
  getValueChanges(data: object): Observable<any[]>{
    this.actions2 = [];
    const dataKeys = Object.keys(data);
    for(let i = 0; i < dataKeys.length; i++){
        this.actions2.push(data[dataKeys[i]]);
    }
    return of(this.actions2);
  };
  getMappedValue(data: Array<any>){
    this.actions3 = [];
    const dataKeys = Object.keys(data);
    for(let i = 0; i < dataKeys.length; i++){
      data[dataKeys[i]].$key = dataKeys[i];
      this.actions3.push(data[dataKeys[i]]);
    }
    return this.actions3;
  };
  addQuerySupport(firebaseRef) {
    var childKey = ".";
  
    firebaseRef.orderByChild = jasmine.createSpy("orderByChild").and.callFake(mockOrderByChild);
  
    function mockOrderByChild(key) {
      childKey = key;
  
      var queryResults = firebaseRef.root().child(firebaseRef.key + "-orderByChild(" + childKey + ")");
      queryResults.autoFlush(true);
  
      var data = firebaseRef.getData();
      var keys = firebaseRef.getKeys();
  
      keys.sort(function (a, b) {
        return compare(data[a][childKey], data[b][childKey]);
      });
  
      // Use priority to get required order
      keys.forEach(function (key, index) {
        queryResults.child(key).setWithPriority(data[key], index);
      });
  
      queryResults.on("child_added", resync);
      queryResults.on("child_changed", resync);
  
      // Brute force to reflect changes in query results to original
      function resync() {
        let data = queryResults.getData();
        let dataKeys = Object.keys(data);
        dataKeys.forEach(function ( key ) {
          let item = data[key];
          firebaseRef.child(key).set(item);
        });
      }
  
      queryResults.equalTo = jasmine.createSpy("equalTo").and.callFake(mockEqualTo);
      queryResults.startAt = jasmine.createSpy("startAt").and.callFake(mockStartAt);
      queryResults.endAt = jasmine.createSpy("endAt").and.callFake(mockEndAt);
  
      return queryResults;
  
      function mockEqualTo(value) {
        let data = queryResults.getData();
        let dataKeys = Object.keys(data);
        dataKeys.forEach(function ( key ) {
          let item = data[key];
          if (item[childKey] !== value) {
            queryResults._removeChild(key);
          }
        });
  
        return queryResults;
      }
      
      function mockStartAt(value) {
        let data = queryResults.getData();
        let dataKeys = Object.keys(data);
        dataKeys.forEach(function ( key ) {
          let item = data[key];
          if (item[childKey] < value) {
            queryResults._removeChild(key);
          }
        });
  
        return queryResults;
      }
      
      function mockEndAt(value) {
        let data = queryResults.getData();
        let dataKeys = Object.keys(data);
        dataKeys.forEach(function ( key ) {
          let item = data[key];
          if (item[childKey] > value) {
            queryResults._removeChild(key);
          }
        });
  
        return queryResults;
      }
    }
    function mapper(buildObj){
      let objHolder = {};
      buildObj.map((snap) => {
        objHolder[snap.key] = snap.payload.val();
      });
      return objHolder;
    }

    function compare(a, b) {
      if (typeof a === "number") {
        return a - b;
      }
      else if (typeof a === "string") {
        return a.localeCompare(b);
      }
      else {
        return 0;
      }
    }

  };
}
