import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProductionPerformanceComponent } from './production-performance.component';

const routes: Routes = [
    {
        path: '',
        component: ProductionPerformanceComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProductionPerformanceRoutingModule {}
