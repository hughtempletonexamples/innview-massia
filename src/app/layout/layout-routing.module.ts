import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: 'dashboard',
                loadChildren: './dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'charts',
                loadChildren: './charts/charts.module#ChartsModule'
            },
            {
                path: 'components',
                loadChildren:
                    './material-components/material-components.module#MaterialComponentsModule'
            },
            {
                path: 'forms',
                loadChildren: './forms/forms.module#FormsModule'
            },
            {
                path: 'grid',
                loadChildren: './grid/grid.module#GridModule'
            },
            {
                path: 'tables',
                loadChildren: './tables/tables.module#TablesModule'
            },
            {
                path: 'blank-page',
                loadChildren: './blank-page/blank-page.module#BlankPageModule'
            },
            {
                path: 'design-issued',
                loadChildren: './design-issued/design-issued.module#DesignIssuedModule'
            },
            {
                path: 'performance',
                loadChildren: './performance/performance.module#PerformanceModule'
            },
            {
                path: 'forward-planner',
                loadChildren: './forward-planner/forward-planner.module#ForwardPlannerModule'
            },
            {
                path: 'projects',
                loadChildren: './projects/projects.module#ProjectsModule'
            },
            {
                path: 'project/:id',
                loadChildren: './project/project.module#ProjectModule'
            },
            {
                path: 'project-create',
                loadChildren: './project-create/project-create.module#ProjectCreateModule'
            },
            {
                path: 'forecast',
                loadChildren: './forecast/forecast.module#ForecastModule'
            },
            {
                path: 'material-request',
                loadChildren: './material-request/material-request.module#MaterialRequestModule'
            },
            {
                path: 'material-request-create',
                loadChildren: './material-request-create/material-request-create.module#MaterialRequestCreateModule'
            },
            {
                path: 'material-request-details/:id',
                loadChildren: './material-request-details/material-request-details.module#MaterialRequestDetailsModule'
            }
            ,
            {
                path: 'project-status',
                loadChildren: './project-status/project-status.module#ProjectStatusModule'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
