import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectDetailsRoutingModule } from './project-details-routing.module';
import { ProjectDetailsComponent } from './project-details.component';
import { MatCardModule, MatFormFieldModule, MatInputModule, MatNativeDateModule, MatDatepickerModule, MatCheckboxModule, MatButtonModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SrdSelectModule } from 'src/app/shared/modules/srd-select/srd-select.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AssociatedUsersSelectModule } from 'src/app/shared/modules/associated-users-select/associated-users-select.module';
import { NotificationSelectModule } from 'src/app/shared/modules/notification-select/notification-select.module';
import { GeoRouteModule } from 'src/app/shared/modules/geo-route/geo-route.module';
import { ConfirmationPopupDialogComponent } from './confirmation-popup-dialog/confirmation-popup-dialog.component';

@NgModule({
  declarations: [ProjectDetailsComponent, ConfirmationPopupDialogComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    SrdSelectModule,
    NotificationSelectModule,
    GeoRouteModule,
    AssociatedUsersSelectModule,
    MatNativeDateModule, 
    MatDatepickerModule,
    ProjectDetailsRoutingModule,
    FlexLayoutModule.withConfig({addFlexToParent: false})
  ],
  exports: [ProjectDetailsComponent],
  entryComponents: [ConfirmationPopupDialogComponent]
})
export class ProjectDetailsModule { }
