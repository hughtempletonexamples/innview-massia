import { Component, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Project, ProjectsAdditional } from 'src/app/shared/models/models';
import { DateUtilService } from 'src/app/shared/services/date-util.service';
import { ProjectsService, AreasService } from 'src/app/shared/services';
import { MatDialog, MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { ConfirmationPopupDialogComponent } from './confirmation-popup-dialog/confirmation-popup-dialog.component';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { CustomDateAdapter } from 'src/app/shared/helpers/custom-adapter-date';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/M/YYYY',
  },
  display: {
    dateInput: 'DD/M/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};


@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: DateAdapter, useClass: CustomDateAdapter, deps: [MAT_DATE_LOCALE]},

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class ProjectDetailsComponent implements OnInit {

  projectControl: FormGroup;
  additionalControl: FormGroup;
  minDate = new Date();
  project: Project;
  areas: Array<any>;
  projectAdditional: ProjectsAdditional;
  originalSiteStart: string;
  nextDeliveryDate: string;
  siteStarted: boolean;
  siteStartDateIsValid: boolean;
  nextDeliveryDateAfterToday: boolean;
  userKeys: Array<any>;
  submitted: boolean;
  _subscription: Subscription;
  loaded: boolean;

  constructor(private route: ActivatedRoute, public dialog: MatDialog, private router: Router, private fb: FormBuilder, private dateUtilService: DateUtilService, private projectsService: ProjectsService, private areasService: AreasService) { 
    this.userKeys = [];
    this.submitted = false;
    this.nextDeliveryDateAfterToday = false;
    this.siteStarted = false;
    this.loaded = false;
  }

  ngOnInit(){

    const id = this.route.parent.parent.snapshot.paramMap.get('id');

    this._subscription = this.areasService.getProjectsAreasAdditional(id).subscribe(([project, projectAdditional, areas]) => {
      this.project = project;
      this.areas = areas;
      this.projectAdditional = projectAdditional;
      this.getOriginalDates();
      this.setUpForm();
      this.loaded = true;
    });
    
  }

  isLoginToken(): boolean {
    const field = this.projectControl.get('id');
    return field.hasError('idAvailable');
  }

  getOriginalDates(){
    this.originalSiteStart = this.project.siteStart;
    this.nextDeliveryDate = this.project.nextDeliveryDate;
    
    var nextDeliveryDate = "9999-99-99";
    var todaysDate = this.dateUtilService.todayAsIso();
    var earliestDeliveryDate = "9999-99-99";
    var revisionRef = this.project.deliverySchedule.published;

    this.areas.forEach(areaSnap => {
      const area = areaSnap.payload.val();
      if (area.revisions[revisionRef] !== undefined && area.revisions[revisionRef] < earliestDeliveryDate) {
        earliestDeliveryDate = area.revisions[revisionRef];
      }
      if (area.revisions[revisionRef] !== undefined && area.revisions[revisionRef] < nextDeliveryDate && area.revisions[revisionRef] > todaysDate) {
        nextDeliveryDate = area.revisions[revisionRef];
      }
    });

    if(nextDeliveryDate !== "9999-99-99"){
      this.project.nextDeliveryDate = nextDeliveryDate;
    }

    if(earliestDeliveryDate !== "9999-99-99"){
      this.project.siteStart = earliestDeliveryDate;
    }
  }

  setUpForm(){

      if(this.project !== undefined){
        if (this.project.siteStart !== undefined) {
          if (this.project.siteStart < this.dateUtilService.todayAsIso()) {
            this.siteStarted = true;
          }
        }
        
        if (this.project.nextDeliveryDate !== undefined) {
          if (this.project.nextDeliveryDate > this.dateUtilService.todayAsIso()) {
            this.nextDeliveryDateAfterToday = true;
          }
        }
      }

      if(this.project.notifyUsers !== undefined){
        Object.keys(this.project.notifyUsers).forEach((userKey)=>{
          this.userKeys.push(userKey);
        });
      }
      this.projectControl = this.fb.group({
        'name': new FormControl({value: this.project.name, disabled: true}, [Validators.required]), 
        'id': new FormControl({value: this.project.id, disabled: true}, [Validators.required, Validators.pattern('^\d\d\d$')]),
        'client': new FormControl(this.project.client, [Validators.required]),
        'datumType': new FormControl(this.project.datumType, [Validators.required]),
        'chainOfCustody': new FormControl(this.project.chainOfCustody),
        'siteAccess': new FormControl(this.project.siteAccess),
        'siteAddress': new FormControl(this.project.siteAddress),
        'postCode': new FormControl(this.project.postCode),
        'weekDuration': new FormControl(this.project.weekDuration),
        'siteStart2': new FormControl({value: this.project.siteStart, disabled: true}, [Validators.required]),
        'siteStart': new FormControl({value: this.project.siteStart, disabled: this.siteStarted}, [Validators.required]),
        'nextDeliveryDate': new FormControl({value: this.project.nextDeliveryDate ? this.project.nextDeliveryDate : this.project.siteStart, disabled: !this.siteStarted}, [Validators.required]),
        'includeSaturday': new FormControl(this.project.includeSaturday),
        'specialRequirements': new FormControl(this.project.specialRequirements),
        'estimatedAreas': this.fb.group({
          'Int': new FormControl(this.project.estimatedAreas.Int, [Validators.required]),
          'Ext': new FormControl(this.project.estimatedAreas.Ext, [Validators.required]),
          'Floor': new FormControl(this.project.estimatedAreas.Floor, [Validators.required]),
          'Roof': new FormControl(this.project.estimatedAreas.Roof, [Validators.required]),
          'ExtF': new FormControl(this.project.estimatedAreas.ExtF, [Validators.required])
        }),
        'delivery-manager': new FormControl(this.project.deliveryManager),
        'notifyUsers': new FormControl(this.userKeys),
        'gateway': new FormControl(this.project.gateway)
      });

      if(this.projectAdditional){
        this.additionalControl = this.fb.group({
          'site-ops': new FormControl(this.projectAdditional.siteOps),
          'designer': new FormControl(this.projectAdditional.designer),
          "quantity-surveyor":new FormControl(this.projectAdditional.qs),  
          "engineer": new FormControl(this.projectAdditional.engineer),
          "team": new FormControl(this.projectAdditional.team),   
          "installer": new FormControl(this.projectAdditional.installer),     
        });
      }else{
        this.additionalControl = this.fb.group({
          'site-ops': new FormControl(),
          'designer': new FormControl(),
          "quantity-surveyor":new FormControl(), 
          "engineer": new FormControl(),
          "team": new FormControl(),  
          "installer": new FormControl(), 
        });
      }
  }

  onSubmit(): void{
    this.submitted = true;
    let updateObj = this.projectControl.value;
    updateObj = this.projectsService.convertFormToDatabaseStruct(updateObj);

    let updateObjAdditional = this.additionalControl.value;
    updateObjAdditional = this.projectsService.convertToAdditional(updateObjAdditional);

      const changeData = this.areasService.getdaysDiff(updateObj, this.originalSiteStart, this.nextDeliveryDate, this.siteStarted);
      if (changeData.diffDays !== 0 && this.project.deliverySchedule !== undefined && this.project.deliverySchedule.published !== undefined) {
        const dialogRef = this.dialog.open(ConfirmationPopupDialogComponent, {
          width: '800px',
          data: {project: this.project, updateObj: updateObj, updateObjAdditional: updateObjAdditional, changeData: changeData}
        });
        dialogRef.componentInstance.data = {project: this.project, updateObj: updateObj, updateObjAdditional: updateObjAdditional, changeData: changeData};
      }else{
        this.projectsService.updateProjectAdditional(this.project.$key, updateObjAdditional)
        .then(()=>{
          return this.projectsService.updateProject(this.project.$key, updateObj);
        })
        .then(()=>{
          this.router.navigate(['/projects']);
        });
      }
  }
}
