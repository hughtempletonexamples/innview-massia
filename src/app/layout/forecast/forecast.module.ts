import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ForecastRoutingModule } from './forecast-routing.module';
import { ForecastComponent } from './forecast.component';
import { StackedGroupedChartModule } from 'src/app/shared/modules/stacked-grouped-chart/stacked-grouped-chart.module';

@NgModule({
  declarations: [ForecastComponent],
  imports: [
    CommonModule,
    StackedGroupedChartModule,
    ForecastRoutingModule
  ]
})
export class ForecastModule { }
