import { Component, OnInit } from '@angular/core';
import { ChartsService } from 'src/app/shared/services/charts.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.scss']
})
export class ForecastComponent implements OnInit {

  _subscription: Subscription;
  chartData: object;
  loaded: boolean;

  constructor(private chartService: ChartsService) { 
    this.loaded = false;
  }

  ngOnInit() {

    this._subscription =  this.chartService.getForecastData().subscribe(([gateways, productTypes, productionLines, projects]) => {
      this.chartData = this.chartService.processForecastData(projects, gateways, productTypes, productionLines, 48, 0);
      this.loaded = true;
    });

  }

}