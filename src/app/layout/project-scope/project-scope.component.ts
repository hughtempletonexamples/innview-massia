import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ProjectsService } from 'src/app/shared/services';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { RevisionsService } from 'src/app/shared/services/revisions.service';

@Component({
  selector: 'app-project-scope',
  templateUrl: './project-scope.component.html',
  styleUrls: ['./project-scope.component.scss'],
})
export class ProjectScopeComponent implements OnInit {

  @Input() projectAdditionalData;
  @Input() project;
  @Input() framingStyles;

  submitted: boolean;
  loaded: boolean;
  scopeControl:FormGroup;
  srdData: any;
  loadedData:any;
  isMitigationEnabled: boolean;
  _subscription: Subscription;
    
  constructor(private route: ActivatedRoute, private revisionsService: RevisionsService, private fb:FormBuilder, private projectsService: ProjectsService, private router:Router) { 
    
    this.loaded = false;
    this.isMitigationEnabled = false;
    this.submitted = false;
    this.loadedData = {
      "framingStyles":{
        "SIP":[],
        "HSIP":[],
        "IFAST":[],
        "CASS":[],
        "TF":[],
      },
      "fireMitigationRequired":false,
      "fireMitigationCoverage":0
    };

  }

  ngOnInit() {

    const id = this.route.parent.parent.snapshot.paramMap.get('id');

    this._subscription = this.revisionsService.getProjectFramingAdditional(id).subscribe(([project, projectAdditional, framingStyles]) => {

      this.framingStyles = framingStyles;
      this.project = project;
      this.projectAdditionalData = projectAdditional;
      
      //for each loopify this !!!!
      this.srdData = {};
      Object.keys(this.framingStyles).forEach(line=>{
        this.srdData[line] =  Object.keys(this.framingStyles[line]);
      });

      if(this.projectAdditionalData && this.projectAdditionalData.framingStyles){
        Object.keys(this.projectAdditionalData.framingStyles).forEach(line=>{
          this.loadedData.framingStyles[line] = Object.keys(this.projectAdditionalData.framingStyles[line]);
        });
        this.loadedData.fireMitigationCoverage = this.projectAdditionalData.fireMitigationCoverage || 0;
        this.loadedData.fireMitigationRequired = this.projectAdditionalData.fireMitigationRequired || false;
      }

      this.scopeControl = this.fb.group({
        'SIP': new FormControl(this.loadedData.framingStyles.SIP),
        'HSIP': new FormControl(this.loadedData.framingStyles.HSIP),
        'IFAST': new FormControl(this.loadedData.framingStyles.IFAST),
        "CASS":new FormControl(this.loadedData.framingStyles.CASS),  
        "TF": new FormControl(this.loadedData.framingStyles.TF),
        "fireMitigationRequired": new FormControl(this.loadedData.fireMitigationRequired),
        "fireMitigationCoverage": new FormControl({value: this.loadedData.fireMitigationCoverage, disabled: !this.loadedData.fireMitigationRequired})   
      });

      this.loaded = true;

    });

    
  }

  isChecked(){
    if(this.scopeControl.value.fireMitigationRequired){
      this.scopeControl.get("fireMitigationCoverage").enable();
    }else{
      this.scopeControl.get("fireMitigationCoverage").disable();
    }
  }

  onSubmit(){

    let controlVal = this.scopeControl.value;

    if (controlVal !== undefined) {
      this.submitted = true;
      Object.keys(this.loadedData.framingStyles).forEach((line)=>{
        this.loadedData.framingStyles[line] = {};
        if(controlVal[line]){
          controlVal[line].forEach((framingStyle)=>{
            if(framingStyle !== undefined){
              if(this.loadedData.framingStyles[line][framingStyle] === undefined){
                this.loadedData.framingStyles[line][framingStyle] = true;
              }
            }
          });
        }
        delete this.loadedData[line];
      });
      this.loadedData.fireMitigationCoverage = controlVal.fireMitigationCoverage || 0;
      this.loadedData.fireMitigationRequired = controlVal.fireMitigationRequired;
      this.projectsService.updateProjectAdditional(this.project.$key, this.loadedData);

      console.log("new framing styles added")
      this.router.navigate(['/projects']);
    } else{
      console.log("nothing happened");
    }
  }
}
