import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectScopeRoutingModule } from './project-scope-routing.module';
import { ProjectScopeComponent } from './project-scope.component';
import { MatCardModule, MatFormFieldModule,MatIconModule, MatOptionModule,MatSelectModule,MatSliderModule, MatInputModule, MatNativeDateModule, MatDatepickerModule, MatCheckboxModule, MatButtonModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ProjectScopeComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatCardModule,
    MatSelectModule,
    MatSliderModule,
    MatFormFieldModule,
    MatOptionModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatNativeDateModule, 
    MatDatepickerModule,
    ProjectScopeRoutingModule,
    FlexLayoutModule.withConfig({addFlexToParent: false})
  ],
  exports: [ProjectScopeComponent],
})
export class ProjectScopeModule { }
