import { Component, Input, EventEmitter, Output} from '@angular/core';
import { ForwardPlannerService } from 'src/app/shared/services';
import { Plan } from '../../../shared/models/models';

@Component({
  selector: 'app-capacity-input',
  templateUrl: './capacity-input.component.html',
  styleUrls: ['./capacity-input.component.scss']
})
export class CapacityInputComponent {

  @Input()
    capacity: number;

  @Input()
    plan: Plan;

  @Input()
    productionLine: String;

  @Input()
    date: String;

  @Input()
    type: String;

  @Output() saveState = new EventEmitter<boolean>();

  constructor(private forwardPlannerService: ForwardPlannerService) { }

  update() {
    this.saveState.emit(true);
    return this.forwardPlannerService.setCapacity(this.plan, this.productionLine, this.date, this.capacity);
  }

}
