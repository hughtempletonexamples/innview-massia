import { Component, Input, OnInit } from '@angular/core';
import { AreasService } from '../../../shared/services';
import { MatDialog } from '@angular/material';
import { DialogLineComponent } from './dialog-line/dialog-line.component';
import { AreaExt } from 'src/app/shared/models/models';

@Component({
  selector: 'app-select-line',
  templateUrl: './select-line.component.html',
  styleUrls: ['./select-line.component.scss']
})
export class SelectLineComponent implements OnInit {

  @Input() area: AreaExt;
  show: boolean;

  constructor(private areaService: AreasService, public dialog: MatDialog) {}

  ngOnInit(){
    this.show = false;
    if(this.area._productionLine.indexOf("SIP") !== -1 || this.area._productionLine.indexOf("TF") !== -1){
      this.show = true;
    }
  }

  openDialogLine(): void {

    const dialogRef = this.dialog.open(DialogLineComponent, {
      width: '200px',
      data: {area: this.area}
    });

    dialogRef.componentInstance.data = {area: this.area};

    dialogRef.afterClosed().subscribe(result => {

    });

  }
}
