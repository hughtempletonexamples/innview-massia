import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DialogLineComponent } from './dialog-line.component';
import { MatCardModule } from '@angular/material'
import { AppComponent } from 'src/app/app.component';

xdescribe('DialogComponent', () => {
  let component: DialogLineComponent;
  let fixture: ComponentFixture<DialogLineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogLineComponent,AppComponent ],
      imports: [MatCardModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
