import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AreasService } from 'src/app/shared/services';

@Component({
  selector: 'app-dialog-line',
  templateUrl: './dialog-line.component.html',
  styleUrls: ['./dialog-line.component.scss']
})
export class DialogLineComponent{

  constructor(
    public dialogRef: MatDialogRef<DialogLineComponent>,
    public areasService: AreasService,
    @Inject(MAT_DIALOG_DATA) public data) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  areaLine(key, line){
    const updateObj = {"line": line};
    return this.areasService.updateArea(key, updateObj)
            .then(()=>{
              this.dialogRef.close();
            });
  }

}

