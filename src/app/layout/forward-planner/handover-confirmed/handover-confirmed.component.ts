import { Component, OnInit, Input } from '@angular/core';
import { AreasService } from 'src/app/shared/services';
import { Subscription } from 'rxjs';
import { AreaExt } from 'src/app/shared/models/models';

@Component({
  selector: 'app-handover-confirmed',
  templateUrl: './handover-confirmed.component.html',
  styleUrls: ['./handover-confirmed.component.scss']
})
export class HandoverConfirmedComponent implements OnInit {

  @Input() area: AreaExt;

  isChecked: boolean;

  _subscription: Subscription;

  constructor(private areasService: AreasService) { }

  ngOnInit() {

    this._subscription = this.areasService.getAdditionalAreaData(this.area.$key).subscribe((area: any) => {
      var val = area.payload.val()
      if(val !== null){
        if(val.handoverConfirmed !== undefined && val.handoverConfirmed.status !== undefined){
          this.isChecked = val.handoverConfirmed.status;
        }
      }
    });
  }

  ngOnDestroy(){
    this._subscription.unsubscribe();
  }

}
