import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  EventEmitter,
  Output,
  Input,
} from '@angular/core';
import { MatDatepickerInputEvent, MatCalendar} from '@angular/material/datepicker';
import {MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatDateFormats, DateAdapter} from '@angular/material/core';
import { ForwardPlannerService } from 'src/app/shared/services/forward-planner.service';
import { DateUtilService } from 'src/app/shared/services/date-util.service';
import { CustomDateAdapter } from 'src/app/shared/helpers/custom-adapter-date';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MomentDateAdapter } from '@angular/material-moment-adapter';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD-MMM',
  },
  display: {
    dateInput: 'DD-MMM',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-start-date-picker',
  templateUrl: './start-date-picker.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./start-date-picker.component.scss'],
  providers: [
    {provide: DateAdapter, useClass: CustomDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})  

export class StartDatePickerComponent {

  minDate = new Date();
  areas:Array<any> =[];

  @Input() startDate;
  @Input() area

  @Output() startChange = new EventEmitter<boolean>();
  
  dateHeader = ExampleHeader;

  constructor(private customDateAdapter: CustomDateAdapter,  private forwardPlannerService: ForwardPlannerService,
    private dateUtilService: DateUtilService) {
  }
  open(picker){
    picker.open();
    this.customDateAdapter.setPicker(picker);
    if(this.area){
      this.customDateAdapter.setArea(this.area);
    }
  }

  startDateChange(type: string, event: MatDatepickerInputEvent<any>) {
    this.startChange.emit(true);
    const date = this.dateUtilService.toIsoDate(event.value._d);
    return this.forwardPlannerService.setPlannedStart(this.area, date, true);
  }
}

@Component({
  selector: 'date-header',
  styles: [`
  .example-header {
    display: flex;
    align-items: center;
    padding: 0.5em;
  }

  .example-header-label {
    flex: 1;
    height: 1em;
    font-weight: 500;
    text-align: center;
  }

  .example-double-arrow .mat-icon {
    margin: -22%;
  }
`],
  template: `
    <div>
    <div style="text-align:center;">
      <button mat-icon-button  (click)="removeUserChange()"><mat-icon>undo</mat-icon></button>
      <button mat-icon-button  (click)="lockProductionDate()"><mat-icon>lock</mat-icon></button>
    </div>
    <div style="text-align:center;">
      <button mat-icon-button class="example-double-arrow" (click)="previousClicked('year')">
      <mat-icon>keyboard_arrow_left</mat-icon>
      <mat-icon>keyboard_arrow_left</mat-icon>
      </button>
      <button mat-icon-button (click)="previousClicked('month')">
        <mat-icon>keyboard_arrow_left</mat-icon>
      </button>
      <span class="example-header-label">{{periodLabel}}</span>
      <button mat-icon-button (click)="nextClicked('month')">
        <mat-icon>keyboard_arrow_right</mat-icon>
      </button>
      <button mat-icon-button class="example-double-arrow" (click)="nextClicked('year')">
        <mat-icon>keyboard_arrow_right</mat-icon>
        <mat-icon>keyboard_arrow_right</mat-icon>
      </button>
    </div>
  </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class ExampleHeader<D> implements OnDestroy {
  private _destroyed = new Subject<void>();
  constructor(private dateUtilService:DateUtilService,
      private _calendar: MatCalendar<any>, private _dateAdapter: DateAdapter<any>,private customDateAdapter:CustomDateAdapter,private forwardPlannerService:ForwardPlannerService
      ,@Inject(MAT_DATE_FORMATS) private _dateFormats: MatDateFormats, cdr: ChangeDetectorRef) {
    _calendar.stateChanges
        .pipe(takeUntil(this._destroyed))
        .subscribe(() => cdr.markForCheck());
  }
  get periodLabel() {
    return this._dateAdapter
        .format(this._calendar.activeDate, this._dateFormats.display.monthYearLabel)
        .toLocaleUpperCase();
  }
  lockProductionDate(){
    const area = this.customDateAdapter.getArea();
    var date = area._productionStartDate;
    this.forwardPlannerService.setPlannedStart(area,date,true);
    this.customDateAdapter.close();
  }
  previousClicked(mode: 'month' | 'year') {
    this._calendar.activeDate = mode === 'month' ?
      this.customDateAdapter.addCalendarMonths(this._calendar.activeDate, -1) :
      this.customDateAdapter.addCalendarYears(this._calendar.activeDate, -1);
  }
  nextClicked(mode: 'month' | 'year') {
    this._calendar.activeDate = mode === 'month' ?
      this.customDateAdapter.addCalendarMonths(this._calendar.activeDate, 1) :
      this.customDateAdapter.addCalendarYears(this._calendar.activeDate, 1);
  }
  removeUserChange(){
    const area = this.customDateAdapter.getArea();
    this.customDateAdapter.close();
    return this.forwardPlannerService.removeUserChange(area); 
  }
  ngOnDestroy() {
    this._destroyed.next();
    this._destroyed.complete();
  }

}