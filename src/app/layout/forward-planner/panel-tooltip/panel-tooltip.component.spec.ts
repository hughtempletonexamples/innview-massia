import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelTooltipComponent } from './panel-tooltip.component';

xdescribe('PanelTooltipComponent', () => {
  let component: PanelTooltipComponent;
  let fixture: ComponentFixture<PanelTooltipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanelTooltipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelTooltipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
