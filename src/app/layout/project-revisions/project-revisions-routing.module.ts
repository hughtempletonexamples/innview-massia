import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectRevisionsComponent } from './project-revisions.component';

const routes: Routes = [
  {
      path: '',
      component: ProjectRevisionsComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectRevisionsRoutingModule { }
