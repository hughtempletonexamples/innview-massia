import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProjectsService } from 'src/app/shared/services';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-project-revisions',
  templateUrl: './project-revisions.component.html',
  styleUrls: ['./project-revisions.component.scss']
})
export class ProjectRevisionsComponent implements OnInit {


  @Input() project;

  id: string;

  _subscription: Subscription;

  constructor(private route: ActivatedRoute, private projectsService: ProjectsService) { }
  
  ngOnInit() {
    this.id = this.route.parent.parent.snapshot.paramMap.get('id');

    this._subscription = this.projectsService.getproject(this.id).subscribe((project) => {
      this.project = project;
    });

  }

  onDestroy(){
    this._subscription.unsubscribe();
  }

  routeToOLdInnview(){
    window.location.href="https://innview.firebaseapp.com/newux.html#/projects/"+ this.project.id +"/revisions"
  }

}
