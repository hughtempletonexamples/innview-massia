import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectRevisionsRoutingModule } from './project-revisions-routing.module';
import { ProjectRevisionsComponent } from './project-revisions.component';
import { MatCardModule, MatFormFieldModule,MatIconModule, MatOptionModule,MatSelectModule,MatSliderModule, MatInputModule, MatNativeDateModule, MatDatepickerModule, MatCheckboxModule, MatButtonModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ProjectRevisionsComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatCardModule,
    MatSelectModule,
    MatSliderModule,
    MatFormFieldModule,
    MatOptionModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatNativeDateModule, 
    MatDatepickerModule,
    ProjectRevisionsRoutingModule,
    FlexLayoutModule.withConfig({addFlexToParent: false})
  ],
  exports: [ProjectRevisionsComponent],
})
export class ProjectRevisionsModule { }
