import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Reason, MaterialRequest } from '../../shared/models/models';
import { ProjectsService, AuthService } from '../../shared/services';
import { MatDialog, MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { Subscription } from 'rxjs';
import { MaterialRequestService } from 'src/app/shared/services/material-request.service';
import { DateUtilService } from 'src/app/shared/services/date-util.service';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { CustomDateAdapter } from 'src/app/shared/helpers/custom-adapter-date';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/M/YYYY',
  },
  display: {
    dateInput: 'DD/M/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-material-request-details',
  templateUrl: './material-request-details.component.html',
  styleUrls: ['./material-request-details.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: DateAdapter, useClass: CustomDateAdapter, deps: [MAT_DATE_LOCALE]},

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class MaterialRequestDetailsComponent implements OnInit {

  minDate = new Date();
  materialControl: FormGroup;
  _subscription: Subscription;
  loaded: boolean;
  submitted: boolean;
  projectIds: Array<any>;
  id: string;
  materialRequest: MaterialRequest;
  reasons: Reason[] = [
    {name: "Missing from Delivery", abbreviation: "MD"},
    {name: "Ancillaries Request", abbreviation: "AR"},
    {name: "Variation Order", abbreviation: "VO"},
    {name: "Site Request (Additional to budget)", abbreviation: "SR"},
  ];

  constructor(private router: Router, private dateUtilService: DateUtilService, private route: ActivatedRoute, private materialRequestService: MaterialRequestService, private fb: FormBuilder, public dialog: MatDialog, private projectsService: ProjectsService, private authService: AuthService) { 
    this.loaded = false;
    this.submitted = false;
  }

  ngOnInit(){
    this.id = this.route.snapshot.paramMap.get('id');
    this._subscription = this.materialRequestService.getMaterialRequest(this.id).subscribe((materialRequest) => {
      this.materialRequest = materialRequest;
      this.loaded = true;
      this.setUpForm();
    });
  }

  back(){
    this.router.navigate(['/material-request']);
  }

  setUpForm(){
    this.materialControl = this.fb.group({
      email: new FormControl({ value: this.materialRequest.email || null, disabled: true }, [Validators.required]),
      person: new FormControl({ value: this.materialRequest.person || null, disabled: true }, [Validators.required]),
      projectId: new FormControl({ value: this.materialRequest.projectId || null, disabled: true }, [Validators.required]),
      projectName: new FormControl({ value: this.materialRequest.projectName || null, disabled: true }, [Validators.required]),
      reason: new FormControl({ value: this.materialRequest.reason || null, disabled: true }, [Validators.required]),
      dateRequired: new FormControl({ value: this.materialRequest.dateRequired || null, disabled: true }, [Validators.required]),
      materialRequired: new FormControl({ value: this.materialRequest.materialRequired || null, disabled: this.materialRequest.action }, [Validators.required]),
      mainContractor: new FormControl({ value: this.materialRequest.mainContractor || null, disabled: this.materialRequest.action }),
      siteContact: new FormControl({ value: this.materialRequest.siteContact || null, disabled: this.materialRequest.action }),
      siteContactNo: new FormControl({ value: this.materialRequest.siteContactNo || null, disabled: this.materialRequest.action }),
      qsName: new FormControl({ value: this.materialRequest.qsName || null, disabled: this.materialRequest.action }),
      qsAuth: new FormControl({ value: this.materialRequest.qsAuth || null, disabled: this.materialRequest.action }),
      action: new FormControl({ value: this.materialRequest.action || null, disabled: this.materialRequest.action }),
      deliveryDate: new FormControl({ value: this.materialRequest.deliveryDate || null, disabled: this.materialRequest.action }),
      note: new FormControl({ value: this.materialRequest.note || null, disabled: this.materialRequest.action })
    });
  }

  onSubmit(): void{
    this.submitted = true;
    let updateObj = this.materialControl.getRawValue();
    updateObj = Object.assign(this.materialRequest, updateObj);
    updateObj.dateRequired = moment.isMoment(updateObj.dateRequired) ? this.dateUtilService.toIsoDate(updateObj.dateRequired._d) : updateObj.dateRequired;
    updateObj.deliveryDate = moment.isMoment(updateObj.deliveryDate) ? this.dateUtilService.toIsoDate(updateObj.deliveryDate._d) : updateObj.deliveryDate;
    this.materialRequestService.updateMaterialRequest(this.id, updateObj)
    .then(()=>{
      this.router.navigate(['/material-request']);
    });
  }
}