import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MaterialRequestDetailsComponent } from './material-request-details.component';

const routes: Routes = [
  {
      path: '',
      component: MaterialRequestDetailsComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaterialRequestDetailsRoutingModule { }
