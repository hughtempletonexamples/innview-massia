import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs';
import { ProjectsService, AreasService, ForwardPlannerService } from 'src/app/shared/services';
import { MatSnackBar, MatTableDataSource } from '@angular/material';
import { DateUtilService } from 'src/app/shared/services/date-util.service';
import { FormControl } from '@angular/forms';
import { ProjectStat } from 'src/app/shared/models/models';

@Component({
  selector: 'app-project-status',
  templateUrl: './project-status.component.html',
  styleUrls: ['./project-status.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProjectStatusComponent implements OnInit {

  _subscription: Subscription;
  _subscription2: Subscription;
  projects: Array<any>;
  projectsStatus: ProjectStat[];
  loaded: boolean;
  unfilteredData : Array<any>;
  sortedData:any;
  projectStatus: object;
  loadedCount: number;
  selected = new FormControl(0);
  selectedProject: object;
  selectedArea: object;
  filterValue: string;
  dataSource: MatTableDataSource<ProjectStat>;

  constructor(private _snackBar: MatSnackBar, 
    private plannerService: ForwardPlannerService, 
    private projectsService: ProjectsService) {
      this.projects = [];
      this.projectsStatus = [];
      this.projectStatus = {};
      this.loadedCount = 0;
      this.filterValue = "";
      this.dataSource = new MatTableDataSource<ProjectStat>([]);
      this.loaded = false;
  }

  ngOnInit() {
    this._subscription = this.projectsService.getActiveProjectsList().subscribe(projects => {
      this.sortBySiteStart(projects);
      this.projects = projects;
        this.loadedCount = 0;
        this.plannerService.setProjectStats(this.projects[this.loadedCount]);
        this._snackBar.open('Loading Project Stats!', 'Hide', {
          panelClass: ['innviewSnack']
        });
    });
    this._subscription2 = this.plannerService.retrieveProjectStats().subscribe((projectStats)=>{
      this.loadedCount++;
      if(this.projects.length === this.loadedCount){
        this._snackBar.dismiss();
      }
      if(this.projects.length && this.loadedCount < this.projects.length){
        this.plannerService.setProjectStats(this.projects[this.loadedCount]);
      }
      if(this.projects.length && this.loadedCount <= this.projects.length){
        if(projectStats){
          const index = this.projectsStatus.findIndex((e) => e.projectId === projectStats.projectId);
          if (index === -1) {
            this.projectsStatus.push(projectStats);
          } else {
            this.projectsStatus[index] = projectStats;
          }
          this.sortByDeliveryDate(this.projectsStatus);
          this.dataSource = new MatTableDataSource<ProjectStat>(this.projectsStatus);
          this.loaded = true;
        }
      }
    });

  }

  projectTabSelected(){
    if(this.projects.length && this.loadedCount < this.projects.length){
      this.plannerService.setProjectStats(this.projects[this.loadedCount]);
    }
  }

  getArea(stats){
    this._snackBar.dismiss();
    this.selectedArea = null;
    this.selectedProject = stats
    this.selected.setValue(1);
  }

  selectPanel(areaStatus){
    this.selectedArea = areaStatus;
    this.selected.setValue(2);
  }

  sortByDeliveryDate(items) {
    items.sort(function (a, b) {
      if (a.delivery < b.delivery) {
        return 1;
      } else if (a.delivery > b.delivery) {
        return -1;
      } else {
        return 0;
      }
    });
  }

  sortBySiteStart(items) {
    items.sort(function (a, b) {
      if (a.siteStart < b.siteStart) {
        return -1;
      } else if (a.siteStart > b.siteStart) {
        return 1;
      } else {
        return 0;
      }
    });
  }

  ngOnDestroy(){
    this._subscription.unsubscribe();
    this._subscription2.unsubscribe();
  }

  applyFilter(filterValue: string) {
    this.dataSource.data = this.projectsStatus;
    filterValue = filterValue.trim().toLowerCase(); // Remove whitespace
    this.filterValue = filterValue;
    this.dataSource.filter = filterValue;
  }

}
