import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectStatusRoutingModule } from './project-status-routing.module';
import { ProjectStatusComponent } from './project-status.component';
import { MatToolbarModule, MatSortModule, MatTableModule, MatFormFieldModule, MatPaginatorModule, MatIconModule, MatInputModule, MatGridListModule, MatSnackBarModule, MatTabsModule } from '@angular/material';
import { UserFromRefModule } from 'src/app/shared/modules/user-from-ref/user-from-ref.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';
import { ProjectStatusDetailsModule } from 'src/app/shared/modules/project-status-details/project-status-details.module';
import { AreaStatusDetailsModule } from 'src/app/shared/modules/area-status-details/area-status-details.module';
import { PanelStatusDetailsModule } from 'src/app/shared/modules/panel-status-details/panel-status-details.module';

@NgModule({
  declarations: [ProjectStatusComponent],
  imports: [
    CommonModule,
    ProjectStatusRoutingModule,
    MatToolbarModule,
    MatSortModule,
    MatTableModule,
    MatFormFieldModule,
    MatGridListModule,
    MatTabsModule,
    MatPaginatorModule,
    MatSnackBarModule,
    MatInputModule,
    MatIconModule,
    UserFromRefModule,
    PipesModule,
    ProjectStatusDetailsModule,
    AreaStatusDetailsModule,
    PanelStatusDetailsModule,
    FlexLayoutModule.withConfig({addFlexToParent: false})
  ]
})
export class ProjectStatusModule { }
