import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Project, Reason } from '../../shared/models/models';
import { ProjectsService, AuthService } from '../../shared/services';
import { MatDialog, MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';

import { Subscription, combineLatest, Observable } from 'rxjs';
import { ReferenceDataService } from '../../shared/services/reference-data.service';
import { startWith, map } from 'rxjs/operators';
import { MaterialRequestService } from '../../shared/services/material-request.service';
import { Router } from '@angular/router';
import { DateUtilService } from 'src/app/shared/services/date-util.service';
import { CustomDateAdapter } from 'src/app/shared/helpers/custom-adapter-date';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/M/YYYY',
  },
  display: {
    dateInput: 'DD/M/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-material-request-create',
  templateUrl: './material-request-create.component.html',
  styleUrls: ['./material-request-create.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: DateAdapter, useClass: CustomDateAdapter, deps: [MAT_DATE_LOCALE]},

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class MaterialRequestCreateComponent implements OnInit {

  twoDays: string;
  minDate = new Date();
  materialControl: FormGroup;
  projects: Project[];
  _subscription: Subscription;
  loaded: boolean;
  submitted: boolean;
  currentUserEmail: string;
  currentUserName: string;
  projectIds: Array<any>;
  filteredOptions: Observable<string[]>;
  reasons: Reason[] = [
    {name: "Missing from Delivery", abbreviation: "MD"},
    {name: "Ancillaries Request", abbreviation: "AR"},
    {name: "Variation Order", abbreviation: "VO"},
    {name: "Site Request (Additional to budget)", abbreviation: "SR"},
  ];

  constructor( private dateUtilService: DateUtilService, private router: Router, private referenceDataService: ReferenceDataService, private fb: FormBuilder, public dialog: MatDialog, private projectsService: ProjectsService, private authService: AuthService, private materialRequestService: MaterialRequestService) { 
    this.twoDays = this.dateUtilService.addWorkingDays(this.minDate, 2);
    this.minDate = new Date(this.twoDays);
    this.loaded = false;
    this.submitted =false
  }

  ngOnInit(){

    this._subscription = combineLatest(this.referenceDataService.getUser(this.authService.currentUserId), this.projectsService.getProjectsList()).subscribe(([user, projects]) => {
      this.projectIds = [];

      projects.forEach((project)=>{
        this.projectIds.push(project.id);
      });

      this.projects = projects;
      this.currentUserName = user.name;
      this.currentUserEmail = user.email;
      this.setUpForm();

      this.filteredOptions = this.materialControl.get('projectId').valueChanges.pipe(
        startWith(''),
        map((value) => {
          if(value === ""){
            this.materialControl.get('projectName').setValue(null);
          }
          const filteredArray = this._filter(value);
          if(filteredArray.length === 1){
            this.projects.forEach((project)=>{
              if(project.id === filteredArray[0]){
                this.materialControl.get('projectName').setValue(project.name);
              }
            });
          }else{
            this.materialControl.get('projectName').setValue(null);
          }
          return filteredArray;
        })
      );

      this.loaded = true;
    });
  }

  setUpForm(){
    this.materialControl = this.fb.group({
      email: new FormControl({ value: this.currentUserEmail, disabled: true }, [Validators.required]),
      person: new FormControl({ value: this.currentUserName, disabled: true }, [Validators.required]),
      projectId: new FormControl(null, [Validators.required]),
      projectName: new FormControl({ value: null, disabled: true }, [Validators.required]),
      reason: new FormControl(null, [Validators.required]),
      dateRequired: new FormControl(null, [Validators.required]),
      materialRequired: new FormControl(null, [Validators.required]),
      mainContractor: new FormControl(null),
      siteContact: new FormControl(null),
      siteContactNo: new FormControl(null)
    });
  }

  private _filter(value: string): string[] {
    return this.projectIds.filter(option => option.indexOf(value) === 0);
  }

  back(){
    this.router.navigate(['/material-request']);
  }

  onSubmit(): void{
    this.submitted = true;
    let updateObj = this.materialControl.getRawValue();
    updateObj.dateRequired = this.dateUtilService.toIsoDate(updateObj.dateRequired._d);
    this.materialRequestService.createMaterialRequest(updateObj)
    .then(()=>{
      this.router.navigate(['/material-request']);
    });
  }
}