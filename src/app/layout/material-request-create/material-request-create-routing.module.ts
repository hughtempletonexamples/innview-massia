import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MaterialRequestCreateComponent } from './material-request-create.component';

const routes: Routes = [
  {
      path: '',
      component: MaterialRequestCreateComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaterialRequestCreateRoutingModule { }
