import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialRequestRoutingModule } from './material-request-routing.module';
import { MaterialRequestComponent } from './material-request.component';
import { MatToolbarModule, MatSortModule, MatTableModule, MatFormFieldModule, MatInputModule, MatPaginatorModule, MatIconModule, MatCheckboxModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TextDialogModule } from 'src/app/shared/modules/text-dialog/text-dialog.module';

@NgModule({
  declarations: [MaterialRequestComponent],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatSortModule,
    MatTableModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatInputModule,
    MatIconModule,
    TextDialogModule,
    FlexLayoutModule.withConfig({addFlexToParent: false}),
    MaterialRequestRoutingModule
  ]
})
export class MaterialRequestModule { }
