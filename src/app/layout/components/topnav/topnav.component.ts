import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { AuthService } from '../../../shared/services';

@Component({
    selector: 'app-topnav',
    templateUrl: './topnav.component.html',
    styleUrls: ['./topnav.component.scss']
})
export class TopnavComponent implements OnInit {
    public pushRightClass: string;
    public containerOut: string;
    public minimiseSidebar: string;

    constructor(public authService: AuthService, public router: Router, private translate: TranslateService) {
        this.router.events.subscribe(val => {
            if (val instanceof NavigationEnd && window.innerWidth <= 992 && this.isToggled()) {
                this.toggleSidebar();
            }
        });
    }

    ngOnInit() {
        this.pushRightClass = 'push-right';
        this.containerOut = 'container-out';
        this.minimiseSidebar = 'minimise-sidebar';
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    isMinimiseToggled(): boolean {
        const dom: Element = document.querySelector('.main-container');
        return dom.classList.contains(this.containerOut);
    }

    toggleMinimiseSidebar() {
        const dom: any = document.querySelector('.main-container');
        dom.classList.toggle(this.containerOut);
        const sideBar: any = document.querySelector('#sidebar');
        sideBar.classList.toggle(this.minimiseSidebar);
    }

    onLoggedout() {
        this.authService.signOut();
        localStorage.removeItem('isLoggedin');
        this.router.navigate(['/login']);
    }

    changeLang(language: string) {
        this.translate.use(language);
    }


}
