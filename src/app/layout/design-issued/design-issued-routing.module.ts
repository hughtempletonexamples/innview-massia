import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DesignIssuedComponent } from './design-issued.component';

const routes: Routes = [
    {
        path: '',
        component: DesignIssuedComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DesignIssuedRoutingModule {}
