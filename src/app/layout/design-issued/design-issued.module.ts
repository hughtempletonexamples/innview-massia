import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DesignIssuedComponent } from './design-issued.component';
import { MatGridListModule, MatCardModule } from '@angular/material';
import { DesignIssuedRoutingModule } from './design-issued-routing.module';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';

@NgModule({
  declarations: [DesignIssuedComponent],
  imports: [
    CommonModule,
    DesignIssuedRoutingModule,
    MatGridListModule,
    MatCardModule,
    PipesModule
  ]
})
export class DesignIssuedModule { }
