import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentDateComponent2 } from './current-date.component2';

xdescribe('CurrentDateComponent', () => {
  let component: CurrentDateComponent2;
  let fixture: ComponentFixture<CurrentDateComponent2>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentDateComponent2 ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentDateComponent2);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
