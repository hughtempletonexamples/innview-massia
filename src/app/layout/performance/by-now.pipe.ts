import {Pipe,PipeTransform,Input} from "@angular/core";
import { formatDate } from "@angular/common"

@Pipe({name:"byNow"})

export class byNowPipe implements PipeTransform {
      
      transform(now, target):number {

        target = parseInt(target, 0);

        let byNow = 0;
        now = now.getTime();
        let start = new Date (new Date().setHours(8,0,0,0)).getTime();
        let halfThree = new Date(new Date().setHours(15,30,0,0)).getTime();
        
        if(now > halfThree){
          byNow = target;
        }
        
        if(now >= start && now <= halfThree){
          let portionOfDay = now - start;
          let portionOfDayFormatted = new Date(portionOfDay);
          let portionOfDayMinutes = (portionOfDayFormatted.getHours() * 60);
          let minutes = portionOfDayFormatted.getMinutes();
          let cal = (portionOfDayMinutes + minutes) / 510;
          byNow = target * cal;
        }
        
        if (byNow > target){
          byNow = target;
        }
        return Math.round(byNow);          
        
      }

        
      }