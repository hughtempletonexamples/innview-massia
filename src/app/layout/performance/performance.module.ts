import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatGridListModule, MatCardModule,MatFormFieldModule, MatDatepickerModule, MatIconModule, MatInputModule } from '@angular/material';
import { PerformanceRoutingModule } from './performance-routing.module';
import { PerformanceComponent } from './performance.component';
import { byNowPipe } from './by-now.pipe';
import { CurrentDateComponent2 } from './current-date2/current-date.component2';
import { MatMomentDateModule } from '@angular/material-moment-adapter';

@NgModule({
  imports: [
      CommonModule, 
      PerformanceRoutingModule, 
      MatGridListModule,
      MatCardModule,
      MatDatepickerModule,
      MatMomentDateModule,
      MatFormFieldModule,
      MatInputModule,
      MatIconModule
  ],
  declarations: [PerformanceComponent, byNowPipe, CurrentDateComponent2]
})
export class PerformanceModule { }
