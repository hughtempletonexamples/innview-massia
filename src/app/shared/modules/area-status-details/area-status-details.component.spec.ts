import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreaStatusDetailsComponent } from './area-status-details.component';

describe('AreaStatusDetailsComponent', () => {
  let component: AreaStatusDetailsComponent;
  let fixture: ComponentFixture<AreaStatusDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreaStatusDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreaStatusDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
