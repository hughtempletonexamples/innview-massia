import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeoRouteComponent } from './geo-route.component';
import { FormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule} from 'agm-direction';
import { environment } from '../../../../environments/environment';
import { GeocodeService } from '../../services/geocode.service';

@NgModule({
  declarations: [GeoRouteComponent],
  imports: [
    CommonModule,
    FormsModule,
    AgmCoreModule.forRoot({ //@agm/core
      apiKey: environment.googleMaps.apiKey
    }),
    AgmDirectionModule //agm-direction
  ],
  providers: [GeocodeService],
  exports: [GeoRouteComponent]
})
export class GeoRouteModule { }
