import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { NgModel } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { Location } from '../../models/models';
import { GeocodeService } from '../../services/geocode.service';

@Component({
  selector: 'app-geo-route',
  templateUrl: './geo-route.component.html',
  styleUrls: ['./geo-route.component.scss']
})
export class GeoRouteComponent implements OnInit {

  address = 'London';
  location: Location;
  loading: boolean;

  constructor(
    private geocodeService: GeocodeService,
    private ref: ChangeDetectorRef,
  ) {}
  
  ngOnInit() {
    this.showLocation();
  }

  showLocation() {
    this.addressToCoordinates();
  }

  addressToCoordinates() {
    this.loading = true;
    this.geocodeService.geocodeAddress(this.address)
    .subscribe((location: Location) => {
        this.location = location;
        this.loading = false;
        this.ref.detectChanges();  
      }      
    );     
  }
}