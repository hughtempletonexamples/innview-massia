import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FileUploaderComponent } from './file-uploader.component';
import {FileUploadModule} from 'ng2-file-upload';
import { MatIconModule, MatButtonModule } from '@angular/material';

@NgModule({
  declarations: [FileUploaderComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule,
    FileUploadModule
  ],
  exports: [FileUploaderComponent]
})
export class FileUploaderModule { }
