import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectStatusDetailsComponent } from './project-status-details.component';

xdescribe('ProjectStatusDetailsComponent', () => {
  let component: ProjectStatusDetailsComponent;
  let fixture: ComponentFixture<ProjectStatusDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectStatusDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectStatusDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
