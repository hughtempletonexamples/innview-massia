import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DateUtilService } from '../../services/date-util.service';
import { MatDatepickerInputEvent, DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material';
import { CustomDateAdapter } from '../../helpers/custom-adapter-date';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD-MMM',
  },
  display: {
    dateInput: 'DD-MMM',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: DateAdapter, useClass: CustomDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ]
})
export class DatePickerComponent {

  todayDate = new Date();
  @Output() currentDateChange = new EventEmitter<string>();
  
  constructor(private dateUtilService: DateUtilService) {}

  addEvent(type: string, event: MatDatepickerInputEvent<any>) {
    const date = this.dateUtilService.toIsoDate(event.value);
    this.currentDateChange.emit(date);
  }

}
