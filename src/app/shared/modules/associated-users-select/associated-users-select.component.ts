import { Component, OnInit, OnDestroy, Input, AfterViewInit } from '@angular/core';
import { ReferenceDataService } from '../../../shared/services/reference-data.service';
import { Observable, Subscription } from 'rxjs';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-associated-users-select',
  templateUrl: './associated-users-select.component.html',
  styleUrls: ['./associated-users-select.component.scss']
})
export class AssociatedUsersSelectComponent implements OnInit {

  @Input() variant: string;
  @Input() required: boolean;
  @Input() name: string;
  @Input() value: string;
  @Input() disableSelect: boolean;
  @Input() ctr: FormGroup;

  users: object;
  loadedUser:string;
  selectOptions: string;
  _subscription: Subscription;

  constructor(private referenceDataService: ReferenceDataService) {
    this.users = [];
  }

  ngOnInit() {
    this._subscription = this.referenceDataService.getUsers().subscribe((users) => {
      this.users = users;
    });
  }

  compareObjects(o1: any, o2: any): boolean {
    if(o1 != undefined && o2 != undefined){
      return o1.name === o2.name && o1.$key === o2.id;
    }
    
  }

  ngOnDestroy(){
    this._subscription.unsubscribe();
  }
}
