import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssociatedUsersSelectComponent } from './associated-users-select.component';

xdescribe('AssociatedUsersSelectComponent', () => {
  let component: AssociatedUsersSelectComponent;
  let fixture: ComponentFixture<AssociatedUsersSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssociatedUsersSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssociatedUsersSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
