import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreaPopupDialogComponent } from './area-popup-dialog.component';

xdescribe('AreaPopupDialogComponent', () => {
  let component: AreaPopupDialogComponent;
  let fixture: ComponentFixture<AreaPopupDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreaPopupDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreaPopupDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
