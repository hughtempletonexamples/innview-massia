import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AreasService, ComponentsService } from 'src/app/shared/services';
import { ReferenceDataService } from 'src/app/shared/services/reference-data.service';
import { SrdComponent } from 'src/app/shared/models/models';
import { CustomDateAdapter } from 'src/app/shared/helpers/custom-adapter-date';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/M/YYYY',
  },
  display: {
    dateInput: 'DD/M/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-area-popup-dialog',
  templateUrl: './area-popup-dialog.component.html',
  styleUrls: ['./area-popup-dialog.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: DateAdapter, useClass: CustomDateAdapter, deps: [MAT_DATE_LOCALE]},

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})


export class AreaPopupDialogComponent implements OnInit{

  minDate = new Date();
  areaControl: FormGroup;
  submitted: boolean;

  constructor(
    private areasService: AreasService,
    private componentsService: ComponentsService,
    private referenceDataService : ReferenceDataService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<AreaPopupDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
      this.submitted = false;
    }

  ngOnInit(){
    this.setUpForm();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  setUpForm(){
    if(this.data.type === "edit"){
      this.areaControl = this.fb.group({
        phase: new FormControl({ value: this.data.area.phase, disabled: true}, [Validators.required]),
        floor: new FormControl({ value: this.data.area.floor, disabled: true}, [Validators.required]),
        supplier: new FormControl(this.data.area.supplier, [Validators.required]),
        offloadMethod: new FormControl(this.data.area.offloadMethod, [Validators.required]),
        component: new FormControl({value: this.data.area.componentName, disabled: true}, [Validators.required]),
        description: new FormControl(this.data.area.description, [Validators.required]),
        comments: new FormControl(this.data.area.notes),
        numberOfPanels: new FormControl(this.data.area.numberOfPanels),
        areaOfPanels: new FormControl(this.data.area.areaOfPanels),
        deliveryDate: new FormControl(this.data.area.revisions[this.data.project.deliverySchedule.unpublished], [Validators.required])
      });
    }else{
      this.areaControl = this.fb.group({
        phase: new FormControl(null, [Validators.required]),
        floor: new FormControl(null, [Validators.required]),
        supplier: new FormControl(null, [Validators.required]),
        offloadMethod: new FormControl(null, [Validators.required]),
        component: new FormControl(null, [Validators.required]),
        description: new FormControl(null, [Validators.required]),
        comments: new FormControl(null),
        deliveryDate: new FormControl(null, [Validators.required])
      });
    }
  }

  onSubmit() {

    this.submitted = true;

    if(!this.areaControl.invalid){
      if(this.data.type === "edit"){
        let key = this.data.area.$key;
        let updateObj = this.areaControl.value;
        let deliveryDate = this.areaControl.value.deliveryDate;
        this.data.area.revisions[this.data.project.deliverySchedule.unpublished] = deliveryDate;
        delete updateObj.deliveryDate;
        delete updateObj.component;
        let area = Object.assign(this.data.area, updateObj);
        delete area.$key;
        return this.areasService.updateArea(key,area)
        .then(()=>{
          this.dialogRef.close();
        });
      }else{
        let newObj = this.areaControl.value;
        let deliveryDate = this.areaControl.value.deliveryDate;
        return this.referenceDataService.getItem("components", newObj.component)
        .then((component: SrdComponent)=>{
          let saves = [];
          Object.keys(component.productTypes).map((productType)=>{
            newObj.project = this.data.project.$key;
            newObj.type = productType;
            let area = Object.assign({}, newObj);
            delete area.deliveryDate;
            delete area.component;
            const newArea = this.areasService.createAreaData(this.data.project, area, deliveryDate);
            saves.push(this.areasService.createArea(newArea));
          });
          return Promise.all(saves);
        })
        .then((createdAreas)=>{
          const component = this.componentsService.createComponentData(newObj.project, createdAreas, newObj.component);
          return this.componentsService.create(component);
        })
        .then(()=>{
          this.dialogRef.close();
        });
      }
    }  

  }

}
