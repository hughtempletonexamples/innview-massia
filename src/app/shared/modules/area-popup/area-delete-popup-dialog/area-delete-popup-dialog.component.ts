import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { RevisionsService } from 'src/app/shared/services/revisions.service';

@Component({
  selector: 'app-area-delete-popup-dialog',
  templateUrl: './area-delete-popup-dialog.component.html',
  styleUrls: ['./area-delete-popup-dialog.component.scss']
})
export class AreaDeletePopupDialogComponent {

  constructor(
    private revisions : RevisionsService,
    public dialogRef: MatDialogRef<AreaDeletePopupDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {}

  action(): void {
    if(this.data.status === "UPLOADED" || this.data.status === "NOPANELS"){
      this.revisions.deleteAreaProcess(this.data.area)
      // .then(()=>{
      //   this.areasService.calculateEstimatedAreasAndPanels(this.data.project);
      // })
      .then(()=>{
        this.dialogRef.close();
      });
    }else{
      this.dialogRef.close();
    }
  }

}
