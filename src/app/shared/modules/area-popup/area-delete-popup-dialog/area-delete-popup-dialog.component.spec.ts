import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreaDeletePopupDialogComponent } from './area-delete-popup-dialog.component';

xdescribe('AreaDeletePopupDialogComponent', () => {
  let component: AreaDeletePopupDialogComponent;
  let fixture: ComponentFixture<AreaDeletePopupDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreaDeletePopupDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreaDeletePopupDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
