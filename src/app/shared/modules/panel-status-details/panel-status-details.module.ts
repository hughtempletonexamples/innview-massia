import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelStatusDetailsComponent } from './panel-status-details.component';
import { MatTableModule, MatCheckboxModule, MatToolbarModule, MatSortModule, MatFormFieldModule, MatPaginatorModule, MatInputModule, MatIconModule, MatCardModule, MatProgressBarModule, MatButtonModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DatePickerModule } from '../date-picker/date-picker.module';
import { ConfirmationPopupDialogComponent } from './confirmation-popup-dialog/confirmation-popup-dialog.component';

@NgModule({
  declarations: [PanelStatusDetailsComponent, ConfirmationPopupDialogComponent],
  imports: [
    CommonModule,
    MatCardModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatSortModule,
    MatButtonModule,
    MatTableModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatInputModule,
    MatIconModule,
    MatProgressBarModule,
    DatePickerModule,
    FlexLayoutModule.withConfig({addFlexToParent: false})
  ],
  exports: [
    PanelStatusDetailsComponent
  ],
  entryComponents: [ConfirmationPopupDialogComponent]
})
export class PanelStatusDetailsModule { }
