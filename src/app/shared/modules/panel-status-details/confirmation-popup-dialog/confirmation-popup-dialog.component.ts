import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { PanelStatusDetailsComponent } from '../panel-status-details.component';

@Component({
  selector: 'app-confirmation-popup-dialog',
  templateUrl: './confirmation-popup-dialog.component.html',
  styleUrls: ['./confirmation-popup-dialog.component.scss']
})
export class ConfirmationPopupDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<PanelStatusDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data) { }

  onNoClick(): void {
    this.dialogRef.close({buttonClicked: false});
  }

  confirmation(){
    this.dialogRef.close({buttonClicked: true});
  }

}
