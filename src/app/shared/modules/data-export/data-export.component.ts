import { Component, Input } from '@angular/core';
import { ExportService } from '../../services/export.service';

@Component({
  selector: 'app-data-export',
  providers: [ExportService],
  templateUrl: './data-export.component.html',
  styleUrls: ['./data-export.component.scss']
})
export class DataExportComponent {

  XLSX_MIMETYPE: string;

  @Input() data: Array<any>;

  constructor(private exportsService: ExportService) {
    this.XLSX_MIMETYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
  }

  exportAsXLSX():void {
    this.exportsService.exportAsExcelFile(this.data, 'production');
  }
  
}
