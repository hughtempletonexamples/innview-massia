import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataExportComponent } from './data-export.component';
import { MatButtonModule, MatIconModule } from '@angular/material';

@NgModule({
  declarations: [DataExportComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
  ],
  exports: [DataExportComponent]
})
export class DataExportModule { }
