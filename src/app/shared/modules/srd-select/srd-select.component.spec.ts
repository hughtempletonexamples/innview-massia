import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SrdSelectComponent } from './srd-select.component';

xdescribe('SrdSelectComponent', () => {
  let component: SrdSelectComponent;
  let fixture: ComponentFixture<SrdSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SrdSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SrdSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
