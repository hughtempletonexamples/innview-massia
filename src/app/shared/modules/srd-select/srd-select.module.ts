import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material';
import { SrdSelectComponent } from './srd-select.component';

@NgModule({
  declarations: [SrdSelectComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatSelectModule
  ],
  exports: [SrdSelectComponent]
})
export class SrdSelectModule { }
