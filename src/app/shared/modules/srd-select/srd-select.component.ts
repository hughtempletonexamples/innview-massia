import { Component, OnInit, Input } from '@angular/core';
import { ReferenceDataService } from '../../../shared/services/reference-data.service';
import { Observable, Subscription } from 'rxjs';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-srd-select',
  templateUrl: './srd-select.component.html',
  styleUrls: ['./srd-select.component.scss']
})
export class SrdSelectComponent implements OnInit {

  @Input() variant: string;
  @Input() required: boolean;
  @Input() name: string;
  @Input() value: string;
  @Input() disableSelect: boolean;
  @Input() ctr: FormGroup;

  items: object;
  selectOptions: string;
  _subscription: Subscription;

  constructor(private referenceDataService: ReferenceDataService) {}

  ngOnInit() {
    this._subscription = this.getReferenceData(this.variant).subscribe((items) => {
      items.sort(function(a, b){
        return a['seq'] - b['seq'];
      });
      this.items = items;
      console.log(this.variant);
    });
  }

  ngOnDestroy(){
    this._subscription.unsubscribe();
  }

  getReferenceData(variant){
    switch (variant) {
    case "chainOfCustody":
      return this.referenceDataService.getChainOfCustodyTypes();
    case "datumType":
      return this.referenceDataService.getDatumTypes();
    case "component":
      return this.referenceDataService.getComponents();
    case "siteAccess":
      return this.referenceDataService.getVehicleTypes();
    case "phase":
      return this.referenceDataService.getPhases();
    case "floor":
      return this.referenceDataService.getFloors();
    case "productType":
      return this.referenceDataService.getProductTypes();
    case "supplier":
      return this.referenceDataService.getSuppliers();
    case "offloadMethod":
      return this.referenceDataService.getOffloadMethods();
    case "gateway":
      return this.referenceDataService.getGateways();
    case "team":
      return this.referenceDataService.getTeams();
    case "installer":
      return this.referenceDataService.getInstallers();
    default:
      console.log("Unknown reference data type '" + variant + "'");
    }
  }
  

}
