import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts';

import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';

import { StackedGroupedChartComponent } from './stacked-grouped-chart.component';

@NgModule({
    imports: [
        CommonModule,
        ChartsModule,
        MatCardModule,
        MatGridListModule,
        FlexLayoutModule.withConfig({addFlexToParent: false})
    ],
    exports: [StackedGroupedChartComponent],
    declarations: [StackedGroupedChartComponent]
})
export class StackedGroupedChartModule { }
