import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StackedGroupedChartComponent } from './stacked-grouped-chart.component';

xdescribe('StackedGroupedChartComponent', () => {
  let component: StackedGroupedChartComponent;
  let fixture: ComponentFixture<StackedGroupedChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StackedGroupedChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StackedGroupedChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
