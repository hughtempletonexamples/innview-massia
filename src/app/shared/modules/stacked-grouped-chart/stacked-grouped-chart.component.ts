import { Component, OnInit, Input } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Color } from 'ng2-charts';



@Component({
  selector: 'app-stacked-grouped-chart',
  templateUrl: './stacked-grouped-chart.component.html',
  styleUrls: ['./stacked-grouped-chart.component.scss']
})

export class StackedGroupedChartComponent implements OnInit {

  @Input() chartData: object;
  @Input() chartColours: object;
  @Input() title: string;

  public barChartOptions: ChartOptions = {
    responsive: true,
    tooltips: {
      enabled: true
    },
    scales: {
      xAxes: [{
        stacked: true,
        scaleLabel: {
          display: true,
          labelString: 'Weeks'
        }
      }],
      yAxes: [{
        stacked: true,
        scaleLabel: {
          display: true,
          labelString: '(m2)'
       }
      }]
    },
    // We use these empty structures as placeholders for dynamic theming.scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        display: function(context) {
          return context.dataset.data[context.dataIndex] > 15;
        },
        anchor: 'center',
        align: 'center',
        font: {
          size: 10,
        },
        labels: {
          value: {
            color: 'white'
          }
        },
        formatter: Math.round
      }
    }
  };

  

  public barChartLabels: string[] = [];
  public barChartType: ChartType;
  public barChartLegend: boolean;

  public barChartPlugins = [pluginDataLabels];

  public barChartColors: Color[] = []

  public barChartData: ChartDataSets[] = [];


  loaded: boolean;

  constructor() {
    this.loaded = false;
  }

  ngOnInit() {

    this.barChartType = 'bar';
    this.barChartLegend = true;

    Object.keys(this.chartData).forEach((y) => {

      let dataObj = { data: [], label: y, stack: 'a' };
      let colour = { backgroundColor: this.chartColours[y], borderColor: '#fff' };

      Object.keys(this.chartData[y]).forEach((x) => {
        if(this.barChartLabels.indexOf(x) === -1){
          this.barChartLabels.push(x);
        }
        let value = this.chartData[y][x];
        dataObj.data.push(Math.round(value));
      });

      this.barChartColors.push(colour);
      this.barChartData.push(dataObj);
      
    });

    this.loaded = true;
    
  }

  chartClicked(e: any): void {
      console.log(e);
  }

  chartHovered(e: any): void {
      console.log(e);
  }

}
