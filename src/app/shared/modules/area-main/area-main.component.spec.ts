import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreaMainComponent } from './area-main.component';

xdescribe('AreaMainComponent', () => {
  let component: AreaMainComponent;
  let fixture: ComponentFixture<AreaMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreaMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreaMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
