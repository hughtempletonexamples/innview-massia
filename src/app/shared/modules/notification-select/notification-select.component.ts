import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ReferenceDataService } from '../../services/reference-data.service';
import { Subscription } from 'rxjs';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-notification-select',
  templateUrl: './notification-select.component.html',
  styleUrls: ['./notification-select.component.scss']
})
export class NotificationSelectComponent implements OnInit {

  @Input() ctr: FormGroup;
  @Input() name: string;
  @Input() userKeys: Array<any>;

  users: object;
  selectOptions: string;
  _subscription: Subscription;

  constructor(private referenceDataService: ReferenceDataService) {
    this.users = [];
  }

  ngOnInit() {
    this._subscription = this.referenceDataService.getUsers().subscribe((users) => {
      this.users = users;
    });
  }

  ngOnDestroy(){
    this._subscription.unsubscribe();
  }
}
