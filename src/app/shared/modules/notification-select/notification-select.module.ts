import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationSelectComponent } from './notification-select.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule, MatFormFieldModule } from '@angular/material';

@NgModule({
  declarations: [NotificationSelectComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatFormFieldModule
  ],
  exports: [NotificationSelectComponent]
})
export class NotificationSelectModule { }
