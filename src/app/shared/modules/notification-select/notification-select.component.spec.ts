import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationSelectComponent } from './notification-select.component';

xdescribe('NotificationSelectComponent', () => {
  let component: NotificationSelectComponent;
  let fixture: ComponentFixture<NotificationSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
