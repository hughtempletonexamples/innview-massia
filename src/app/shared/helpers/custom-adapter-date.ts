import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter } from '@angular/material';
import { Injectable } from '@angular/core';


@Injectable()

export class CustomDateAdapter extends MomentDateAdapter {
  DateAdapter:DateAdapter<any>;
  activeDate:any;
  picker:any;
  area:any;
    getFirstDayOfWeek(): number {
     return 1;
    }
    setArea(area){
      this.area = area;
    }
  
    getArea(){
      return this.area;
    }
    setPicker(picker){
      this.picker = picker;
    }
    close(){
      return this.picker.close();
    }

  }