import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'numbersFromString'
})
export class NumbersFromStringPipe implements PipeTransform {

  transform(value: string, args?: any): any {
    return value.replace(/[0-9]/g, "");;
  }

}
