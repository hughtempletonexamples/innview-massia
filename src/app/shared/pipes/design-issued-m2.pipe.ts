import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'designIssuedM2'
})
export class DesignIssuedM2Pipe implements PipeTransform {

  transform(m2: number, count: number): any {
    if(m2 !== undefined && count !== undefined){
      if(count <= 0){
        m2 = 0;
      }
    }
    return Math.round(m2);
  }

}
