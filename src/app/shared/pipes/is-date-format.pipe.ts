import { Pipe, PipeTransform } from '@angular/core';
import { DateUtilService } from '../services/date-util.service';

@Pipe({
  name: 'isDateFormat'
})
export class IsDateFormatPipe implements PipeTransform {

  constructor(private dateUtilsService: DateUtilService) {}

  transform(value: any, args?: any): any {
    if(this.dateUtilsService.isDateFormat(value)){
      return this.dateUtilsService.toIsoDate(value);
    }else{
      return value;
    }
  }

}
