import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'riskDays'
})
export class RiskDaysPipe implements PipeTransform {

  transform(days: any, args?: any): any {
    if (days === undefined) {
      return "!";
    }

    if (days == null) {
      return "";
    }

    return days > 0 ? "+" + days : days.toString();
  }

}
