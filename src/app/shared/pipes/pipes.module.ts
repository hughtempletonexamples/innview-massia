import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NumberZeroPipe } from './number-zero.pipe';
import { NumbersFromStringPipe } from './numbers-from-string.pipe';
import { IsDateFormatPipe } from './is-date-format.pipe';
import { UsersForRolesPipe } from './users-for-roles.pipe';
import { RevisionCountPipe } from './revision-count.pipe';
import { DesignIssuedM2Pipe } from './design-issued-m2.pipe';
import { unpublishedRevisionFormatPipe } from './unpublished-revision.pipe';
import { RiskDaysPipe } from './risk-days.pipe'

@NgModule({
  declarations: [NumberZeroPipe,unpublishedRevisionFormatPipe, NumbersFromStringPipe, IsDateFormatPipe, UsersForRolesPipe, RevisionCountPipe, DesignIssuedM2Pipe, RiskDaysPipe],
  imports: [
    CommonModule
  ],
  exports: [
    NumberZeroPipe,
    unpublishedRevisionFormatPipe,
    NumbersFromStringPipe,
    IsDateFormatPipe,
    UsersForRolesPipe,
    RevisionCountPipe,
    DesignIssuedM2Pipe,
    RiskDaysPipe
  ]
})
export class PipesModule { }
