export interface AreaPlan {
    save: boolean;
    areaKey: string;
    startDate: string;
    userChange: boolean;
  }
  
  export interface Blotter {
    planStart: string;
    planDays: number;
    viewStart: string;
    current: number;
    productionLines: object;
    areas: Array<any>
  }
  
  export interface Plan {
    viewStart: string;
    current: number;
    capacity: object;
    dailyM2Totals: object;
    lineM2Totals: object;
    areas: Array<any>
  }

  export interface ExtendedArea {
    _productionLine: string;
    _line: number;
    _projectDetails: object;
    _sortKey: number;
    _stats: object;
    _deliveryDate: string;
    _hasUnpublished: boolean;
    _hasUnpublishedOnly: boolean;
    _id: string;
    _projId:string;
    _dispatchDate: string;
    _riskDate:string;
    _designHandoverDate:string;
    _unpublishedDeliveryDate:string;
    _unpublishedDispatchDate:string;
    _unpublishedRiskDate:string;
    _unpublishedDesignHandoverDate:string;
  }
  
  export interface ProductionPlan {
    buildDays : object;
    plannedFinish : string;
    plannedStart : string;
    project : string;
    riskDate : string;
    userChange: boolean;
    timestamps : innviewTimestamps;
  }
  
  export interface innviewTimestamps {
    created: object;
    modified: object;
  }

  export interface panel {
    area: string;
    id: string;
    project: string;
    type: string;
    dimensions: dimensions;
    timestamps: timestamps;
  }
  export interface dimensions {
    area: number;
    width: number;
    height: number;
    length: number;
    weight: number;
  }
  export interface timestamps {
    created:number;
    modified:number;
  }
  export interface revisions {
    
  }
  export interface area {
      $id:string;
      floor: string;
      phase: string;
      type: string;
      project:string;
      estimatedArea: number;
      estimatedPanels: number;
      actualArea: number;
      actualPanels: number;
      completedArea: number;
      completedPanels: number;
      supplier: string;
      revisions: revisions;
      timestamps:timestamps;
  }

  export interface deliverySchedule{
    published:string;
    revisions:revisions;
    unpublished:string;
  }

  export interface SrdComponent {
    name: string,
    productTypes: Set<string>,
    seq: number
  }

  export interface Components {
    project: string,
    areas: Set<string>,
    type: string,
    comments: string
  }

  export interface Location {
    lat: number; 
    lng: number;
  }

  export interface ComponentsHolder {
    project: Project
    componentWeek: string;
    componentName: string;
    componentId: string;
    comments: string;
    phase: string;
    floor: string;
    supplier: string;
    offload: string;
    areas: Array<any>;
    areasDeliveryDate: Array<any>;
    areasLatestRevision: Array<any>;
    areasDelivered: Array<any>;
    delivered: boolean;
    areasActive: Array<any>;
    active: boolean;
    same: boolean;
    isSame: Array<any>;
    deliveryDate: Date;
    latestRevision: Date;
  }
  
  export interface Project {
    $key: string;
    id: string;
    name:string;
    client: string;
    siteAccess: string;
    datumType: string;
    chainOfCustody: string;
    specialRequirements: string;
    siteAddress: string;
    postCode: string;
    deliverySchedule: {
      published: string;
      revisions: Object;
      unpublished: string;
    };
    siteStart: string;
    nextDeliveryDate: string;
    includeSaturday: boolean;
    deliveryManager: ContactDetails;
    estimatedAreas: EstimatedAreas;
    notifyUsers: any;
    gateway: string;
    weekDuration: number;
    active: boolean;
  }

  export interface EstimatedAreas {
    Ext: number;
    ExtH: number;
    ExtF: number;
    Floor: number;
    Int: number;
    Roof: number;
  }

  export interface ContactDetails {
    id: string;
    name: string;
    mobile: string;
    email: string;
}
  
  export interface DeliverySchedule {
    published: String;
    revisions: Object;
    unpublished: String;
  }
  
  export interface AreasGrouped {
    percentage: number;
    groups: Array<any>;
  }

  export interface AreaExt {
    $key: string
    id: string;
    _stats: _stats;
    _productionLine: string;
    _line: string;
    line: number;
    active: boolean;
    comments: string;
    description: string;
    floor: string;
    offloadMethod: string;
    phase: string;
    project: string;
    revisions: object;
    supplier: string;
    timestamps: object;
    type: string;
  }

  export interface area {
    $id:string;
    floor: string;
    phase: string;
    type: string;
    project:string;
    estimatedArea: number;
    estimatedPanels: number;
    actualArea: number;
    actualPanels: number;
    completedArea: number;
    completedPanels: number;
    supplier: string;
    revisions: revisions;
    timestamps:timestamps;
}
  
  export interface _stats {
    type: string;
    count: number;
    display: string;
    percent: number;
    oneTrue: string;
    completed: number;
  }
  
  export interface AreasMap {
    [key: string]: AreaExt;
  }

  export interface Users {
    "email": string;
    "name": string;
    "phone": string;
    "roles" : {
      "delivery-manager" : boolean,
      "designer" : boolean,
      "operations" : boolean,
      "performance" : boolean,
      "production" : boolean
    };
  }

  export interface ProjectsAdditional {
    "siteOps": ContactDetails,
    "designer": ContactDetails,
    "qs": ContactDetails,
    "engineer": ContactDetails,
    "team": ContactDetails,
    "installer": ContactDetails,
  }

  export interface designIssued {
    "type": object,
    "totalCompleteCount": number,
    "totalDesignedCount": number,
    "totalCompleteM2": number,
    "totalDesignedM2": number,
    "totalDesignIssuedCount": number,
    "totalDesignIssuedM2": number
  }

  export interface AreasAdditional {
    bookings: object;
    matcheck: AdditionalDataTypes;
    checkedDFM: AdditionalDataTypes;
    handoverConfirmed: AdditionalDataTypes;
    issued: AdditionalDataTypes;
    collection: AdditionalDataTypes;
    panelsUploaded: AdditionalDataTypes;
    buildPlannedDays: object;
    buildPlannedDays2: object;
  }
  
  export interface AdditionalDataTypes {
    timeDate: Number;
    status: Boolean;
    user: String;
  }

  export interface MaterialRequest {
    $key: string;
    email: string;
    person: string;
    projectId: string;
    projectName: string;
    materialRequired: string;
    dateRequired: string;
    mainContractor: string;
    siteContact: string;
    siteContactNo: number;
    reason: string;
    qsName: string;
    qsAuth: boolean;
    action: boolean;
    deliveryDate: string;
    note: string;
    timestamps:timestamps;
  }

  export interface Reason {
    name: string;
    abbreviation: string;
  }

  export interface ProjectStats {
    stats: object;
    project: object;
  }

  export interface AreaStats {
    id: string;
    name: string;
    revision: number;
    areas: area[];
  }

  export interface PanelStats {
    stats: PanelStat[],
    actualPanels: number,
    actualArea: number,
    started: number,
    completed: number,
    percentage: number
  }

  export interface ProjectStat {
    projectId: string;
    project: Project;
    label: string;
    delivery: string;
    revision: number;
    estimated: object;
    actual: object;
    completed: object;
    risk: object;
    allProjectRisks : [];
  }

  export interface PanelStat {
    position: number;
    id: string;
    uploaded: string;
    updated: string;
    status: string;
    complete: boolean;
    panel: panel;
  }
