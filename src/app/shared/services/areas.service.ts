import {map, switchMap, flatMap, first, delay} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import { Observable, Subject, combineLatest, timer } from 'rxjs';
import { ProjectsService} from './projects.service';
import { DateUtilService } from './date-util.service';
import { AreasMap, AreaExt, ComponentsHolder } from '../models/models';
import { AuthService } from './auth.service';
import { AsyncValidatorFn, AbstractControl } from '@angular/forms';
import { ReferenceDataService } from './reference-data.service';
import Srd  from '../../../assets/srd/srd.json';
import { ComponentsService } from './components.service';

@Injectable({
  providedIn: 'root'
})
export class AreasService {

  private basePath = '/areas';
  areasRef: AngularFireList<any>;
  areaRef: AngularFireObject<any>;

  areas: Observable<any>;
  area: Observable<any>;
  activeAreas: Object;

  diffDays: number;

  areasMap: AreasMap[];
  areaDetails: AreaExt[];
  dataHolder : Observable<AreasMap>;
  productTypeProductionLine: object;
  productTypes: object;
  productionLines: object;

  accumulatedProjectAreas$: Observable<any>;
  accumulatedAreas$: Observable<any>;
  projectRef$: Subject<string|null>;
  areaRef$: Subject<string|null>;

  takenIds = ['222', '111', '333'];

  private bulkMoveToggleSource = new Subject<boolean>();
  bulkMoveToggle$ = this.bulkMoveToggleSource.asObservable();
  
  private incSatSource = new Subject<boolean>();
  incSat$ = this.incSatSource.asObservable();

  constructor(private componentsService: ComponentsService, private referenceDataService: ReferenceDataService, private authService: AuthService, private db: AngularFireDatabase, private projectsService: ProjectsService, private dateUtilService: DateUtilService) {
    
    this.areasRef = this.db.list(this.basePath);

    this.productTypes = Srd.productTypes;
    this.productionLines = Srd.productionLines;
    this.productTypeProductionLine = this.dateUtilService.buildMap(this.productTypes, "productionLine");

    this.areasMap = [];
    this.areaDetails = [];

    this.projectRef$ = new Subject<string>();
    this.accumulatedProjectAreas$ = this.projectRef$.pipe(
      switchMap(projectRef => 
        db.list(this.basePath, ref =>
          projectRef ? ref.orderByChild('project').equalTo(projectRef) : ref
        ).snapshotChanges()
      )
    );

    this.areaRef$ = new Subject<string>();
    this.accumulatedAreas$ = this.areaRef$.pipe(
      flatMap(areaRef => 
        db.object(`${this.basePath}/${areaRef}`).snapshotChanges()
      )
    );

  }

  // Service message commands
  bulkMoveToggle(toggle: boolean) {
    this.bulkMoveToggleSource.next(toggle);
  }

  // Service message commands
  incSat(toggle: boolean) {
    this.incSatSource.next(toggle);
  }


  addRevisionToAreas(areas, revision) {
    const promises = [];
    areas.forEach((area) => {
      var revisions = Object.keys(area.revisions);
      var lastRevisionDate = area.revisions[revisions[revisions.length -1]];
      area.revisions[revision] =  lastRevisionDate;
      promises.push(this.updateArea(area.$key, area));
    });
    return Promise.all(promises);
  }

  setAreas(areaRef){
    return this.areaRef$.next(areaRef);
  }

  isAreasComplete(){
    return this.areaRef$.complete();
  }

  getAreas(){
    return this.accumulatedAreas$;
  }

  setProjectAreas(projectRef){
    return this.projectRef$.next(projectRef);
  }

  isProjectAreasComplete(){
    return this.projectRef$.complete();
  }

  getProjectAreas(){
    return this.accumulatedProjectAreas$;
  }

  getAdditionalAreaData(areaRef){
    return this.db.object(`${"additionalData/areas"}/${areaRef}`).snapshotChanges()
  }

  onlyStatusTrue(additionalArea){
    let returnVal = false;
    if (additionalArea !== null && (additionalArea.checkedDFM !== undefined && additionalArea.checkedDFM.status !== undefined && additionalArea.checkedDFM.status === true)) {
      if (additionalArea.handoverConfirmed !== undefined && additionalArea.handoverConfirmed.status !== undefined && additionalArea.handoverConfirmed.status === true) {
        returnVal = true;
      }
    }
    return returnVal;
  }

  checkedCount(){
    return this.additionalAreaCheckedDFM().pipe(
      switchMap((additionalAreas) => {
        let arrToCombine = [];
        additionalAreas.map(additionalArea => {
          if(this.onlyStatusTrue(additionalArea)){
            arrToCombine.push(this.getArea(additionalArea.$key));
          }
        });
        return combineLatest(arrToCombine, (...arrays) => arrays.reduce((acc, array) => [...acc, ...array], [])).pipe(map(arr => {
          let productionLineCount = {};

          if(productionLineCount["TOTAL"] === undefined){
            productionLineCount["TOTAL"] = {
              "count": 0,
              "m2": 0
            };
          }

          Object.keys(this.productionLines).forEach((productionLine) => {
            if(productionLineCount[productionLine] === undefined){
              productionLineCount[productionLine] = {
                "count": 0,
                "m2": 0
              };
            }
          });

          arr.forEach(snap => {
            let area = snap.payload.val();
            let m2 = 0;
            let count = 0;
            if(area !== null){
              if(area.actualPanels === undefined || area.actualPanels === 0){
                if(area.estimatedPanels !== undefined){
                  count = area.estimatedPanels;
                  m2 = area.estimatedArea;
                }
                if(area.areaOfPanels !== undefined){
                  count = area.numberOfPanels;
                  m2 = area.areaOfPanels;
                }
                productionLineCount["TOTAL"].count += count;
                productionLineCount["TOTAL"].m2 += m2;
                productionLineCount[this.productTypeProductionLine[area.type]].count += count;
                productionLineCount[this.productTypeProductionLine[area.type]].m2 += m2;
              }
            }
          });

          return productionLineCount;
        }));
      }));
  }

  additionalAreaCheckedDFM(){
    return this.db.list("additionalData/areas", ref => ref.orderByChild("checkedDFM/timeDate")).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
  }

  getProjectsAreasAdditional(id: string){
    return combineLatest([this.projectsService.getprojectWithId(id),this.projectsService.getProjectAdditional(id), this.getAreasForProject(id)]);
  }
  
  getActiveProjectsList() {
    return this.db.list("/projects", ref => ref.orderByChild('active').equalTo(true)).snapshotChanges();
  }

  getCapacities() {
    return this.db.list("productionSubCapacities").snapshotChanges().pipe(first()).toPromise();
  }

  checkProjectExists(id: string) {
    // simulate http.get()
    return this.db.list("/projects", ref => ref.orderByChild('id').equalTo(id)).valueChanges()
      .pipe(delay(400));
  }

  searchProjId(id) {
    return timer(1000)
      .pipe(
        switchMap(() => {
          return this.db.list("/projects", ref => ref.orderByChild('id').equalTo(id)).valueChanges().pipe(first());
        })
      );
  }

  projIdValidator(): AsyncValidatorFn {
    return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
      return this.searchProjId(control.value.toString())
        .pipe(
          map(res => {
            if (res.length) {
              return { 'projIdExists': true};
            }
          })
        );
    };
  }

  searchProjName(name) {
    return timer(1000)
      .pipe(
        switchMap(() => {
          return this.db.list("/projects", ref => ref.orderByChild('name').equalTo(name)).valueChanges().pipe(first());
        })
      );
  }

  projNameValidator(): AsyncValidatorFn {
    return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
      return this.searchProjName(control.value.toString())
        .pipe(
          map(res => {
            if (res.length) {
              return { 'projNameExists': true};
            }
          })
        );
    };
  }

  getareasList() {
    return this.areasRef.snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
  }

  getAreasForProjectAndComponents(projectKey){
    return combineLatest([this.getAreasForProject(projectKey),this.componentsService.getcomponentsforproject(projectKey)]);
  }

  getAreasForProject(key: string): Observable<any> {
    const path = this.basePath;
    this.areas = this.db.list(path, areasRef => areasRef.orderByChild('project').equalTo(key)).snapshotChanges();
    return this.areas;
  }

  getAreasForProjectPromise(key): Promise <any> {
    const path = this.basePath;
    return this.db.list(path, areasRef => areasRef.orderByChild('project').equalTo(key)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    })).pipe(first()).toPromise();
  }

  getArea(key: string): Observable<any> {
    const path = `${this.basePath}/${key}`;
    this.area = this.db.object(path).snapshotChanges();
    return this.area;
  }

  updateAreas(areasArray){
    let promises = [];
    areasArray.map((area)=>{
      promises.push(this.updateArea(area.$key, area));
    });
    return Promise.all(promises);
  }

  updateAreasSpanOut(areasArray){
    let updates = {};
    areasArray.map((area)=>{
      let areaKey = area.$key;
      if(area.$key !== undefined){
        delete area.$key;
      }
      if (!area.timestamps) {
        area.timestamps = {
          created: this.authService.serverTime()
        };
      }
      area.timestamps.modified = this.authService.serverTime();
      updates['/areas/' + areaKey] = area;
    });
    if(Object.keys(updates).length > 0){
      return this.db.database.ref().update(updates);
    }else{
      return Promise.resolve();
    }  
  }

  updateArea(key, updateObj){
    if(updateObj.$key !== undefined){
      delete updateObj.$key;
    }
    if (!updateObj.timestamps) {
      updateObj.timestamps = {
        created: this.authService.serverTime()
      };
    }
    updateObj.timestamps.modified = this.authService.serverTime();
    const path = `${this.basePath}/${key}`;
    const itemRef = this.db.object(path);
    return itemRef.update(updateObj);
  }

  createAreaData(project, area, deliveryDate){

    //Add project's unpublished revision to area revisions
    deliveryDate = this.dateUtilService.toIsoDate(deliveryDate)
    var revisions = {};
    revisions[project.deliverySchedule.unpublished] = deliveryDate;

    var newArea = Object.assign(
      area,
      {
        active: true,
        delivered: false,
        revisions: revisions,
        timestamps: {
          created: this.authService.serverTime(),
          modified: this.authService.serverTime()
        }
      }
    );

    console.log(JSON.stringify(newArea));

    return newArea;
    
  }

  createArea(newArea) {
    return this.areasRef.push(newArea)
    .catch(function (error) {
      console.log("Unable to save area: " + error);
    });
  }

  createAreaComponentFraming(area, project, deliveryDate, framingData, componentType){
    // Get a key for a new Post.
    const newAreatKey = this.db.database.ref().child('areas').push().key;
    const additionalDataArea = {"framing": framingData};
    const newArea = this.createAreaData(project, area, deliveryDate);
    const componentData = this.componentsService.createComponentData(project.$key, [{key: newAreatKey}], componentType);
      
    const newComponentstKey = this.db.database.ref().child('components').push().key;
    // Write the new post's data simultaneously in the posts list and the user's post list.
    let updates = {};
    updates['/areas/' + newAreatKey] = newArea;
    updates['/components/' + newComponentstKey] = componentData;
    updates['/additionalData/areas/' + newAreatKey] = additionalDataArea;

    return this.db.database.ref().update(updates);

  }

  updateareaMessage(key: string, value: any) {
    console.log('key',key,'value',value);
    this.areasRef.update(key, {messages: value.messages});
  }

  deletearea(key: string) {
    return this.areasRef.remove(key);
  }


  resetDeliveryChanges(project){

    return this.getAreasForProjectPromise(project.$key)
      .then((areasHolder: AreaExt[])=>{
        let areas = areasHolder;
        let areasToUpdate = [];
        areas.map((area) => {
          if (area.revisions[project.deliverySchedule.unpublished] !== area.revisions[project.deliverySchedule.published]){
            area.revisions[project.deliverySchedule.unpublished] = area.revisions[project.deliverySchedule.published];
            areasToUpdate.push(area);
          }
        });
        return this.updateAreasSpanOut(areasToUpdate);
      });
  }

  getdaysDiff(updateObj, originalSiteStart, nextDeliveryDate, siteStarted){

    let dateData = {
      "diffDays": 0,
      "currentDate": "",
      "changedDate": "",
      "includeSaturday": false,
      "siteStarted": siteStarted
    };

    let diffDays = 0;
  
    if (!siteStarted){
      if (originalSiteStart !== undefined) {
        var originalProjectDate = this.dateUtilService.toIsoDate(originalSiteStart);
        var updatedProjectDate = this.dateUtilService.toIsoDate(updateObj.siteStart);
      }
    }else{
      if (nextDeliveryDate !== undefined) {
        var originalProjectDate = this.dateUtilService.toIsoDate(nextDeliveryDate);
        var updatedProjectDate = this.dateUtilService.toIsoDate(updateObj.nextDeliveryDate);
      }
    }
  
    if (this.dateUtilService.validDate(originalProjectDate) && this.dateUtilService.validDate(updatedProjectDate)){    
      if (updateObj.includeSaturday) {
        diffDays = this.dateUtilService.daysBetweenWeekDaysAndSat(originalProjectDate, updatedProjectDate);
      }else{
        diffDays = this.dateUtilService.daysBetweenWeekDaysOnly(originalProjectDate, updatedProjectDate);
      }
    }

    dateData.diffDays = diffDays;
    dateData.currentDate = originalProjectDate;
    dateData.changedDate = updatedProjectDate;
    dateData.includeSaturday = updateObj.includeSaturday;

    return dateData;
  }

  // checkDateDiffWithDeliveryDateUpdate(project, updateObj, diffDays): Promise<any>{

  //   this.diffDays = diffDays;

  //   if (this.diffDays !== 0 && (this.diffDays > 0 || this.diffDays < 0)) {
  //     return this.getAreasForProjectPromise(project.$key).then((areas: AreaExt[])=>{
  
  //       const promises = [];
  
  //       areas.map((area)=>{
  //         var plusDate;
  //         if (this.diffDays > 0) {
  //           if (updateObj.includeSaturday) {
  //             plusDate = this.dateUtilService.addWorkingDaysAndSat(area.revisions[project.deliverySchedule.unpublished], this.diffDays);
  //           } else {
  //             plusDate = this.dateUtilService.addWorkingDays(area.revisions[project.deliverySchedule.unpublished], this.diffDays);
  //           }
  //         } else {
  //           this.diffDays = Math.abs(this.diffDays);
  //           if (updateObj.includeSaturday) {
  //             plusDate = this.dateUtilService.subtractWorkingDaysAndSat(area.revisions[project.deliverySchedule.unpublished], this.diffDays);
  //           } else {
  //             plusDate = this.dateUtilService.subtractWorkingDays(area.revisions[project.deliverySchedule.unpublished], this.diffDays);
  //           }
  //         }
  //         area.revisions[project.deliverySchedule.unpublished] = plusDate;
  //         console.log(area);
  //         promises.push(this.updateArea(area.$key, area));
  //       });
  
  //       return Promise.all(promises);
  
  //     });
  //   }else{
  //     return Promise.resolve();
  //   }
  
  // }

  createAreasMap( components ) {
    var areasMap = {};
    var areasMapCounter = {};

    components.forEach(function ( component ) {
      if (areasMapCounter[component.type] === undefined) {
        areasMapCounter[component.type] = 0;
      }
      areasMapCounter[component.type]++;
      let areaKeys = Object.keys(component.areas);
      areaKeys.forEach(function ( areaKey ) { 
        var componentName = component.type + areasMapCounter[component.type];
        areasMap[areaKey] = {
          id: component.$key,
          name: componentName,
          comments: component.comments
        };
      });
    });

    return areasMap;
  }

  calculateEstimatedAreasAndPanels(project) {
      
    var productTypes;
    var datumData;

    return this.referenceDataService.getDatumsPromise(project.datumType)
      .then((datum) => {
        datumData = datum;
        return this.referenceDataService.getReferenceDataPromise("productTypes");
      })
      .then((pT) => {
        productTypes = pT;
        return this.getAreasForProjectPromise(project.$key);
      })
      .then((areas) => {
        
        var promises = []
        var productTypeProductionLine = this.dateUtilService.buildMapKey(productTypes, "productionLine");
        var estimatesPerGroup = {};
        var areaCount = {};

        Object.keys(project.estimatedAreas).forEach((group) => {
          let totalArea = project.estimatedAreas[group];
          estimatesPerGroup[group] = totalArea;
        });

        areas.forEach((area) => {
          if (areaCount[area.type] === undefined) {
            areaCount[area.type] = 0;
          }
          areaCount[area.type]++;
        });
        
        areas.forEach((area) => {
          var productionLineKey = productTypeProductionLine[area.type];
          if (estimatesPerGroup[area.type] !== undefined) {
            area.estimatedArea = Math.round(estimatesPerGroup[area.type] / areaCount[area.type]);
            if (productionLineKey !== undefined && datumData[0] !== undefined && datumData[0].productionLines[productionLineKey] !== undefined) {
              var panelAvgOverride = datumData[0].productionLines[productionLineKey].panelAvgOverride;
              var panelAvg = datumData[0].productionLines[productionLineKey].panelAvg;
              if (panelAvgOverride > 0) {
                area.estimatedPanels = Math.round(area.estimatedArea / panelAvgOverride) | 0;
              } else {
                area.estimatedPanels = Math.round(area.estimatedArea / panelAvg) | 0;
              }
            }
            promises.push(this.updateArea(area.$key, area));
          }
        });
        
        return Promise.all(promises);
        
      });

    
  }

  getProjectAreasGroupedByComponent(id){
    let componentsMap = {};
    let groupedComponents: ComponentsHolder[] = [];
    return this.projectsService.getProjectAndComponents(id).pipe(
      switchMap(([project, components]) => {
        project.$key = id;
        let arrToCombine = [];
        componentsMap = this.createAreasMap( components )
        components.map(component => {
          Object.keys(component.areas).map(areaKey => {
          arrToCombine.push(this.getArea(areaKey))
          });
        });
        return combineLatest(arrToCombine, (...arrays) => arrays.reduce((acc, array) => [...acc, ...array], [])).pipe(
          map(arr => {
            groupedComponents = [];
            arr.map((areaSnapshot) => {
              let area = areaSnapshot.payload.val();
              let areaKey = areaSnapshot.payload.key;

              if(area !== null){

                area.$key = areaKey;

                let componentKey = componentsMap[areaSnapshot.key];
                let noneCount = 0;
          
                if(componentKey === undefined) {
                  noneCount++;
                  componentKey = {};
                  componentKey.name = "NONE" + noneCount;
                  componentKey.id = 0;
                  componentKey.comments = 0;
                }
          
                let component = groupedComponents[componentKey.name];
                if(component === undefined) {
                  component = groupedComponents[componentKey.name] = {
                    project: project,
                    componentName: componentKey.name,
                    componentWeek: "",
                    phase: "",
                    floor: "",
                    supplier: "",
                    offload: "",
                    componentId: componentKey.id,
                    comments: componentKey.comments,
                    areas: [],
                    areasDeliveryDate: [],
                    areasLatestRevision: [],
                    areasDelivered: [],
                    areasActive: [],
                    delivered: true,
                    active: true,
                    same: false,
                    isSame: [],
                    deliveryDate: new Date("2100-12-30"),
                    latestRevision: new Date("2100-12-30")
                  };
                }
          
                let weekStart = this.dateUtilService.weekStart(area.revisions[project.deliverySchedule.published]);
          
                component.phase = area.phase;
                component.supplier = area.supplier;
                component.floor = area.floor;
                component.componentWeek = this.dateUtilService.toIsoDate(weekStart);
                component.offload = area.offloadMethod;
          
                let areaDate = new Date(area.revisions[project.deliverySchedule.unpublished]);
                let revision = new Date(area.revisions[project.deliverySchedule.unpublished]);
                
                if(project.deliverySchedule.published !== undefined){
                  revision = new Date(area.revisions[project.deliverySchedule.published]);
                }
                
                let isSame = component.isSame[areaSnapshot.key];
                let deliveryDate = component.areasDeliveryDate[areaSnapshot.key];
                let latestRevision = component.areasLatestRevision[areaSnapshot.key];
                let areaDelivered = component.areasDelivered[areaSnapshot.key];
                let areaActive = component.areasActive[areaSnapshot.key];
                
                if (deliveryDate === undefined) {
                  deliveryDate = component.areasDeliveryDate[areaSnapshot.key] = areaDate;
                }
                
                if (latestRevision === undefined) {
                  latestRevision = component.areasLatestRevision[areaSnapshot.key] = revision;
                }
                
                if (isSame === undefined) {
                  if(this.dateUtilService.toIsoDate(deliveryDate) === this.dateUtilService.toIsoDate(latestRevision)){
                    isSame = component.isSame[areaSnapshot.key] = true;
                  }else{
                    isSame = component.isSame[areaSnapshot.key] = false;
                  }
                }
                
                if (areaDelivered === undefined) {
                  if (area.delivered !== undefined && area.delivered) {
                    areaDelivered = component.areasDelivered[areaSnapshot.key] = true;
                  } else {
                    areaDelivered = component.areasDelivered[areaSnapshot.key] = false;
                  }
                }
                
                if (areaActive === undefined) {
                  if (area.active) {
                    areaActive = component.areasActive[areaSnapshot.key] = true;
                  } else {
                    areaActive = component.areasActive[areaSnapshot.key] = false;
                  }
                }

                if (!areaDelivered){
                  component.delivered = false;
                }
                
                if (!areaActive){
                  component.active = false;
                }
                
                component.areas.push(area);
          
                if (areaDate < component.deliveryDate) {
                  component.deliveryDate = areaDate;
                }
                if (latestRevision < component.latestRevision) {
                  component.latestRevision = latestRevision;
                }
                if(this.dateUtilService.toIsoDate(component.deliveryDate) === this.dateUtilService.toIsoDate(component.latestRevision)){
                  component.same = true;
                }
          
              }
      
            });
            
            return groupedComponents;

          }));
      }));
      
  }


}

