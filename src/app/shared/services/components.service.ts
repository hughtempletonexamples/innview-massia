import {map, switchMap, flatMap, first} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ComponentsService {

  private basePath = '/components';
  componentsRef: AngularFireList<any>;
  activecomponentsRef: AngularFireList<any>;
  componentRef: AngularFireObject<any>;

  components: Observable<any>;
  component: Observable<any>;

  items$: Observable<any>;
  projectRef$: Subject<string|null>;

  constructor(private db: AngularFireDatabase) {
    this.componentsRef = this.db.list(this.basePath);

    this.projectRef$ = new Subject<string>();
    this.items$ = this.projectRef$.pipe(
      flatMap(projectRef => 
        db.list(this.basePath, ref =>
          projectRef ? ref.orderByChild('project').equalTo(projectRef) : ref
        ).snapshotChanges()
      )
    );
  }

  createComponentData(projectKey, areas, componentType) {
      
    var newComponent = {
      project: projectKey,
      areas: createSet(areas),
      type: componentType
    };

    function createSet(arrayOfItems) {
      var set = {};
      arrayOfItems.map((item)=>{
        set[item.key] = true;
      });
      return set;
    }

    return newComponent;

  }

  create(newComponent) {
    return this.componentsRef.push(newComponent); 
  }

  setAreas(projectRef){
    return this.projectRef$.next(projectRef);
  }

  getAreas(){
    return this.items$;
  }

  updateComponent(key, updateObj){
    const path = `${'components'}/${key}`;
    const itemRef = this.db.object(path);
    return itemRef.update(updateObj);
  }

  getcomponentsList() {
    return this.componentsRef.snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
  }

  getcomponentsforprojectPromise(key: string): Promise<any>{
    const path = this.basePath;
    return this.db.list(path, areasRef => areasRef.orderByChild('project').equalTo(key)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    })).pipe(first()).toPromise();
  }

  getcomponentsforproject(key: string): Observable<any> {
    const path = this.basePath;
    return this.db.list(path, areasRef => areasRef.orderByChild('project').equalTo(key)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
  }

  getcomponent(key: string): Observable<any> {
    const path = `${this.basePath}/${key}`;
    this.component = this.db.object(path).valueChanges();
    return this.component;
  }

  createcomponent(component) {
    this.componentsRef.push(component);
  }

  updatecomponentMessage(key: string, value: any) {
    console.log('key',key,'value',value);
    this.componentsRef.update(key, {messages: value.messages});
  }

  deleteComponentArea(componentKey: string, areaKey: string) {
    const path = `${this.basePath}/${componentKey}/areas`;
    let component = this.db.object(path);
    let area = {};
    if(area[areaKey] !== undefined){
      area[areaKey] = null;
    }
    return component.set(area);
  }

  deletecomponent(key: string) {
    this.componentsRef.remove(key);
  }

  deleteAll() {
    this.componentsRef.remove();
  }
}
