import { Injectable } from '@angular/core';
import { AngularFireDatabase, snapshotChanges, AngularFireList } from '@angular/fire/database';
import { map, first } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ReferenceDataService {

  private basePath = '/srd';

  usersRef: AngularFireList<any>;

  constructor(private db: AngularFireDatabase) {
    this.usersRef = this.db.list('users');
  }

  getChainOfCustodyTypes() {
    return this.getReferenceData("chainOfCustodyTypes");
  }
  
  getComponents() {
    return this.getReferenceData("components");
  }

  getDatumTypes() {
    return this.getReferenceData("datumTypes");
  }

  getVehicleTypes() {
    return this.getReferenceData("vehicleTypes");
  }
  
  getHaulierTypes() {
    return this.getReferenceData("haulierTypes");
  }

  getPhases() {
    return this.getReferenceData("phases");
  }

  getFloors() {
    return this.getReferenceData("floors");
  }

  getProductionLines() {
    return this.getReferenceData("productionLines");
  }
  
  getProductionSubLines() {
    return this.getReferenceData("productionSubLines");
  }

  getProductTypes() {
    return this.getReferenceData("productTypes");
  }

  getPanelTypes() {
    return this.getReferenceData("panelTypes");
  }

  getSuppliers() {
    return this.getReferenceData("suppliers");
  }

  getOffloadMethods() {
    return this.getReferenceData("offloadMethods");
  }
  
  getCuttingTypes(){
    return this.getReferenceData("cuttingTypes");
  }
  
  getStockCount(){
    return this.getReferenceData("stockCount");
  }
  
  getGateways(){
    return this.getReferenceData("gateways");
  }

  getTeams(){
    return this.getReferenceData("teams");
  }

  getInstallers(){
    return this.getReferenceData("installers");
  }

  getItem(variant, id) {
    const path = `${this.basePath}/${variant}/${id}`;
    return this.db.object(path).valueChanges().pipe(first()).toPromise();
  }

  getReferenceData(key) {
    const path = `${this.basePath}/${key}`;
    return this.db.list(path).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
  }

  getReferenceDataPromise(key) {
    const path = `${this.basePath}/${key}`;
    return this.db.list(path).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    })).pipe(first()).toPromise();
  }

  getUsers() {
    return this.usersRef.snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
  }

  getUser(key: string): Observable<any> {
    const path = `${'users'}/${key}`;
    return this.db.object(path).valueChanges();
  }

  getUserPromise(key: string): Promise<any> {
    const path = `${'users'}/${key}`;
    return this.db.object(path).valueChanges().pipe(first()).toPromise();
  }

  getDatumsPromise(key: string) {
    const path = 'datums';
    return this.db.list(path, datumRef => datumRef.orderByChild('name').equalTo(key)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    })).pipe(first()).toPromise();
  }



}
