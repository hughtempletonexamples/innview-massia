
import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import { AngularFireAuth} from '@angular/fire/auth';
import { Observable } from 'rxjs';
import * as firebaseApp from 'firebase/app';

@Injectable({
  providedIn: 'root',
 })
export class AuthService {

  private basePath = 'users';
  chatsRef: AngularFireList<any>;
  chatRef: AngularFireObject<any>;

  chats: Observable<any>;
  chat: Observable<any>;

  authState: any = null;

  constructor(private afAuth: AngularFireAuth, private db: AngularFireDatabase) {
    this.chatsRef = this.db.list(this.basePath);
    this.afAuth.authState.subscribe((auth) => {
      this.authState = auth;
    });
    
  }

  serverTime() {
    return firebaseApp.database.ServerValue.TIMESTAMP;
  }

  // Returns true if user is logged in
  public authenticated(): boolean {
    return this.authState !== null;
  }

  // Returns current user data
  public currentUser(): any {
    return this.authenticated ? this.authState : null;
  }

  // Returns
  get currentUserObservable(): any {
    return this.afAuth.authState
  }

  // Returns current user UID
  get currentUserId(): string {
    return this.authenticated() ? this.authState.uid : '';
  }

  // Anonymous User
  get currentUserAnonymous(): boolean {
    return this.authenticated() ? this.authState.isAnonymous : false
  }

  // Returns current user display name or Guest
  get currentUserDisplayName(): string {
    if (!this.authState) { return 'Guest' }
    else if (this.currentUserAnonymous) { return 'Anonymous' }
    else { return this.authState['displayName'] || 'User without a Name' }
  }

  emailLogin(email:string, password:string) {
     return this.afAuth.auth.signInWithEmailAndPassword(email, password)
       .then((user) => {
         this.authState = user
         return user;
       })
       .catch(error => console.log(error));
  }

  //// Sign Out ////

  signOut(): void {
    this.afAuth.auth.signOut();
  }

}
