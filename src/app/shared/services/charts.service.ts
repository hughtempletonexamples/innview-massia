import { Injectable } from '@angular/core';
import { DateUtilService } from './date-util.service';
import { ReferenceDataService } from './reference-data.service';
import { ProjectsService } from './projects.service';
import { combineLatest } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ChartsService {

  today: Date;
  prevMonth: string;

  constructor(private dateUtilService: DateUtilService, private referenceDataService: ReferenceDataService, private projectsService: ProjectsService) {
    this.today = new Date();
    this.today.setDate(1);
    let prevMonth = this.today.setMonth(this.today.getMonth()-1);
    this.prevMonth = this.dateUtilService.weekStart(prevMonth);
  }

  getForecastData(){
    return combineLatest([
      this.referenceDataService.getGateways(),
      this.referenceDataService.getProductTypes(),
      this.referenceDataService.getProductionLines(),
      this.projectsService.getActiveProjectsList()
    ]);
  }

  processForecastData(projects, gateways, productTypes, productionLines, weeksAmount, dayzGone){

      let mainData = {};
      let monthData = {};
      let projectsByGateway = {};
      let monthProjectsByGateway = {};
      let gatewayData = {};
      let productTypeProductionLine = {};
      let monthsAmount = weeksAmount / 4;
      let defaultDuration = 12;
      let durationOfWeeks;
      let duration;
      let returnData = {};

      gateways.sort(this.compareGateways);
      gateways.reverse();
      gatewayData = this.dateUtilService.buildMapKey(gateways, "colour");
      productTypeProductionLine = this.dateUtilService.buildMapKey(productTypes, "productionLine");
      productionLines.forEach((productionLine) => {
        var productionLineKey = productionLine.$key;
        if (mainData[productionLineKey] === undefined) {
          mainData[productionLineKey] = {};
        }
        if (monthData[productionLineKey] === undefined) {
          monthData[productionLineKey] = {};
        }
        if (projectsByGateway[productionLineKey] === undefined) {
          projectsByGateway[productionLineKey] = {};
        }
        if (monthProjectsByGateway[productionLineKey] === undefined) {
          monthProjectsByGateway[productionLineKey] = {};
        }
        gateways.forEach((gateway) => {
          let gatewayKey = gateway.$key;
          if (mainData[productionLineKey][gatewayKey] === undefined) {
            mainData[productionLineKey][gatewayKey] = {};
          }
          if (monthData[productionLineKey][gatewayKey] === undefined) {
            monthData[productionLineKey][gatewayKey] = {};
          }
          if (projectsByGateway[productionLineKey][gatewayKey] === undefined) {
            projectsByGateway[productionLineKey][gatewayKey] = {};
          }
          if (monthProjectsByGateway[productionLineKey][gatewayKey] === undefined) {
            monthProjectsByGateway[productionLineKey][gatewayKey] = {};
          }
          var weeks = this.getWeeks(weeksAmount, dayzGone);
          weeks.forEach((week) => {
            let siteStartRaw = new Date(week);
            let siteStart = this.dateUtilService.weekStart(siteStartRaw);
            if (projectsByGateway[productionLineKey][gatewayKey][siteStart] === undefined) {
              projectsByGateway[productionLineKey][gatewayKey][siteStart] = [];
            }
            if (mainData[productionLineKey][gatewayKey][siteStart] === undefined) {
              mainData[productionLineKey][gatewayKey][siteStart] = 0;
            }
          });
          var months = this.buildMonths(monthsAmount);
          months.forEach((month) => {
            let siteStartRaw = new Date(month);
            let siteStart = this.dateUtilService.dateMonth(siteStartRaw);
            if (monthProjectsByGateway[productionLineKey][gatewayKey][siteStart] === undefined) {
              monthProjectsByGateway[productionLineKey][gatewayKey][siteStart] = [];
            }
            if (monthData[productionLineKey][gatewayKey][siteStart] === undefined) {
              monthData[productionLineKey][gatewayKey][siteStart] = 0;
            }
          });
        });
      });

      projects.forEach((project) => {
        if (project.siteStart !== undefined) {
          let siteStart = new Date(project.siteStart);
          let week = this.dateUtilService.weekStart(siteStart);
          if (project.weekDuration !== undefined){
            durationOfWeeks = this.buildDuration(week, project.weekDuration);
            duration = project.weekDuration;
          }else{
            durationOfWeeks = this.buildDuration(week, defaultDuration);
            duration = defaultDuration;
          }
          for (let i = 0; i<= durationOfWeeks.length; i++){
            let weekBetween = durationOfWeeks[i];
            let weekDate = new Date(weekBetween);
            let month = this.dateUtilService.dateMonth(weekDate);
            Object.keys(project.estimatedAreas).forEach((type) => {
              let meterSquared = project.estimatedAreas[type];
              let productionLine = productTypeProductionLine[type];
              if (project.gateway !== undefined) {
                if (projectsByGateway[productionLine][project.gateway][weekBetween] !== undefined) {
                  if(projectsByGateway[productionLine][project.gateway][weekBetween].indexOf(project.id) === -1){
                    projectsByGateway[productionLine][project.gateway][weekBetween].push(project.id);
                  }
                }
                if (mainData[productionLine][project.gateway][weekBetween] !== undefined) {
                  mainData[productionLine][project.gateway][weekBetween] += meterSquared / duration;
                }
                if (monthProjectsByGateway[productionLine][project.gateway][month] !== undefined) {
                  if(monthProjectsByGateway[productionLine][project.gateway][month].indexOf(project.id) === -1){
                    monthProjectsByGateway[productionLine][project.gateway][month].push(project.id);
                  }
                }
                if (monthData[productionLine][project.gateway][month] !== undefined) {
                  monthData[productionLine][project.gateway][month] += meterSquared / duration;
                }
              }
            });
          } 
        }
      });

      returnData["gatewayData"] = gatewayData;
      returnData["weekData"] = mainData;
      returnData["monthData"] = monthData;
      returnData["projectsByGateway"] = projectsByGateway;
      returnData["monthProjectsByGateway"] = monthProjectsByGateway;

      return returnData;
    }

    compareChartGateways(a, b) {
      a = a.key.replace("G","");
      b = b.key.replace("G","");
      return a - b;
    }
    
    compareGateways(a, b) {
      a = a.$key.replace("G","");
      b = b.$key.replace("G","");
      return a - b;
    }
    
    buildDuration(siteStart, duration){
      var buildWeeks = [];
      buildWeeks.push(siteStart);
      for (var j = 1; j < duration; j++) {
        siteStart = this.dateUtilService.plusDays(siteStart, 7);
        buildWeeks.push(siteStart);
      }
      return buildWeeks;
    }
    
    getWeeks(weeksAmount, dayzGone) {
      var weekHolder = [];
      var monday = this.dateUtilService.weekStart(this.prevMonth);
      var firstMonday = this.dateUtilService.minusDays(monday, dayzGone);
      weekHolder.push(firstMonday);
      for (var j = 1; j <= weeksAmount; j++) {
        firstMonday = this.dateUtilService.plusDays(firstMonday, 7);
        weekHolder.push(firstMonday);
      }
      return weekHolder;
    }
    
    buildMonths(monthsAmount) {
      var months = [];
      var tmpDate;
  
      for (var i = 0; i < monthsAmount; i++) {
        tmpDate = new Date(this.prevMonth);
        tmpDate.setDate(1);
        tmpDate.setMonth(tmpDate.getMonth() + i);
        tmpDate = this.dateUtilService.dateMonth(tmpDate);
        months.push(tmpDate);
      }
      return months;
    }
}
