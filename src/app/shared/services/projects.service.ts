import {map, first} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import { Observable, combineLatest } from 'rxjs';
import { ComponentsService } from './components.service';
import { AuthService } from './auth.service';
import { DateUtilService } from './date-util.service';
import { ProjectsAdditional } from '../models/models';

@Injectable({
  providedIn: 'root'
})

export class ProjectsService {

  private basePath = '/projects';
  private basePathAdditional = '/additionalData/projects';
  projectsRef: AngularFireList<any>;
  activeProjectsRef: AngularFireList<any>;
  projectRef: AngularFireObject<any>;

  projects: Observable<any>;
  project: Observable<any>;

  constructor(private db: AngularFireDatabase, private dateUtilService: DateUtilService, private componentsService: ComponentsService, private authService: AuthService) {
    this.projectsRef = this.db.list(this.basePath);
    this.activeProjectsRef = this.db.list(this.basePath, ref => ref.orderByChild('active').equalTo(true));
  }

  getMaterialRequests(): Observable<any>{
    return this.db.list("materialrequests").snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
  }

  getProjectAndComponents(id){
    return combineLatest([this.getproject(id),this.componentsService.getcomponentsforproject(id)]);
  }

  getActiveProjectsList() {
    return this.activeProjectsRef.snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
  }

  doesExist(project, projects){
    let exists = false;
    projects.array.forEach(proj => {
      if(project.id === proj.id){
        exists = true;
      }
    });
    return exists;
  }

  updateProject(key, updateObj){
    if(updateObj.$key !== undefined){
      delete updateObj.$key;
    }

    const path = `${this.basePath}`;
    const itemRef = this.db.list(path);
    console.log("Key: " + key);
    console.log("Update Data: " + JSON.stringify(updateObj));
    return itemRef.update(key, updateObj);
  }


  updateProjectAdditional(key, updateObj){
    if(updateObj.$key !== undefined){
      delete updateObj.$key;
    }

    const path = `${this.basePathAdditional}`;
    const itemRef = this.db.list(path);
    return itemRef.update(key, updateObj);
  }

  getProjectsList() {
    return this.projectsRef.snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
  }

  getproject(key: string): Observable<any> { 
    const path = `${this.basePath}/${key}`;
    this.project = this.db.object(path).valueChanges();
    return this.project;
  }

  getprojectWithId(key: string): Observable<any> { 
    const path = `${this.basePath}/${key}`;
    this.project = this.db.object(path).snapshotChanges().pipe(map(obj => {
      return Object.assign(obj.payload.val(), {$key: obj.key});
    }));
    return this.project;
  }

  getFramingStyles(){
    const path = "config/framingStyles";
    var framingStyles = this.db.object(path).valueChanges();
    return framingStyles;
  }

  getFramingStylesPromise(){
    const path = "config/framingStyles";
    var framingStyles = this.db.object(path).valueChanges().pipe(first()).toPromise();
    return framingStyles;
  }

  getProjectAdditional(key: string): Observable<any> {
    const path = `${this.basePathAdditional}/${key}`;
    this.project = this.db.object(path).valueChanges();
    return this.project;
  }
  // saveProjectAdditional(data,key){
  //   const path = 'additionalData/projects/'+key+'/framingStyles';
  //   var newData = this.db.object(path).update(data);
  // }

  removeRevisions(projectKey){
    return this.db.list('revisions', panelsRef => panelsRef.orderByChild('project').equalTo(projectKey)).snapshotChanges().pipe(first()).toPromise()
      .then((revisions) => this.db.object('revisions').update(
        revisions.reduce((acc, rev) => { acc[rev.key] = null; return acc; }, {})
      ));
  }

  createProject(project) {
    if (!project.timestamps) {
      project.timestamps = {       
        created: this.authService.serverTime(),
        createdBy: this.authService.currentUserId,
        modified: this.authService.serverTime(),
        modifiedBy: this.authService.currentUserId
      };
    }
    project.active = true;

    console.log(JSON.stringify(project));  
    return this.projectsRef.push(project);
  }

  updateprojectMessage(key: string, value: any) {
    console.log('key',key,'value',value);
    this.projectsRef.update(key, {messages: value.messages});
  }

  deleteproject(key: string) {
    this.projectsRef.remove(key);
  }

  rebuildPeople(updateObj, name){
    var newPeople = {
      id: "",
      name: "",
      mobile: "",
      email: ""
    };
    if(updateObj[name] !== undefined && updateObj[name] !== null){
      newPeople.id = updateObj[name].$key || updateObj[name].id;
      newPeople.name = updateObj[name].name;
      if(updateObj[name].phone !== undefined){
        newPeople.mobile = updateObj[name].phone;
      }
      newPeople.email = updateObj[name].email;
    }else{
      newPeople = null;
    }
    return newPeople;
  }

  convertToAdditional(updateObj){
    let newAdditional: ProjectsAdditional = {
      siteOps: this.rebuildPeople(updateObj, "site-ops"),
      designer: this.rebuildPeople(updateObj, "designer"),
      qs: this.rebuildPeople(updateObj, "quantity-surveyor"),
      engineer: this.rebuildPeople(updateObj, "engineer"),
      installer: updateObj.installer || null,
      team: updateObj.team || null,
    }
    return newAdditional;
  }

  convertFormToDatabaseStruct(updateObj){
    var newProject = Object.assign({}, updateObj);

    if(newProject.id !== undefined){
      newProject.id = String(newProject.id);
    }

    newProject.deliveryManager = this.rebuildPeople(updateObj, "delivery-manager");
    delete newProject["delivery-manager"];

    if(newProject.notifyUsers === null){
      newProject.notifyUsers = {};
    }else{
      newProject.notifyUsers = this.setNotifyUsers(newProject);
    }

    newProject.estimatedAreas.ExtH = Math.floor((10/ 100) * updateObj.estimatedAreas.Ext);
    
    if(updateObj.siteStart !== undefined){
      newProject.siteStart = this.dateUtilService.toIsoDate(updateObj.siteStart);
    }
    if(updateObj.nextDeliveryDate !== undefined){
      newProject.nextDeliveryDate = this.dateUtilService.toIsoDate(updateObj.nextDeliveryDate);
    }
    
    return newProject;
  }

  setNotifyUsers(newProject){
    let notifyUsers = {};
    newProject.notifyUsers.forEach((selectedUser) => {
      if(notifyUsers[selectedUser] === undefined){
        notifyUsers[selectedUser] = true;
      }
    });
    return notifyUsers;
  }

  

}