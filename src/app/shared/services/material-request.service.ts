import { Injectable } from '@angular/core';
import { AngularFireList, AngularFireDatabase } from '@angular/fire/database';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MaterialRequestService {

  private basePath = '/materialrequests';
  materialsRef: AngularFireList<any>;
  materialRequest: Observable<any>;

  constructor(private db: AngularFireDatabase, private authService: AuthService) { 
    this.materialsRef = this.db.list(this.basePath);
  }

  getMaterialRequest(key: string): Observable<any> { 
    const path = `${this.basePath}/${key}`;
    this.materialRequest = this.db.object(path).valueChanges();
    return this.materialRequest;
  }

  createMaterialRequest(materialRequest) {
    if (!materialRequest.timestamps) {
      materialRequest.timestamps = {       
        created: this.authService.serverTime(),
        modified: this.authService.serverTime(),
      };
    }
    console.log(JSON.stringify(materialRequest));  
    return this.materialsRef.push(materialRequest);
  }

  updateMaterialRequest(id, materialRequest) {
    materialRequest.timestamps.modified = this.authService.serverTime(),
    console.log(JSON.stringify(materialRequest));
    return this.materialsRef.update(id, materialRequest);
  }

}
