import { Injectable } from '@angular/core';
import * as XLSX from 'xlsx';
import Srd from '../../../assets/srd/srd.json'
import { DateUtilService } from './date-util.service';
import { ReferenceDataService } from './reference-data.service';
import { ProjectsService } from './projects.service.js';
import { AngularFireDatabase } from '@angular/fire/database';
import { AreasService } from './areas.service.js';
import { Observable, Subject } from 'rxjs';
import { ComponentsService } from './components.service.js';

@Injectable({
  providedIn: 'root'
})
export class LoadPanelsService {

  fileType:string;
  project:any;
  productTypeProductionLine: object;
  productTypeName: object;
  phases: object = Srd.phases;
  floors: object = Srd.floors;
  panelTypes: object = Srd.panelTypes;
  productTypes: object = Srd.productTypes;
  components: object = Srd.components;
  componentsMapped: object;
  datumData: Array<any>;
  this:any;
  subject$: Subject<any>;

  constructor(private componentsService: ComponentsService, private areasService : AreasService, private db: AngularFireDatabase, private projectsService: ProjectsService, private dateUtilService: DateUtilService, private referenceDataService: ReferenceDataService) {
    this.productTypeProductionLine = this.dateUtilService.buildMap(this.productTypes, "productionLine");
    this.productTypeName = this.dateUtilService.buildMapReverse(this.productTypes, "name");
    this.componentsMapped = this.mapComponents(this.components);
    this.subject$ = new Subject();
  }

  updateProgress(obj) { 
    this.subject$.next(obj); 
  }
  
  getProgress() {
    return this.subject$.asObservable();
  }

  addAreas(data){

    let panelsAssociated = [];
    let areasToDelete = [];

    let division = 0;

    let statusData = {
      loading: true,
      status:"Processing....",
      type: "Delete",
      counter: 0
    }

    this.updateProgress(statusData);
  
    data.areasGrouped.forEach(groupedAreas => {
      groupedAreas.areas.forEach(area => {
        if(area.actualPanels === undefined){
          areasToDelete.push(area);
        }else{
          panelsAssociated.push(this.getAreaId(area, data.project));
        }
      });
    });

    division = 100 / (areasToDelete.length + data.result.models.length);

    if(areasToDelete.length){
      statusData.status = "Found " + areasToDelete.length + " areas to with no panels to delete - Deleting....";
      let promises = [];
      areasToDelete.forEach((area)=>{
        promises.push(this.areasService.deletearea(area.$key).then(()=>{
          statusData.counter += Number(division.toFixed(0));
          this.updateProgress(statusData);
        }));
      });
      return Promise.all(promises)
      .then(()=>{
        return createAreas(this);
      })
      .then(()=>{
        statusData.status = "Components sucsessfully added.";
        statusData.counter = 100;
        this.updateProgress(statusData);
      })
      .catch((error) => {
        statusData.status = error + " - Please let IT know!";
        this.updateProgress(statusData);
      });
    }else{
      return createAreas(this)
      .then(()=>{
        statusData.status = "Components sucsessfully added.";
        statusData.counter = 100;
        this.updateProgress(statusData);
      })
      .catch((error) => {
        statusData.status = error + " - Please let IT know!";
        this.updateProgress(statusData);
      });
    }


    function createAreas(that){

      statusData.status = "Creating " + data.result.models.length + " components....";
      statusData.type = "create";

      let promises = [];
      data.result.models.forEach(element => {
        let component = element.component;
        let deliveryDate = element.deliveryDate;
        let framing = element.framing;
        element.description = element.component;
        element.supplier = "Innovaré";
        element.revitUpload = true;
        delete element.component;
        delete element.productionLine;
        delete element.framing;
        delete element.deliveryDate;
        let isAssociated = false;
        panelsAssociated.forEach(areaId => {
          let elementId = that.getAreaId(element, data.project);
          if(elementId === areaId){
            isAssociated = true;
            return;
          }
        });
        if(!isAssociated){
          promises.push(that.areasService.createAreaComponentFraming(element, data.project, deliveryDate, framing, component)
          .then(()=>{
            statusData.counter += Number(division.toFixed(0));
            that.updateProgress(statusData);
          }));
        }else{
          promises.push(Promise.resolve());
        }
      })
      return Promise.all(promises);
    
    }
  }

  getAreaId(area, project) {
    return [project.$key, area.phase, area.floor, area.type].join("-");
  }

  mapComponents(components){
    let singleProductTypes = {};
    Object.keys(components).map((componentKey)=>{
      let component = components[componentKey];
      let productTypes = Object.keys(component.productTypes);
      if(productTypes.length === 1){
        let productType = productTypes[0];
        singleProductTypes[productType] = componentKey;
      }
    });
    return singleProductTypes;
  }

  loadAreas(project, file){
    this.project = project;
    this.fileType = file.type.toString();
    return this.referenceDataService.getDatumsPromise(project.datumType)
    .then((datumData)=>{
      this.datumData = datumData;
      return this.readBase64(file);
    })
    .then(this.parseWorkbook)
    .then(this.loadFromWorkbook.bind(this));
  }

  readBase64(file): Promise<any> {
    let reader  = new FileReader();
    let future = new Promise((resolve, reject) => {
      reader.addEventListener("load", (data) => {
        resolve(decode(reader.result));
      }, false);

      reader.addEventListener("error", (event)=> {
        reject(event);
      }, false);

      if (reader.readAsBinaryString !== undefined) {
        reader.readAsBinaryString(file);
      }
      else {
        reader.readAsArrayBuffer(file);
      }

      function decode(result) {
        if (typeof result === "string") {
          return result;
        }

        let data = "";
        let chunkSize = 10240;
        for (let offset = 0; offset < result.byteLength; offset += chunkSize) {
          data += String.fromCharCode.apply(null, new Uint8Array(result.slice(offset, offset+chunkSize)));
        }
        return data;
      }
    });
    return future;
  }

  parseWorkbook(data) {
    try {
      let workbook;
      workbook = XLSX.read(data, {type: "binary"});
      return Promise.resolve(workbook);
    }
    catch (e) {
      return Promise.reject(e.message);
    }
  }

  loadFromWorkbook(workbook) {

    let framing = {};
    let counter = 0;
    let startDate = this.project.siteStart;
    var noBracketsPattern = /\[.*?\]/g;
    var noneOfThese = /[.#$/,]/g;


    let response:any = {
      models: [],
      errors:[]
    };

    return loadFromWorkbookProcess(response, workbook)
    //.then(configFramingStyles.bind(this))
    .then(() => {
      let sortToAreas = {};
      response.models.forEach(element => {
        if (this.productTypeProductionLine[element.type] !== undefined && this.datumData[0] !== undefined && this.datumData[0].productionLines[this.productTypeProductionLine[element.type]] !== undefined) {
          var panelAvgOverride = this.datumData[0].productionLines[this.productTypeProductionLine[element.type]].panelAvgOverride;
          var panelAvg = this.datumData[0].productionLines[this.productTypeProductionLine[element.type]].panelAvg;
          if (panelAvgOverride > 0) {
            element.estimatedPanels = Math.round(element.estimatedArea / panelAvgOverride) | 0;
          } else {
            element.estimatedPanels = Math.round(element.estimatedArea / panelAvg) | 0;
          }
        }
        element.component = this.componentsMapped[element.type];
        element.project = this.project.$key;
        if(sortToAreas[element.phase + "-" + element.floor] === undefined){
          sortToAreas[element.phase + "-" + element.floor] = {};
        }
        if(sortToAreas[element.phase + "-" + element.floor][this.productTypeProductionLine[element.type]] === undefined){
          sortToAreas[element.phase + "-" + element.floor][this.productTypeProductionLine[element.type]] = {
            area: element,
            totalArea: 0,
            totalCount: 0,
            framing: {}
          };
        }
        if(sortToAreas[element.phase + "-" + element.floor][this.productTypeProductionLine[element.type]].framing[element.framing] === undefined){
          sortToAreas[element.phase + "-" + element.floor][this.productTypeProductionLine[element.type]].framing[element.framing] = 0;
        }
        sortToAreas[element.phase + "-" + element.floor][this.productTypeProductionLine[element.type]].totalArea += element.estimatedArea;
        sortToAreas[element.phase + "-" + element.floor][this.productTypeProductionLine[element.type]].totalCount += element.estimatedPanels;
        sortToAreas[element.phase + "-" + element.floor][this.productTypeProductionLine[element.type]].framing[element.framing] += element.estimatedArea;
      });
      response.models = [];
      Object.keys(sortToAreas).forEach((key)=>{
        Object.keys(sortToAreas[key]).forEach((key2)=>{
          sortToAreas[key][key2].area.deliveryDate = startDate;
          sortToAreas[key][key2].area.estimatedArea = Number(sortToAreas[key][key2].totalArea.toFixed(1));
          sortToAreas[key][key2].area.estimatedPanels = sortToAreas[key][key2].totalCount;
          Object.keys(sortToAreas[key][key2].area.framing = sortToAreas[key][key2].framing).forEach((frame)=>{
            sortToAreas[key][key2].area.framing[frame] = Number(sortToAreas[key][key2].framing[frame].toFixed(1));
          });
          sortToAreas[key][key2].area.framing = sortToAreas[key][key2].framing;
          response.models.push(sortToAreas[key][key2].area);
        });
        startDate = this.dateUtilService.addWorkingDays(startDate, 2);
      });
      return response;
    });

    function configFramingStyles() {
 
      let configFramingStyles = {};
      let promises = [];

      Object.keys(framing).forEach((framingStyle)=>{
        let framingStylesData = framing[framingStyle];
        let productionLine = this.productTypeProductionLine[framingStylesData.type];
        promises.push(this.projectsService.getFramingStylesPromise()
          .then((framingStyles) => {
            productionLine = this.productTypeProductionLine[framingStylesData.type];
            if(configFramingStyles[framingStyle] === undefined){
              configFramingStyles[framingStyle] = true;
            }
            if(framingStyles[productionLine] !== undefined && framingStyles[framingStyle] !== undefined ){
              return Promise.resolve();
            }else{
              return this.db.object("config/framingStyles/" + productionLine).update(configFramingStyles);
            }
          }));
      });

      return Promise.all(promises);

    }

    function loadFromWorkbookProcess(response, workbook) {

      let deferredList = [];

      let worksheet = workbook.Sheets["DAT_Wall"];
      let range = XLSX.utils.decode_range(worksheet["!ref"]);
      let columnMap = {};
      let row = -1;

      while (++row <= range.e.r) {
        for (var col = 1; col <= range.e.c; col++) {
          var heading = cellValue(worksheet, col, row);
          if (heading) {
            columnMap[heading] = col;
          }
        }
        break;
      }

      while (++row <= range.e.r) {
        let quote = cellValue(worksheet, 0, row);
        if (quote) {
          let phase = cellValue(worksheet, columnMap["EST_Phase"], row);
          let floor = cellValue(worksheet, columnMap["Base Constraint"], row);
          let type = cellValue(worksheet,columnMap["Function"],row);
          let framing = cellValue(worksheet,columnMap["Type"],row);
          let estimatedArea = cellValue(worksheet,columnMap["Area_Net"],row);

          counter++;
          
          deferredList.push(
            validatePanel(phase, floor, type, framing, estimatedArea, row)
            .then((data) => {
              response.models.push(data);
              return Promise.resolve(data);
            })
            .catch((error) => {
              response.errors.push(error);
              return Promise.resolve(error);
            })
          );
        }
      }

      return Promise.all(deferredList);
      
      function validatePanel(phaseData, floorData, typeData, framingData, estimatedAreaData, row) {

        if(typeData){
          typeData = typeData.substring(0, 3);
        }
        
        const productTypes = Object.keys(Srd.productTypes);
        const phases = Object.keys(Srd.phases);
        const floors = Object.keys(Srd.floors);

        let phase;
        let floor;
        let type;
        let estimatedArea;
        let framingInfo;

        productTypes.forEach(data =>{
          if(typeData === data){
            type = data;
          }
        });

        phases.forEach(data =>{
          if(phaseData === data){
            phase = data;
          }
        });

        floors.forEach(data =>{
          if(floorData === data){
            floor = data;
          }
        });

        if(!isNaN(estimatedAreaData)){
          estimatedArea = estimatedAreaData;
        }

        if(framingData !== "" && !noBracketsPattern.test(framingData) && !noneOfThese.test(framingData)){
          framingInfo = framingData;
        }

        if (phase && floor && type && framingInfo && estimatedArea) {

          let section = {
            phase: "",
            floor: "",
            type: "",
            framing: "",
            estimatedArea: ""
          }

          if(framing[framingData] === undefined){
            counter++;
            framing[framingData] = {
              counter: counter,
              type: type
            }
          }

          section.floor = floor;
          section.phase = phase;
          section.type = type;
          section.framing = framingInfo;
          section.estimatedArea = estimatedArea;

          return Promise.resolve(section);

        }else {

          let errors = {
            row: row + 1,
            phase: {
              message: phase ? phase : "No match", 
              correct: phase ? true : false
            },
            floor: {
              message: floor ? floor : "No match", 
              correct: floor ? true : false
            },
            type: {
              message: type ? type : "No match",
              correct: type ? true : false
            },
            framing: {
              message: framingInfo ? framingInfo : "Contains '','.','#','$','/',','[' or ']'", 
              correct: framingInfo ? true : false
            },
            estimatedArea: {
              message: estimatedArea ? estimatedArea : "Not a number", 
              correct: estimatedArea ? true : false
            }
          }

          return Promise.reject(errors);
        }

      }  


      function cellValue(worksheet, column, row) {
        var cell_address = XLSX.utils.encode_cell({c: column, r: row});
        var cell = worksheet[cell_address];
        return cell ? cell.v : undefined;
      }
    }

  }

}
