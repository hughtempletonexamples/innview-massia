import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class DateUtilService {

  constructor() { }

  isWeekDay(value) {
    var day = this.asDate(value).getDay();
    return day >= 1 && day <= 5;
  }

  dayOfWeek(value) {
    var date = this.asDate(value);
    return (date.getDay() + 6) % 7;
  }

  todayAsIso() {
    return this.toIsoDate(new Date());
  }

  plusDays(value, days) {
    var date = new Date(value);
    date.setDate(date.getDate() + days);
    return this.toIsoDate(date);
  }
  
  minusDays(value, days) {
    var date = new Date(value);
    date.setDate(date.getDate() - days);
    return this.toIsoDate(date);
  }
  
  dateMonth(value) {
    var date = this.asDate(value);
    return date.getFullYear()+"-" + ("0" + (date.getMonth()+1)).slice(-2);
  }

  weekStart(value) {
    if (value !== undefined) {
      var date = new Date(value);
    } else {
      var date = new Date();
    }
    date.setDate(date.getDate() - this.dayOfWeek(date));

    return this.toIsoDate(date);
  }

  subtractWorkingDays(startDate, days) {
    var date = this.asDate(startDate);

    while (days > 0) {
      date.setDate(date.getDate()-1);
      if (this.isWeekDay(date)) {
        days--;
      }
    }
    return this.toIsoDate(date);
  }

  subtractWorkingDaysAndSat(startDate, days) {
    var date = this.asDate(startDate);

    while (days > 0) {
      date.setDate(date.getDate()-1);
      if (!this.isSun(date)) {
        days--;
      }
    }
    return this.toIsoDate(date);
  }

  asDate(value) {
    return value === undefined ? value : new Date(value);
  }

  isDateFormat(value) {
    return value instanceof Date && !isNaN(+value);
  }

  mapper(buildObj){
    let objHolder = {};
    buildObj.map((snap) => {
      objHolder[snap.key] = snap.payload.val();
    });
    return objHolder;
  }
  areaMapper(buildObj){
    let objHolder = {};
    buildObj.map((snap) => {
      objHolder[snap.$key] = snap;
    });
    return objHolder;
  }
  
  validDate(date) {
    date = this.asDate(date);
    var isDate = false;
    if (moment.isDate(date)) {
      isDate = true;
      if (isNaN(date.getTime())) {  // d.valueOf() could also work
        isDate = false;
      } else {
        isDate = true;
      }
    } else {
      isDate = false;
    }
    return isDate;
  }

  toUserDate(value) {
    var date = this.asDate(value);
    return ("0" + date.getDate()).slice(-2) + "-" + ("0" + (date.getMonth()+1)).slice(-2) + "-" + date.getFullYear();
  }

  toUserSlachDate(value) {
    var date = this.asDate(value);
    return ("0" + date.getDate()).slice(-2) + "/" + ("0" + (date.getMonth()+1)).slice(-2) + "/" + date.getFullYear();
  }

  toIsoDate(value) {
    var date = this.asDate(value);
    return date.getFullYear()+"-" + ("0" + (date.getMonth()+1)).slice(-2) + "-"+ ("0" + date.getDate()).slice(-2);
  }

  daysBetween(from, to) {
    var toDate = this.asDate(to);
    var fromDate = this.asDate(from);
    var millis = toDate.getTime() - fromDate.getTime();
    return Math.round(millis / 1000 / 24 / 60 / 60);
  }

  addWorkingDays(startDate, days) {
    var date = this.asDate(startDate);

    while (days > 0) {
      date.setDate(date.getDate()+1);
      if (this.isWeekDay(date)) {
        days--;
      }
    }
    return this.toIsoDate(date);
  }

  addWorkingDaysAndSat(startDate, days) {
    var date = this.asDate(startDate);

    while (days > 0) {
      date.setDate(date.getDate()+1);
      if (!this.isSun(date)) {
        days--;
      }
    }
    return this.toIsoDate(date);
  }

  daysBetweenWeekDaysOnly(from, to) {
    var toDate = this.asDate(to);
    var fromDate = this.asDate(from);
    var minusWeekends = 0;
    if (fromDate < toDate){
      while(fromDate < toDate){
        if (this.isWeekDay(fromDate)){
          minusWeekends++;
        }
        fromDate = this.plusDays(fromDate, 1);
        fromDate = this.asDate(fromDate);
      }
    } else if (fromDate > toDate){
      while(fromDate > toDate){
        if (this.isWeekDay(fromDate)){
          minusWeekends--;
        }
        fromDate = this.minusDays(fromDate, 1);
        fromDate = this.asDate(fromDate);
      }
    }
    return minusWeekends;
  }

  daysBetweenWeekDaysAndSat(from, to) {
    var toDate = this.asDate(to);
    var fromDate = this.asDate(from);
    var minusSun = 0;
    if (fromDate < toDate){
      while(fromDate < toDate){
        if (!this.isSun(fromDate)){
          minusSun++;
        }
        fromDate = this.plusDays(fromDate, 1);
        fromDate = this.asDate(fromDate);
      }
    } else if (fromDate > toDate){
      while(fromDate > toDate){
        if (!this.isSun(fromDate)){
          minusSun--;
        }
        fromDate = this.minusDays(fromDate, 1);
        fromDate = this.asDate(fromDate);
      }
    }
    return minusSun;
  }

  isSun(value) {
    var day = this.asDate(value).getDay();
    return day === 0;
  }

  buildMapKey(items, property) {
    const map = {};
    Object.keys(items).forEach(function (itemkey) {
      let item = items[itemkey];
      map[item.$key] = item[property];
    });
    return map;
  }

  buildMap(items, property) {
    const map = {};
    Object.keys(items).forEach(function (itemkey) {
      let item = items[itemkey];
      map[itemkey] = item[property];
    });
    return map;
  }

  buildMapReverse(items, property) {
    const map = {};
    Object.keys(items).forEach(function (itemkey) {
      let item = items[itemkey];
      map[item[property]] = itemkey;
    });
    return map;
  }

  hasNumber(myString) {
    return /\d/.test(myString);
  }

  fieldSorter(fields) {
    return function (a, b) {
        return fields
            .map(function (o) {
                var dir = 1;
                if (o[0] === '-') {
                   dir = -1;
                   o=o.substring(1);
                }
                if (a[o] > b[o]) return dir;
                if (a[o] < b[o]) return -(dir);
                return 0;
            })
            .reduce(function firstNonZeroValue (p,n) {
                return p ? p : n;
            }, 0);
    };
}

}
