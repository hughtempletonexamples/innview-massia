import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireObject, AngularFireList } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { map, combineLatest } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { DateUtilService } from './date-util.service';

@Injectable({
  providedIn: 'root'
})
export class PanelsService {


  private basePath = '/panels';
  panelsRef: AngularFireList<any>;
  activePanelsRef: AngularFireList<any>;
  panelRef: AngularFireObject<any>;

  panels: Observable<any>;
  panel: Observable<any>;
  stats: Observable<any>;

  constructor(private db: AngularFireDatabase, private authService: AuthService, private dateUtilService: DateUtilService) {
    this.panelsRef = this.db.list(this.basePath);
  }

  getpanelsList() {
    return this.panelsRef.snapshotChanges();
  }

  uncompletePanel(panelStats, panel){
    let updates = {};
    if (panel.qa !== undefined && panel.qa["completed"] !== undefined) {
      panelStats.areaCompleted = Math.round((panelStats.areaCompleted - panel.dimensions.area)*10)/10;
      panelStats.completed--;
      updates["panels/" + panel.$key + "/qa"] = null;
      updates["panels/" + panel.$key + "/timestamps/modified"] = this.authService.serverTime();
      updates["areas/" + panel.area + "/completedArea"] = panelStats.areaCompleted;
      updates["areas/" + panel.area + "/completedPanels"] = panelStats.completed;
      updates["areas/" + panel.area + "/timestamps/modified"] = this.authService.serverTime();
    }
    if(Object.keys(updates).length > 0){
      return this.db.database.ref().update(updates);
    }else{
      return Promise.resolve();
    } 
  }

  completeSetected(panelStats, selection, dateComplete){
    let updates = {};
    let area;
    let today = this.dateUtilService.todayAsIso();
    dateComplete = this.dateUtilService.toIsoDate(dateComplete);
    if(today === dateComplete){
      dateComplete = new Date(dateComplete).getTime();
    }else{
      dateComplete = this.authService.serverTime();
    }
    dateComplete = new Date(dateComplete).getTime();
    selection.selected.forEach(panelStat => {
      let panel = panelStat.panel;
      panelStats.areaCompleted = Math.round((panelStats.areaCompleted + panel.dimensions.area)*10)/10;
      panelStats.completed++;
      area = panel.area;
      updates["panels/" + panel.$key + "/qa/started"] = dateComplete;
      updates["panels/" + panel.$key + "/qa/completed"] = dateComplete;
      updates["panels/" + panel.$key + "/timestamps/modified"] = this.authService.serverTime();
    });

    if(Object.keys(updates).length > 0){
      updates["areas/" + area + "/completedArea"] = panelStats.areaCompleted;
      updates["areas/" + area + "/completedPanels"] = panelStats.completed;
      updates["areas/" + area + "/timestamps/modified"] = this.authService.serverTime();
      return this.db.database.ref().update(updates);
    }else{
      return Promise.resolve();
    } 
  }

  deleteSetected(panelStats, selection){
    let updates = {};
    let area;
    selection.selected.forEach(panelStat => {
      let panel = panelStat.panel;
      panelStats.actualArea = Math.round((panelStats.actualArea - panel.dimensions.area)*10)/10;
      panelStats.actualPanels--;
      area = panel.area;
      updates["panels/" + panel.$key] = null;
    });
    if(Object.keys(updates).length > 0){
      updates["areas/" + area + "/actualArea"] = panelStats.actualArea;
      updates["areas/" + area + "/actualPanels"] = panelStats.actualPanels;
      updates["areas/" + area + "/timestamps/modified"] = this.authService.serverTime();
      return this.db.database.ref().update(updates);
    }else{
      return Promise.resolve();
    } 
  }

  getPanelsForArea(key: string): Observable<any> {
    const path = this.basePath;
    this.panels = this.db.list(path, areasRef => areasRef.orderByChild('area').equalTo(key)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
    return this.panels;
  }

  getPanelsCompleteForRange(start, end){
    const path = this.basePath;
    this.panels = this.db.list(path, areasRef => areasRef.orderByChild("qa/completed").startAt(start).endAt(end)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
    return this.panels;
  }

  getlastPanels(){
    const path = this.basePath;
    this.panels = this.db.list(path, areasRef => areasRef.orderByKey().limitToLast(6000)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
    return this.panels;
  }

  getPanelsStartedForRange(start, end){
    const path = this.basePath;
    this.panels = this.db.list(path, areasRef => areasRef.orderByChild("qa/started").startAt(start).endAt(end)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
    return this.panels;
  }

  getStatsForDay(productionLine: string, date: string) {
    const path = `${'stats'}/${productionLine}/${date}`;
    this.stats = this.db.object(path).snapshotChanges();
    return this.stats;
  }

  getCapacityForDay(productionLine: string, date: string) {
    const path = `${'capacity'}/${productionLine}/${date}`;
    this.stats = this.db.object(path).snapshotChanges();
    return this.stats;
  }

  getProPerformanceData(date: string){
    return combineLatest([
      this.getCapacityForDay("SIP",date),
      this.getCapacityForDay("HSIP",date),
      this.getCapacityForDay("TF",date),
      this.getCapacityForDay("CASS",date),
      this.getStatsForDay("SIP",date),
      this.getStatsForDay("HSIP",date),
      this.getStatsForDay("TF",date),
      this.getStatsForDay("CASS",date)
    ]);
  }

  getpanel(key: string): Observable<any> {
    const path = `${this.basePath}/${key}`;
    this.panel = this.db.object(path).valueChanges();
    return this.panel;
  }

  createpanel(panel) {
    this.panelsRef.push(panel);
  }

  deletepanel(key: string) {
    this.panelsRef.remove(key);
  }

}