import {map, switchMap, first, takeWhile, withLatestFrom, flatMap} from 'rxjs/operators';
import { Injectable, Inject, LOCALE_ID } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import { Observable, Subject, combineLatest, of } from 'rxjs';
import { ProjectsService } from './projects.service';
import { DateUtilService } from './date-util.service';
import { PanelsService } from './panels.service';
import Srd  from '../../../assets/srd/srd.json';
import { Blotter, ExtendedArea, ProductionPlan, ComponentsHolder, _stats } from '../models/models';
import { AreasService } from './areas.service';
import { StatusService } from './status.service';
import { formatDate } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class ForwardPlannerService {

  private basePath = '/productionPlans/areas';
  productionLines: object;
  productionLinesForStats: object;
  areasproductionPlansRefRef: AngularFireList<any>;
  areaproductionPlansRef: AngularFireObject<any>;
  blotter: Blotter;
  extendedArea: ExtendedArea;
  productTypeProductionLine: object;
  productTypes:object;
  areasSelection:object;
  take: boolean;
  stats: Observable<any>;
  plannerViewPeriod: number;
  components: ComponentsHolder[];

  designHandoverPeriod = Srd.defaultSettings.designHandoverPeriod;
  designHandoverBuffer = Srd.defaultSettings.designHandoverBuffer;
  suppliers = Srd.suppliers;
  thirdPartySuppliers = this.dateUtilService.buildMap(this.suppliers, "thirdParty");

  items$: Observable<any>;
  project$: Subject<object|null>;
  date$:Observable<any>;
  dateRef$:Subject<string|null>;

  constructor(
    @Inject(LOCALE_ID) private locale: string,
    private db: AngularFireDatabase, 
    private panelsService: PanelsService, 
    private areasService: AreasService, 
    private projectsService: ProjectsService, 
    private dateUtilService: DateUtilService,
    private statusService: StatusService) { 

    this.areasproductionPlansRefRef = this.db.list(this.basePath);

    this.take = true;

    this.productionLines = Srd.productionSubLines;
    this.productionLinesForStats = Srd.productionLines;
    this.areasSelection ={};
    this.productTypes = Srd.productTypes;
    this.plannerViewPeriod = Srd.defaultSettings.plannerViewPeriod;
    this.productTypeProductionLine = this.dateUtilService.buildMap(this.productTypes, "productionLine");
    this.dateRef$ = new Subject<string>();


    this.project$ = new Subject<object>();
    this.items$ = this.project$.pipe(
      flatMap(project =>
        this.getProjectStats(project)
      )
    );

  }

  getDate(){
    return this.dateRef$
  }

  setDate(dateRef){
    return this.dateRef$.next(dateRef);
  }

  setProjectStats(project){
    return this.project$.next(project);
  }

  isProjectStatsComplete(){
    return this.project$.complete();
  }

  retrieveProjectStats(){
    return this.items$;
  }

  updateStartDateCounter(){
    const increment = 1;
    const startDateCounter = this.db.database.ref().child("config/startDateCounter");
    return startDateCounter.transaction(( current ) => {
      return (current || 0) + increment;
    });
  }

  setPlannedStart(area, plannedStart, change){
    const itemRef = this.db.object(`${this.basePath}/${area.$key}`);
    return itemRef.update({ plannedStart: plannedStart, userChange: change}).then((_)=>{
       this.updateStartDateCounter();
    });
  }

  removeUserChange(area){
    const itemRef = this.db.object(`${this.basePath}/${area.$key}`);
    return itemRef.update({userChange: null}).then((_)=>{
      this.updateStartDateCounter();
    });
  }

  setCapacity(plan, productionLine, dateCount, panelCount) {
      
    let totalPanelCount = 0;
    let productionLineFiltered = productionLine.replace(/[0-9]/g, '');
    dateCount = parseInt(dateCount);
    let date = this.dateUtilService.plusDays(plan.viewStart,dateCount);

    let dateObj = this.dateUpdate(date, panelCount);

    const promise = this.db.object('productionSubCapacities/'+productionLine).update(dateObj);
    return promise
    .then(_ => {
      let toAdd = 0;
      let toUpdate = panelCount;
      if(this.dateUtilService.hasNumber(productionLine)){
        if(plan.capacity.count[productionLineFiltered] !== undefined && plan.capacity.count[productionLineFiltered][dateCount] !== undefined){
          toAdd = plan.capacity.count[productionLineFiltered][dateCount];
        }
      }else{
        let productionLine= productionLineFiltered+"2";
        if(plan.capacity.count[productionLine] !== undefined && plan.capacity.count[productionLine][dateCount] !== undefined){
          toAdd = plan.capacity.count[productionLine][dateCount];
        }
      }

      totalPanelCount = toUpdate + toAdd;

      dateObj = this.dateUpdate(date, totalPanelCount);
      
      const productionCapacities = this.db.object('productionCapacities/'+productionLineFiltered);
      return productionCapacities.update(dateObj);
    })
    .catch(err => console.log(err, 'Error happened in set capacity! Tell Developer'));

  }

  dateUpdate(date, panels){
    const dateUpdate = {};
    if (dateUpdate[date] === undefined){
      dateUpdate[date] = panels;
    }
    return dateUpdate;
  }
 
  getPlannerSort() {
    return this.db.object("config/plannerSort").snapshotChanges();
  }

  getStartDateCounter() {
    return this.db.object("config/startDateCounter").snapshotChanges();
  }

  setPlannerSort(updateObj) {
    const itemRef = this.db.object('config/plannerSort');
    return itemRef.set(updateObj);
  }

  removePlannerSort() {
    const itemRef = this.db.object('config/plannerSort');
    return itemRef.remove();
  }

  getCapacitiesFromToday() {
    const todayDate = new Date();
    const start = this.dateUtilService.toIsoDate(todayDate);
    return this.db.list("productionSubCapacities", capRef => capRef.orderByKey().startAt(start)).snapshotChanges();
  }

  getStatsSelectRange(start, end) {
    let toCombine = [];
    let productionLines = [];
    Object.keys(this.productionLinesForStats).map(productionLine => {
      productionLines.push(productionLine);
      const path = `${'stats'}/${productionLine}`;
      toCombine.push(this.db.list(path, capRef => capRef.orderByKey().startAt(start).endAt(end)).snapshotChanges());
    });
    return combineLatest(toCombine).pipe(map(arr => {
      var group = {};
      arr.forEach((proStats,key) => {
        let productionLine = productionLines[key];
        if(group[productionLine] === undefined){
          group[productionLine] = {};
        }
        proStats.forEach((stat) => {
          if(group[productionLine][stat.key] === undefined){
            group[productionLine][stat.key] = {};
          }
          group[productionLine][stat.key] = stat.payload.val();
        });
      });
      return group;
    }));
  }

  extendArea(project, area, areaKey){

    let extendedArea = {
      $key: areaKey,
      _productionLine: "",
      _line: 0,
      _projectDetails: {},
      _stats: {},
      _deliveryDate: "",
      _hasUnpublished: false,
      _hasUnpublishedOnly: false,
      _id: "",
      _projId:"",
      _dispatchDate: "",
      _sortKey: 0,
      _riskDate:"",
      _designHandoverDate:"",
      _unpublishedDeliveryDate:"",
      _unpublishedDispatchDate:"",
      _unpublishedRiskDate:"",
      _unpublishedDesignHandoverDate:"",
    };
    let deliveryDate = area.revisions[project.deliverySchedule.published];
    let unpublishedDeliveryDate = area.revisions[project.deliverySchedule.unpublished];

    if (deliveryDate || unpublishedDeliveryDate) {
      if (area.line > 1){
        var productionLine = this.productTypeProductionLine[area.type] || null;
        if (productionLine === "SIP"){
          extendedArea._productionLine =  productionLine + area.line;
          extendedArea._line = area.line;
        }else if (productionLine === "TF"){
          extendedArea._productionLine =  productionLine + area.line;
          extendedArea._line = area.line;
        }else{
          extendedArea._productionLine =  productionLine;
          extendedArea._line = 1;
        }           
      }else{
        extendedArea._productionLine = this.productTypeProductionLine[area.type] || null;
        extendedArea._line = 1;
      }
      extendedArea._projectDetails = project;
      extendedArea._stats = this.getPanelsCount(area);
      extendedArea._deliveryDate = deliveryDate || unpublishedDeliveryDate;
      extendedArea._hasUnpublished = unpublishedDeliveryDate && !(extendedArea._deliveryDate && extendedArea._deliveryDate == unpublishedDeliveryDate);
      extendedArea._hasUnpublishedOnly = !deliveryDate;
      extendedArea._id = this.getAreaId(area, project);
      extendedArea._projId = project.id;
      extendedArea._dispatchDate = this.dateUtilService.subtractWorkingDays(extendedArea._deliveryDate, 1);
      extendedArea._riskDate = this.dateUtilService.subtractWorkingDays(extendedArea._deliveryDate, 2);
      extendedArea._designHandoverDate = this.dateUtilService.subtractWorkingDays(extendedArea._deliveryDate, this.plannerViewPeriod);
      if (extendedArea._hasUnpublished) {
        extendedArea._unpublishedDeliveryDate = unpublishedDeliveryDate;
        extendedArea._unpublishedDispatchDate = this.dateUtilService.subtractWorkingDays(unpublishedDeliveryDate, 1);
        extendedArea._unpublishedRiskDate = this.dateUtilService.subtractWorkingDays(unpublishedDeliveryDate, 2);
        extendedArea._unpublishedDesignHandoverDate = this.dateUtilService.subtractWorkingDays(unpublishedDeliveryDate, this.plannerViewPeriod);
      }
      return Object.assign(area, extendedArea);
    }

  }

  extendAreaFilter(area){
    if (area.supplier === "Innovaré" && (!area.completedPanels || area.completedPanels < area.actualPanels)) {
      return area;
    }
  }

  getAreaId(area, project) {
    return [project.id, area.phase, area.floor, area.type].join("-");
  }  

  getActiveAreas(){
    return this.projectsService.getActiveProjectsList().pipe(
      switchMap((itemKeys) => {
        let arrToCombine = [];
        itemKeys.map(project => {
          arrToCombine.push(this.areasService.getAreasForProject(project.$key).pipe(
            map(arr => {
              return arr.filter((snap) => {
                return this.extendAreaFilter(snap.payload.val());
              });
            }),
            map(arr => {
              return arr.map((snap) => {
                return this.extendArea(project, snap.payload.val(), snap.payload.key);
              });
            })));
        });
        return combineLatest(arrToCombine, (...arrays) => arrays.reduce((acc, array) => [...acc, ...array], [])).pipe(map(arr => {
          var sortKey = 0;
          arr.forEach(element => {
            element._sortKey = sortKey;
            sortKey++;
          });
          return arr;
        }));
      }));
  }

  projectsStatus(){
    return this.projectsService.getActiveProjectsList().pipe(
      switchMap((itemKeys) => {
        let arrToCombine = [];
        itemKeys.map(project => {
          arrToCombine.push(this.areasService.getAreasForProject(project.$key).pipe(
            map(arr => {
              return arr.filter((snap) => {
                return this.extendAreaFilter(snap.payload.val());
              });
            }),
            map(arr => {
              return arr.map((snap) => {
                return this.extendArea(project, snap.payload.val(), snap.payload.key);
              });
            })));
        });
        return combineLatest(arrToCombine, (...arrays) => arrays.reduce((acc, array) => [...acc, ...array], [])).pipe(map(arr => {
          var sortKey = 0;
          arr.forEach(element => {
            element._sortKey = sortKey;
            sortKey++;
          });
          return arr;
        }));
      }));
  }
  
  getPlansAndAdd(){
    return this.getActiveAreas().pipe(
      switchMap(arr => {
        let toCombine = [];
        arr.map((area) => {
          toCombine.push(this.getEachAreaProductionPlans(area).pipe(map(snap => {
            if(snap.key !== null){
              return Object.assign(snap.payload.val(), {$key: snap.key, $type: "plan"});
            }
          })));
        });
        arr.map((area) => {
          toCombine.push(this.getEachAreaAdditionalData(area).pipe(map(snap => {
            if(snap.key !== null){
              return Object.assign(snap.payload.val(), {$key: snap.key, $type: "additionalData"});
            }
          })));
        });
        return combineLatest(toCombine, (...arrays) => arrays.reduce((acc, array) => [...acc, ...array], [])).pipe(map(snap => {
          let plans = {};
          let additionalData = {};
          snap.map((data) => {
            if(data !== undefined){
              if(data.$type === "plan"){
                plans[data.$key] = data;
              }else{
                additionalData[data.$key] = data;
              }
            }
          });
          arr.map((area) => {
            return Object.assign(area, {plan: plans[area.$key]});
          });
          return arr.map((area) => {
            return Object.assign(area, {additionalData: additionalData[area.$key]});
          });
        }));
      })
    );
  }

  getPerformance(today, start, end){

    today = this.dateUtilService.toIsoDate(new Date(today));
    let startStamp = new Date(start).setHours(0,0,0,0);
    let endStamp = new Date(end).setHours(23,59,59,999);

    let defaultCapacities = this.dateUtilService.buildMap(this.productionLinesForStats, "defaultCapacity");

    return combineLatest([
      this.panelsService.getPanelsCompleteForRange(startStamp, endStamp), 
      this.panelsService.getPanelsStartedForRange(startStamp, endStamp), 
    ]).pipe(
      switchMap(([completed, started]) => {
        let arrToCombine = [];
        Object.keys(this.productionLinesForStats).map(productionLine => {
          arrToCombine.push(this.getCapacityForDay(productionLine, today).pipe(map(snap => {
            if(snap !== null && snap !== undefined && snap.payload !== undefined){
              let cap = snap.payload.val();
              let pl = snap.payload.ref.parent.key;
              if(cap === null){
                cap = defaultCapacities[pl];
              }
              return Object.assign({}, {$key: pl, capacity: cap});
            }
          })));
        });  
        return combineLatest(arrToCombine, (...arrays) => arrays.reduce((acc, array) => [...acc, ...array], [])).pipe(map(arr => {
          let capacities = this.crateCap(arr);
          let stats = this.crateStats(today, completed, started);
          return [capacities, stats];
        }));
      })
    );
  }

  crateCap(arr){
    let returnData = {};
    arr.forEach(cap => {
      if(returnData[cap.$key] === undefined){
        returnData[cap.$key] = cap.capacity;
      }
    });
    return returnData;
  }

  crateStats(today, completedSnap, startedSnap){

    let returnData = {
      each: {},
      totalStartedToday: 0,
      totalCompletedToday: 0,
      totalStartedM2Today: 0,
      totalCompletedM2Today: 0,
      totalCompleted: 0,
      totalStarted: 0,
      totalCompletedM2: 0,
      totalStartedM2: 0
    };

    Object.keys(this.productionLinesForStats).map(productionLine => {
      if(returnData.each[productionLine] === undefined){
        returnData.each[productionLine] = {
          startedToday: 0,
          completedToday: 0,
          startedM2Today: 0,
          completedM2Today: 0,
          completed: 0,
          started: 0,
          completedM2: 0,
          startedM2: 0
        }
      }
    });

    completedSnap.map((panel) => {
      let productionLine = this.productTypeProductionLine[panel.type];
      if(panel.qa !== undefined && panel.qa.completed !== undefined){
        let panelCompleted = this.dateUtilService.toIsoDate(panel.qa.completed);
        if(panelCompleted === today){
          returnData.each[productionLine].completedToday++;
          returnData.each[productionLine].completedM2Today += panel.dimensions.area;
          returnData.totalCompletedToday++;
          returnData.totalCompletedM2Today += panel.dimensions.area;;
        }
        returnData.each[productionLine].completed++;
        returnData.each[productionLine].completedM2 += panel.dimensions.area;
        returnData.totalCompleted++;
        returnData.totalCompletedM2 += panel.dimensions.area;
      }

    });

    startedSnap.map((panel) => {
      let productionLine = this.productTypeProductionLine[panel.type];
      if(panel.qa !== undefined && panel.qa.started !== undefined && panel.qa.completed === undefined){
        let panelCompleted = this.dateUtilService.toIsoDate(panel.qa.started);
        if(panelCompleted === today){
          returnData.each[productionLine].startedToday++;
          returnData.each[productionLine].startedM2Today += panel.dimensions.area;
          returnData.totalStartedToday++;
          returnData.totalStartedM2Today += panel.dimensions.area;
        }
        returnData.each[productionLine].started++;
        returnData.each[productionLine].startedM2 += panel.dimensions.area;
        returnData.totalStarted++;
        returnData.totalStartedM2 += panel.dimensions.area;
      }

    });

    Object.keys(returnData.each).map((productionLine) => {
      returnData.each[productionLine].startedToday = Math.round(returnData.each[productionLine].startedToday*10)/10;
      returnData.each[productionLine].completedToday = Math.round(returnData.each[productionLine].completedToday*10)/10;
      returnData.each[productionLine].startedM2Today = Math.round(returnData.each[productionLine].startedM2Today*10)/10;
      returnData.each[productionLine].completedM2Today = Math.round(returnData.each[productionLine].completedM2Today*10)/10;
      returnData.each[productionLine].completed = Math.round(returnData.each[productionLine].completed*10)/10;
      returnData.each[productionLine].started = Math.round(returnData.each[productionLine].started*10)/10;
      returnData.each[productionLine].completedM2 = Math.round(returnData.each[productionLine].completedM2*10)/10;
      returnData.each[productionLine].startedM2 = Math.round(returnData.each[productionLine].startedM2*10)/10;
    });

    returnData.totalStartedToday = Math.round(returnData.totalStartedToday*10)/10;
    returnData.totalCompletedToday = Math.round(returnData.totalCompletedToday*10)/10;
    returnData.totalStartedM2Today = Math.round(returnData.totalStartedM2Today*10)/10;
    returnData.totalCompletedM2Today = Math.round(returnData.totalCompletedM2Today*10)/10;
    returnData.totalCompleted = Math.round(returnData.totalCompleted*10)/10;
    returnData.totalStarted = Math.round(returnData.totalStarted*10)/10;
    returnData.totalCompletedM2 = Math.round(returnData.totalCompletedM2*10)/10;
    returnData.totalStartedM2 = Math.round(returnData.totalStartedM2*10)/10;
      
    return returnData;

  }

  getDesignIssued(){
    return this.panelsService.getlastPanels().pipe(map(arr => {
      let returnData = {
        each: {},
        totalDesignIssued: 0,
        totalDesignIssuedM2: 0
      };
      arr.map((panel) => {
        let productionLine = this.productTypeProductionLine[panel.type];
        if(returnData.each[productionLine] === undefined){
          returnData.each[productionLine] = {
            completed: 0,
            count: 0,
            completedM2: 0,
            countM2: 0,
            designIssued: 0,
            designIssuedM2: 0,
          }
        }
        if(panel.qa !== undefined && panel.qa.completed !== undefined){
          returnData.each[productionLine].completed++;
          returnData.each[productionLine].completedM2 += panel.dimensions.area;
        }
        returnData.each[productionLine].count++;
        returnData.each[productionLine].countM2 += panel.dimensions.area;
      });
      Object.keys(returnData.each).map((productionLine) => {
        returnData.each[productionLine].countM2 = Math.round(returnData.each[productionLine].countM2*10)/10;
        returnData.each[productionLine].completedM2 = Math.round(returnData.each[productionLine].completedM2*10)/10;
        returnData.each[productionLine].designIssued = returnData.each[productionLine].count - returnData.each[productionLine].completed;
        returnData.each[productionLine].designIssuedM2 = returnData.each[productionLine].countM2 - returnData.each[productionLine].completedM2;
        returnData.each[productionLine].designIssued = Math.round(returnData.each[productionLine].designIssued*10)/10;
        returnData.each[productionLine].designIssuedM2 = Math.round(returnData.each[productionLine].designIssuedM2*10)/10;
        returnData.totalDesignIssued += returnData.each[productionLine].designIssued;
        returnData.totalDesignIssuedM2 += returnData.each[productionLine].designIssuedM2;
      });
      returnData.totalDesignIssuedM2 = Math.round(returnData.totalDesignIssuedM2*10)/10;
      return returnData;
    }))
  }

  getActiveAreasAndCapacity(start, end){
    return combineLatest([
      this.getActiveAreas(),
      this.getCapacitiesFromToday(), 
      this.getPlannerSort(),
      this.panelsService.getPanelsCompleteForRange(start, end),
      this.getStartDateCounter().pipe(withLatestFrom(this.getAreaProductionPlans()))
    ]);
  }

  getStatsForDay(productionLine: string, date: string) {
    const path = `${'stats'}/${productionLine}/${date}`;
    this.stats = this.db.object(path).snapshotChanges();
    return this.stats;
  }

  getCapacityForDay(productionLine: string, date: string) {
    const path = `${'productionCapacities'}/${productionLine}/${date}`;
    this.stats = this.db.object(path).snapshotChanges();
    return this.stats;
  }

  getAreaProductionPlans(){
    return this.db.list("productionPlans/areas").snapshotChanges();
  }

  getAreaAdditionalData(){
    return this.db.list("additionalData/areas").snapshotChanges();
  }

  getEachAreaAdditionalData(area){
    const path = "additionalData/areas";
    return this.db.object(`${path}/${area.$key}`).snapshotChanges();
  }

  getCapacities(){
    return this.db.list("productionCapacities").snapshotChanges();
  }

  getStats(){
    return this.db.list("stats").snapshotChanges();
  }

  getProductionPerformanceData(today){
    let start = this.dateUtilService.subtractWorkingDays(today, 15);
    let end = this.dateUtilService.addWorkingDays(today, 15);
    return combineLatest([this.getPerformance(today, start, end), this.getPlansAndAdd()])
  }

  getAreaProductionPlansTakeOnce(){
    return this.db.list("productionPlans/areas").snapshotChanges().pipe(takeWhile(val => this.take));
  }

  getEachAreaProductionPlansPromise(area) {
    return this.db.object(`${this.basePath}/${area.$key}`).snapshotChanges().pipe(first()).toPromise();
  }

  getEachAreaProductionPlans(area) {
    return this.db.object(`${this.basePath}/${area.$key}`).snapshotChanges();
  }

  buildAreasPlan(plan){
    const updates = {};
    const itemRef = this.db.object(`${this.basePath}`);
    const promiseArr = [];
    plan.areas.map((area) => {
      promiseArr.push(refreshAreaProductionPlan(this, area, plan));   
    });

    return Promise.all(promiseArr)
            .then((_)=>{
              return itemRef.update(updates);
            });


    function refreshAreaProductionPlan(that, area, plan) {

      return that.getEachAreaProductionPlansPromise(area)
      .then((snap)=>{
        const today = that.dateUtilService.todayAsIso();    
        let buildDays = {};
        if (area._productionPlan !== undefined){
          area._productionPlan.map((panels, key) => {
            var date = that.dateUtilService.plusDays(plan.viewStart, key);
            //date greater than today because plan is effected now by panels complete on day
            if(panels["count"] !== null && date > today){
              buildDays[date] = panels["count"];
            }
          });
          
          var areaPlan: ProductionPlan = {
            timestamps: {
              created:{'.sv': 'timestamp'},
              modified: {'.sv': 'timestamp'}
            },
            project: area.project,
            plannedStart: area._productionStartDate,
            plannedFinish: area._productionEndDate,
            userChange: area._userChange,
            riskDate: area._riskDate,
            buildDays: buildDays
          };

          var daysChanged = false;

          let existing = snap.payload.val();
          if (existing !== null) {
            areaPlan.timestamps.created = existing.timestamps.created;
            if(existing.buildDays !== undefined){
              Object.keys(existing.buildDays).map((buildDate ) => {
                let amount = existing.buildDays[buildDate];
                if (buildDays[buildDate] !== amount) {
                  daysChanged = true;
                }
              });
            }
          }
          if (existing === null
            || existing.plannedStart !== areaPlan.plannedStart
            || existing.plannedFinish !== areaPlan.plannedFinish
            || existing.riskDate !== areaPlan.riskDate
            || daysChanged) {  
              console.log("save: " + areaPlan)
              updates[area.$key] = areaPlan;                
          }
        }
      });
    }
  }

  avgM2(area, buildCount){

    let avg = 0;
    let amount = 0;
    if (area.actualPanels !== undefined && area.actualPanels > 0) {
      if (area.actualArea !== undefined && area.actualArea > 0) {
        avg = area.actualArea / area.actualPanels;
        amount = avg * buildCount;
      }
    } else if (area.numberOfPanels !== undefined && area.numberOfPanels > 0) {
      if (area.areaOfPanels !== undefined && area.areaOfPanels > 0) {
        avg = area.areaOfPanels / area.numberOfPanels;
        amount = avg * buildCount;
      }
    } else if (area.estimatedPanels !== undefined && area.estimatedPanels > 0) {
      if (area.estimatedArea !== undefined && area.estimatedArea > 0) {
        avg = area.estimatedArea / area.estimatedPanels;
        amount = avg * buildCount;
      }
    }
    return amount;

  }

  getPanelsCount( area ) {

    const stats: _stats = {
      type:"none",
      count:0,
      display:"none",
      percent: 0,
      oneTrue: "none",
      completed: 0
    };
    
    if (area.actualPanels !== undefined && area.numberOfPanels !== undefined  && area.actualPanels > 0) {
      if (area.actualPanels !== area.numberOfPanels) {
        stats.type = "noMatch";
        stats.oneTrue = "actual";
        stats.count = area.actualPanels;
        stats.display = area.numberOfPanels + " / " + area.actualPanels;
      } else {
        stats.type = "actual";
        stats.oneTrue = "actual";
        stats.count = area.actualPanels;
        stats.display = area.actualPanels;
      }
    } else {
      if (area.actualPanels !== undefined  && area.actualPanels > 0) {
        stats.type = "actual";
        stats.oneTrue = "actual";
        stats.count = area.actualPanels;
        stats.display = area.actualPanels;
      } else if (area.numberOfPanels !== undefined ) {
        stats.type = "design";
        stats.oneTrue = "design";
        stats.count = area.numberOfPanels;
        stats.display = area.numberOfPanels;
      } else if (area.estimatedPanels !== undefined ) {
        stats.type = "est";
        stats.oneTrue = "est";
        stats.count = area.estimatedPanels;
        stats.display = area.estimatedPanels;
      }
    }

    stats.completed =  area.completedPanels || 0;
    stats.percent = stats.count == 0 ? 0 : Math.floor(100 * stats.completed / stats.count);

    return stats;
  }

  getProjectStats(project){
    if(project.deliverySchedule !== undefined && project.deliverySchedule.published !== undefined){
      return this.areasService.getAreasForProject(project.$key).pipe(
        switchMap(arr => {
          if(arr !== null && arr !== undefined && arr.length){
            let toCombine = [];
            arr.map((snapShot) => {
              let area = snapShot.payload.val();
              area.$key = snapShot.key;
              toCombine.push(this.getEachAreaProductionPlans(area).pipe(map(snap => {
                if(snap.key !== null){
                  return Object.assign(snap.payload.val(), {$key: snap.key, $type: "plan"});
                }
              })));
              toCombine.push(this.getEachAreaAdditionalData(area).pipe(map(snap => {
                if(snap.key !== null){
                  return Object.assign(snap.payload.val(), {$key: snap.key, $type: "additionalData"});
                }
              })));
            });
            return combineLatest(toCombine, (...arrays) => arrays.reduce((acc, array) => [...acc, ...array], [])).pipe(map(snap => {
              
              let plans = {};
              let additionalData = {};
              snap.map((data) => {
                if(data !== undefined){
                  if(data.$type === "plan"){
                    plans[data.$key] = data;
                  }else{
                    additionalData[data.$key] = data;
                  }
                }
              });
  
              var deliveryDate = "9999-99-99";
              var estimatedAreaTotal = 0;
              var estimatedCountTotal = 0;
              var actualAreaTotal = 0;
              var actualCountTotal = 0;
              var completedAreaTotal = 0;
              var completedCountTotal = 0;
              var mediumRisk = false;
              var highRisk = false;
              var past = false;
              var riskDays = undefined;
              var allAreaRisks = {};
              var allProjectRisks = {};
  
              arr.map((snapShot) => {
                let area = snapShot.payload.val();
                area.$key = snapShot.key;
                deliveryDate = this.statusService.calculateDeliveryDate(deliveryDate, area, project);
                estimatedAreaTotal += this.statusService.getOptionalNumber(area.estimatedArea);
                estimatedCountTotal += this.statusService.getOptionalNumber(area.estimatedPanels);
                actualAreaTotal += this.statusService.getOptionalNumber(area.actualArea);
                var areaActualCount = this.statusService.getOptionalNumber(area.actualPanels);
                actualCountTotal += areaActualCount;
                completedAreaTotal += this.statusService.getOptionalNumber(area.completedArea);
                completedCountTotal += this.statusService.getOptionalNumber(area.completedPanels);
                highRisk = highRisk || this.statusService.isAtRisk(areaActualCount, deliveryDate, this.designHandoverPeriod);
                mediumRisk = mediumRisk || this.statusService.isAtRisk(areaActualCount, deliveryDate, this.designHandoverPeriod + this.designHandoverBuffer);
                past = this.statusService.riskInPast(deliveryDate);
                var areaPlan = plans[area.$key];
                if (areaPlan && areaPlan.plannedFinish && areaPlan.riskDate) {
                  var areaRiskDays = this.dateUtilService.daysBetweenWeekDaysOnly(areaPlan.plannedFinish, areaPlan.riskDate);
                  if (!this.thirdPartySuppliers[area.supplier] && area.actualPanels && (!area.completedPanels || area.completedPanels < area.actualPanels)) {
                    if(project.active){
                      var productionLine = this.productTypeProductionLine[area.type];
                      if (productionLine !== undefined && allAreaRisks[productionLine] === undefined){
                        allAreaRisks[productionLine] = [];
                      }
                      allAreaRisks[productionLine].push(areaRiskDays);
                    }
                  }
                  riskDays = riskDays !== undefined ? Math.min(riskDays, areaRiskDays) : areaRiskDays;
                }
              });
  
              allProjectRisks = this.statusService.lowestRisk(allAreaRisks);
                  
              var revisions = 0;
  
              if (project.deliverySchedule.revisions !== undefined){
                revisions = Object.keys(project.deliverySchedule.revisions).length;
              }
  
              return {
                projectId: project.id,
                project: project,
                label: this.statusService.createProjectLabel(project),
                delivery: deliveryDate,
                revision: revisions,
                estimated: {
                  area: this.statusService.round(estimatedAreaTotal),
                  count: estimatedCountTotal
                },
                actual: {
                  area: this.statusService.round(actualAreaTotal),
                  areaPercentage: this.statusService.calculatePercentage(actualAreaTotal, estimatedAreaTotal),
                  count: actualCountTotal,
                  countPercentage: this.statusService.calculatePercentage(actualCountTotal, estimatedCountTotal)
                },
                completed: {
                  area: this.statusService.round(completedAreaTotal),
                  areaPercentage: this.statusService.calculatePercentage(completedAreaTotal, actualAreaTotal),
                  count: completedCountTotal,
                  countPercentage: this.statusService.calculatePercentage(completedCountTotal, actualCountTotal)
                },
                risk: {
                  past: past,
                  days: riskDays,
                  medium: highRisk ? false : mediumRisk,
                  high: highRisk
                },
                allProjectRisks : allProjectRisks,
              };
            }));
  
          }else{
            return of(null);
          }
  
        })
      );
    }else{
      return of(null);
    }

  }

  getAreaStats(project){
    return this.areasService.getAreasForProjectAndComponents(project.$key).pipe(
      switchMap(([arr, components]) => {
        let toCombine = [];
        arr.map((snapShot) => {
          let area = snapShot.payload.val();
          area.$key = snapShot.key;
          toCombine.push(this.getEachAreaProductionPlans(area).pipe(map(snap => {
            if(snap.key !== null){
              return Object.assign(snap.payload.val(), {$key: snap.key, $type: "plan"});
            }
          })));
          toCombine.push(this.getEachAreaAdditionalData(area).pipe(map(snap => {
            if(snap.key !== null){
              return Object.assign(snap.payload.val(), {$key: snap.key, $type: "additionalData"});
            }
          })));
        });
        return combineLatest(toCombine, (...arrays) => arrays.reduce((acc, array) => [...acc, ...array], [])).pipe(map(snap => {
          
          let plans = {};
          let additionalData = {};
          snap.map((data) => {
            if(data !== undefined){
              if(data.$type === "plan"){
                plans[data.$key] = data;
              }else{
                additionalData[data.$key] = data;
              }
            }
          });

          var revisions = 0;

          if (project.deliverySchedule.revisions !== undefined) {
            revisions = Object.keys(project.deliverySchedule.revisions).length;
          }

          let status = {
            id: project.id,
            name: project.name,
            revision: revisions,
            areasExport: [],
            areas: []
          }

          arr.map((snapShot) => {
            let area = snapShot.payload.val();
            area.$key = snapShot.key;

            var deliveryDate = area.revisions[project.deliverySchedule.published];

            if(deliveryDate !== undefined){

              var estimatedPanels = this.statusService.getOptionalNumber(area.estimatedPanels);
              var estimatedArea = this.statusService.getOptionalNumber(area.estimatedArea);
              var actualPanels = this.statusService.getOptionalNumber(area.actualPanels);
              var actualArea = this.statusService.getOptionalNumber(area.actualArea);
              var completedPanels = this.statusService.getOptionalNumber(area.completedPanels);
              var completedArea = this.statusService.getOptionalNumber(area.completedArea);

              var highRisk = this.statusService.isAtRisk(actualPanels, deliveryDate, this.designHandoverPeriod);
              var mediumRisk = !highRisk && this.statusService.isAtRisk(actualPanels, deliveryDate, this.designHandoverPeriod + this.designHandoverBuffer);
              
              var component = this.statusService.getComponent(components, area.$key);
              var designHandoverDate = this.dateUtilService.subtractWorkingDays(deliveryDate, this.designHandoverPeriod);

              var handoverConfirmed = "";

              if (additionalData[area.$key] !== undefined 
                && additionalData[area.$key].handoverConfirmed !== undefined
                && additionalData[area.$key].handoverConfirmed.status){
                  handoverConfirmed = this.dateUtilService.toIsoDate(additionalData[area.$key].handoverConfirmed.timeDate);
              }         

              var areaStatus = {
                areaRef: area.$key,
                handoverConfirmed: handoverConfirmed,
                designHandoverDate: designHandoverDate,
                label: area.phase + "-" + area.floor + "-" + area.type,
                component: component,
                delivery: deliveryDate,
                estimated: {
                  count: estimatedPanels,
                  area: this.statusService.round(estimatedArea)
                },
                actual: {
                  count: actualPanels,
                  countPercentage: this.statusService.calculatePercentage(actualPanels, estimatedPanels),
                  area: this.statusService.round(actualArea),
                  areaPercentage: this.statusService.calculatePercentage(actualArea, estimatedArea)
                },
                completed: {
                  count: completedPanels,
                  countPercentage: this.statusService.calculatePercentage(completedPanels, actualPanels),
                  area: this.statusService.round(completedArea),
                  areaPercentage: this.statusService.calculatePercentage(completedArea, actualArea)
                },
                risk: {
                  high: highRisk,
                  medium: mediumRisk,
                  past: false,
                  days: 0
                },
                plan: {}
              };

              var exporter = {
                Area: area.phase + "-" + area.floor + "-" + area.type,
                Component: component,
                DeliveryDate: deliveryDate,
                Supplier: area.supplier,
                PlannedStart: "",
                PlannedFinish: "",
                Risk: 0,
                EstimatedCount: estimatedPanels,
                EstimatedM2: this.statusService.round(estimatedArea),
                ActualCount: actualPanels,
                ActualM2: this.statusService.round(actualArea),
                CompletedCount: completedPanels,
                CompletedM2: this.statusService.round(completedArea)
              };
              
              var areaPlan = plans[area.$key];
              if (areaPlan) {
                var past = this.statusService.riskInPast(areaPlan.plannedFinish);
                var areaRiskDays = this.dateUtilService.daysBetweenWeekDaysOnly(areaPlan.plannedFinish, areaPlan.riskDate);
                exporter.PlannedStart = areaPlan.plannedStart;
                exporter.PlannedFinish = areaPlan.plannedFinish
                exporter.Risk = areaRiskDays;
                areaStatus.risk.past = past;
                areaStatus.risk.days = areaRiskDays;
                areaStatus.plan = {
                  plannedStart: areaPlan.plannedStart,
                  plannedFinish: areaPlan.plannedFinish,
                  riskDate: areaPlan.riskDate
                };
              }
              
              status.areasExport.push(exporter);
              status.areas.push(areaStatus);
            }

          });

          return status;
        }));
      })
    );
  }

  getPanelStats(area){
    return this.panelsService.getPanelsForArea(area.areaRef).pipe(map(arr => {
      let gPos = 0;
      let panelStats = {
        stats:[],
        actualPanels: 0,
        actualArea: 0,
        areaStarted: 0,
        started: 0,
        areaCompleted: 0,
        completed: 0,
        percentage: 0
      }
      arr.map((panel) => {
        gPos++;
        let status = "...";
        let complete = false;
        panelStats.actualArea += panel.dimensions.area;
        panelStats.actualPanels++;
        if(panel.qa !== undefined && panel.qa.started !== undefined){
          status = "In Progress";
          panelStats.started++;
          panelStats.areaStarted += panel.dimensions.area;
        }
        if(panel.qa !== undefined && panel.qa.completed !== undefined){
          status = formatDate(panel.qa.completed, "EEE dd-MMM-yy", this.locale),
          complete = true;
          panelStats.completed++;
          panelStats.areaCompleted += panel.dimensions.area;
        }
        panelStats.stats.push({
          position: gPos, 
          id: panel.id, 
          uploaded: formatDate(panel.timestamps.created, "EEE dd-MMM-yy", this.locale),
          updated: formatDate(panel.timestamps.modified, "EEE dd-MMM-yy", this.locale),
          complete: complete,
          status: status,
          panel: panel
        });
      });
      panelStats.percentage = panelStats.stats.length > 0 ? Math.round((panelStats.completed / panelStats.actualPanels) * 100) : 0
      return panelStats;
    }));
  }


}

