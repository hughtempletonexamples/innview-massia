import { Component, OnInit, Input, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../shared/services';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    providers: [ AuthService ]
})
export class LoginComponent implements OnInit {

    public password: string;
    public email: string;

    constructor(@Inject('AuthService') private authService, private router: Router) {}

    ngOnInit() {}

    onLogin() {
        this.authService.emailLogin(this.email, this.password)
        .then((value) => {
            if(value){
                localStorage.setItem('isLoggedin', 'true');
                this.router.url;
                return this.router.navigate(['/projects']);             
            }else{
                return null;
            }

        })
        .catch(error => console.log(error));
    }
}
