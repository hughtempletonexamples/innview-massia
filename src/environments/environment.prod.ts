export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyA6L-oDINBDpSha71JRJkqPbzeky6MlSLk",
    authDomain: "innview-showcase.firebaseapp.com",
    databaseURL: "https://innview-showcase.firebaseio.com",
    projectId: "innview-showcase",
    storageBucket: "innview-showcase.appspot.com",
    messagingSenderId: "752817967769"
  },
  googleMaps: {
    apiKey: "AIzaSyBEEc870OkdmV5NGohifLO7iFxWDr844yA"
  }
};
